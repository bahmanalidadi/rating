<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('company_name');
            $table->string('company_national_id');
            $table->integer('company_type');
            $table->string('register_number');
            $table->integer('register_date');
            $table->string('register_place');
            $table->integer('city_id')->unsigned();
            $table->string('company_subject');
            $table->string('activity_type');
            $table->text('company_address');
            $table->string('shipment_licence_number');
            $table->string('printing_licence_number');
            $table->string('established_history');
            $table->integer('last_modified_date');
            $table->string('current_company_subject');
            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('city');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_properties');
    }
}
