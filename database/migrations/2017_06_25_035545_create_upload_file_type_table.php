<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadFileTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_file_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('upload_file_category_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('upload_file_category_id')->references('id')->on('upload_file_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_file_type');
    }
}
