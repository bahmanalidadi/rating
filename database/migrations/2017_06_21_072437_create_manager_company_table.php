<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_company', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
//            $table->integer('company_id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('father_name');
            $table->integer('birth_date');
            $table->integer('gender');
            $table->string('national_code');
            $table->string('id_number');
            $table->string('place_of_issue');
            $table->string('phone');
            $table->string('mobile');
            $table->integer('degree');
            $table->string('major');
            $table->integer('major_status');
            $table->string('work_place');
            $table->integer('position');
            $table->integer('start_work_date');
            $table->integer('end_work_date');
            $table->string('experience');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
//            $table->foreign('company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manager_company');
    }
}
