<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJazbSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jazb_shohada', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('jazb_first_name');
            $table->string('jazb_last_name');
            $table->string('jazb_score_type');
            $table->integer('jazb_percent');
            $table->integer('jazb_start_date');
            $table->integer('jazb_end_date');
            $table->string('jazb_experience');
            $table->string('jazb_accept_number');
            $table->integer('jazb_accept_date');
            $table->string('jazb_workhouse_code');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jazb_shohada');
    }
}
