<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentralOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('central_office', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('office_space');
            $table->string('office_type');
            $table->string('office_no');
            $table->tinyInteger('office_floor');
            $table->string('office_phone');
            $table->string('office_mobile');
            $table->string('office_postal_code');
            $table->string('office_address');
            $table->string('end_work_number');
            $table->integer('end_work_date');
            $table->string('evidence_number');
            $table->integer('evidence_date');
            $table->string('rental_number');
            $table->integer('rental_date');
            $table->integer('rent_duration');
            $table->integer('rent_start');
            $table->integer('rent_end');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('central_office');
    }
}
