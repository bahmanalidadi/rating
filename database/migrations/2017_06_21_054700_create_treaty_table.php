<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treaty', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('treaty_number');
            $table->string('treaty_employees_number');
            $table->integer('treaty_date');
            $table->string('treaty_workhouse_code');
            $table->string('treaty_subject');
            $table->string('treaty_summary');
            $table->bigInteger('treaty_price');
            $table->bigInteger('treaty_total_price');
            $table->integer('treaty_duration');
            $table->integer('treaty_start_date');
            $table->integer('treaty_end_date');
            $table->string('treaty_place');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treaty');
    }
}
