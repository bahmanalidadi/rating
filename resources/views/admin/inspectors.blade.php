@extends('layouts.admin.dashboard_sub_layout')

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>لیست بازرس ها</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                لیست بازرس ها
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12" id="assessors_list" style="padding: 0;">

                                <button type="button" id="add_arzyab_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 30px;font-size: 15px;">
                                    +                                                 اضافه کردن بازرس جدید
                                </button>
                                <div style="clear: both"></div>
                                <div class="table-responsive">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr class="headings">
                                            <th>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </th>
                                            <th class="column-title" style="display: table-cell;">ردیف</th>
                                            <th class="column-title" style="display: table-cell;">نام</th>
                                            <th class="column-title" style="display: table-cell;">نام خانوادگی</th>
                                            <th class="column-title" style="display: table-cell;">مدرک تحصیلی</th>
                                            <th class="column-title" style="display: table-cell;">رشته تحصیلی</th>
                                            <th class="column-title" style="display: table-cell;">کد ملی</th>
                                            <th class="column-title" style="display: table-cell;">تلفن</th>
                                            <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">سوابق ارزیابی</span>
                                            <th class="column-title" style="display: table-cell;">عملیات</th>
                                            </th>
                                            <th class="bulk-actions" colspan="7" style="display: none;">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </td>
                                            <td class=" ">۱</td>
                                            <td class=" ">محمود</td>
                                            <td class=" ">خسروی</td>
                                            <td class=" ">دیپلم</td>
                                            <td class=" ">کامپیوتر</td>
                                            <td class=" ">4133286103</td>
                                            <td class=" ">09389267856</td>
                                            <td class=" "><a href="/responsible-company/history">نمایش سوابق</a></td>
                                            <td class=" last"><a href="#"><i class="fa fa-pencil"></i>&nbsp;</a><a href="#"><i class="fa fa-times"></i></a></td>
                                        </tr>
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </td>
                                            <td class=" ">۱</td>
                                            <td class=" ">علیرضا</td>
                                            <td class=" ">مرادی</td>
                                            <td class=" ">لیسانس</td>
                                            <td class=" ">برق</td>
                                            <td class=" ">4133286103</td>
                                            <td class=" ">09389267856</td>
                                            <td class=" "><a href="/inspector/single-request">نمایش سوابق</a></td>
                                            <td class=" last"><a href="#"><i class="fa fa-pencil"></i>&nbsp;</a><a href="#"><i class="fa fa-times"></i></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12" id="assessor_form" style="padding: 0">
                                <h2 class="extra_title StepTitle">افزودن بازرس جدید</h2>
                                <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">نام
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">نام خانوادگی
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">مدرک تحصیلی
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">رشته تحصیلی
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">کد ملی
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">تلفن
                                            </label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <button type="button" id="save_arzyab_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
                                    +                                                 اضافه کردن به لیست بازرس ها
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $('#add_arzyab_button').click(function(){
            $('#assessors_list').hide();
            $('#assessor_form').fadeIn();
        });

        $('#save_arzyab_button').click(function(){
            $('#assessor_form').hide();
            $('#assessors_list').fadeIn();
        })
    </script>
@endsection
