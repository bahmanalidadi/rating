@extends('layouts.admin.dashboard_sub_layout')

@section('content')

    <div class="right_col" role="main">
        <div class="table-responsive col-md-10" style="display: inline-block;">
            <table class="table table-striped jambo_table bulk_action" >
                <thead>
                <tr class="headings">
                    <th class="column-title" style="display: table-cell;">ردیف</th>
                    <th class="column-title" style="display: table-cell;">نام</th>
                    <th class="column-title" style="display: table-cell;">نام خانوادگی</th>
                    <th class="column-title" style="display: table-cell;">ایمیل</th>
                    <th class="column-title" style="display: table-cell;">شماره موبایل</th>
                    <th class="column-title" style="display: table-cell;">تلفن</th>
                    <th class="column-title" style="display: table-cell;">آدرس</th>
                </tr>
                </thead>

                <tbody>
                <tr class="even pointer">
                    <td class=" ">۱</td>
                    <td class=" ">محمود</td>
                    <td class=" ">خسروی</td>
                    <td class=" ">khosravi.iran@gmail.com</td>
                    <td class=" ">09389267856</td>
                    <td class=" ">06642602055</td>
                    <td class=" ">حافظ جنوبی</td>
                </tr>

                <tr class="even pointer">
                    <td class=" ">۱</td>
                    <td class=" ">محمود</td>
                    <td class=" ">خسروی</td>
                    <td class=" ">khosravi.iran@gmail.com</td>
                    <td class=" ">09389267856</td>
                    <td class=" ">06642602055</td>
                    <td class=" ">حافظ جنوبی</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection
