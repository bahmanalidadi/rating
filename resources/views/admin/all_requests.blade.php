@extends('layouts.admin.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>همه درخواست ها</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                لیست همه درخواست ها
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </th>
                                            <th class="column-title" style="display: table-cell;">نام شرکت</th>
                                            <th class="column-title" style="display: table-cell;">شماره ثبت شرکت</th>
                                            <th class="column-title" style="display: table-cell;">شناسه ملی شرکت</th>
                                            <th class="column-title" style="display: table-cell;">موضوع شرکت</th>
                                            <th class="column-title" style="display: table-cell;">نام مدیر عامل</th>
                                            <th class="column-title" style="display: table-cell;">کد ملی مدیر عامل</th>
                                            <th class="column-title" style="display: table-cell;">تلفن شرکت</th>
                                            <th class="column-title" style="display: table-cell;">آدرس شرکت</th>
                                            <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">نمایش جزئیات</span>
                                            </th>
                                            <th class="bulk-actions" colspan="7" style="display: none;">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </td>
                                            <td class=" ">هدی رایانه زاگرس</td>
                                            <td class=" ">۲۱۴۳۲۴۱</td>
                                            <td class=" ">۲۳۵۴۳۵۳۴۵۳</td>
                                            <td class=" ">IT</td>
                                            <td class=" ">سعید محمودی</td>
                                            <td class=" ">۳۵۴۲۳۵۴۲۵</td>
                                            <td class=" ">۲۱۳۲۳۲۱۲</td>
                                            <td class=" ">بروجرد- حافظ جنوبی</td>
                                            <td class=" last"><a href="/admin/single-request">نمایش اطلاعات</a>
                                            </td>
                                        </tr>
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </td>
                                            <td class=" ">دیباگران اندیشه</td>
                                            <td class=" ">۲۱۴۳۲۴۱</td>
                                            <td class=" ">۲۳۵۴۳۵۳۴۵۳</td>
                                            <td class=" ">IT</td>
                                            <td class=" ">علیرضا مرادی</td>
                                            <td class=" ">۳۵۴۲۳۵۴۲۵</td>
                                            <td class=" ">۲۱۳۲۳۲۱۲</td>
                                            <td class=" ">بروجرد- حافظ شمالی</td>
                                            <td class=" last"><a href="/admin/single-request">نمایش اطلاعات</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
