@extends('layouts.admin.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        @include('subviews.responsible_company.rating_result_first_step')
    </div>
    </div>
    </div>
    </div>
    </div>

@endsection

@section('script')
<script type="text/javascript">
    $('#display_rating_details_button').click(function(){
        window.location = "/admin/rating-result-details";
    })
    $('input').attr('disabled','disabled')
</script>
@endsection