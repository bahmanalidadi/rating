@extends('layouts.admin.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>بررسی مدارک</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                بررسی کامل بودن مدارک و ارسال به اداره کار
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <!-- start accordion -->
                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel">
                                    <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel-title">اطلاعات مربوط به شرکت</h4>
                                    </a>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            @include('subviews.user.register_company_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel-title">اطلاعات مربوط به هیئت مدیره</h4>
                                    </a>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            @include('subviews.user.register_manager_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <h4 class="panel-title">اطلاعات مربوط به دفتر مرکزی</h4>
                                    </a>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            @include('subviews.user.register_central_office_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <h4 class="panel-title">اطلاعات مربوط به پیمان ها</h4>
                                    </a>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            @include('subviews.user.register_treaty_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFive" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <h4 class="panel-title">اطلاعات مربوط به امکانات و تجهیزات</h4>
                                    </a>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                        <div class="panel-body">
                                            @include('subviews.user.register_equipment_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingSix" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <h4 class="panel-title">اطلاعات مربوط به جذب خانواده شهدا</h4>
                                    </a>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                        <div class="panel-body">
                                            @include('subviews.user.register_jazb_shohada_form')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of accordion -->
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px">

                            <button type="button" id="second_inspector_form_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#second_inspector_form_modal">
                                تایید مدارک و ارسال به اداره کار
                            </button>
                            <button type="button" id="first_inspector_form_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#first_inspector_form_modal">
                                عدم تایید مدارک و بازگشت به متقاضی
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- first_inspector_form modal -->
    <div id="first_inspector_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">فرم عدم تایید مدارک</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left edit-form" >
                        <div class="col-md-10 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" for="office_space">دلیل عدم تایید مدارک را در کادر زیر وارد کنید
                                </label>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div style="clear:both"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">ارسال مدارک به متقاضی</button>
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">لغو</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /first_inspector_form modal -->
    <!-- second_inspector_form modal -->
    <div id="second_inspector_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">تایید مدارک</h4>
                </div>
                <div class="modal-body">
                    <p>
                        شما تکمیل بودن مدارک را تایید کرده و به اداره کار ارسال نموده اید
                    </p>
                    <div style="clear:both"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">متوجه شدم</button>
                </div>
            </div>
        </div>
    </div>

    <!-- /second_inspector_form modal -->
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('input').attr('disabled', 'disabled');
            $('.edit-form input').removeAttr('disabled');
            $('.modal-footer button').removeAttr('disabled');
            $('.modal-dialog textarea').removeAttr('disabled');
            $('textarea').attr('disabled', 'disabled');
            $('.dropdown-btn').attr('disabled', 'disabled');
            $('button').hide();
            $('.display-btn').show();
            $('#back_to_certificate_list').show();
            $('#back_to_employees_list').show();
            $('#back_to_treaty_employees_list').show();
            $('#back_to_equipment_list').show();
            $('#back_to_jazb_shohada_list').show();
            $('#back_to_treaty_list').show();
            $('#bill_upload_box').show();
            $('.upload_box_title').text('مدارک آپلود شده');
            $('.extra_title').remove();
            $('.member_card_box').children()[$('.member_card_box').children().length - 1].remove()
            $('.edit-btn').hide();
            $('.delete-btn').hide();

            $('#first_inspector_form_button').show();
            $('#second_inspector_form_button').show();
            $('.modal-footer button').show();
            $('.modal-dialog textarea').removeAttr('disabled');


        })
    </script>
@endsection
