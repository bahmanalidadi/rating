@extends('layouts.admin.dashboard_sub_layout')

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>تعریف کدهای معین</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>تعریف کدهای معین</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                    <tr class="headings">
                                        <th>
                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        </th>
                                        <th class="column-title" style="display: table-cell;">نام معین</th>
                                        <th class="column-title" style="display: table-cell;">کد درآمد</tH>
                                        <th class="column-title" style="display: table-cell;">عملیات</tH>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr class="even pointer">
                                        <td class="a-center ">
                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        </td>
                                        <td class=" ">---</td>
                                        <td class=" ">---</td>
                                        <td class=" "><a href="#"><i class="fa fa-times" aria-hidden="true"></i>
                                                &nbsp;<i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                    </tr>
                                    <tr class="even pointer">
                                        <td class="a-center ">
                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        </td>
                                        <td class=" ">---</td>
                                        <td class=" ">---</td>
                                        <td class=" "><a href="#"><i class="fa fa-times" aria-hidden="true"></i>
                                                &nbsp;<i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <button type="button" id="add_equipment_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">+ اضافه کردن کدهای معین
                        </button>
                        <div class="col-md-12" id="equipment_form" style="padding: 0">
                            <h2 class="extra_title StepTitle">افزودن کدهای معین </h2>
                            <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_model">نتم معین
                                        </label>
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="equipment_model" name="equipment_model" required="required" class="date_input form-control col-md-12 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_serial">کد معین
                                        </label>
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="equipment_serial" name="equipment_serial" required="required" class="form-control col-md-12 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <button type="button" id="save_equipment_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
                                +                                                 اضافه کردن به کدهای معین
                            </button>
                            <button type="button" id="back_to_equipment_list" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
                                بازگشت به کدهای معین
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
