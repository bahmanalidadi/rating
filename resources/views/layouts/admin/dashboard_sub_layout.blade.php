@extends('layouts.dashboard_layout')

@section('menu')
    <li><a><i class="fa fa-table"></i>متقاضیان<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/all-users">کلیه متقاضیان</a></li>
            <li><a href="/admin/all-requests">همه درخواست ها</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>ارزیابی<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/new-requests">درخواست های بررسی نشده</a></li>
            <li><a href="/admin/rating-result">نتیجه ارزیابی درخواست ها</a></li>
            <li><a href="/admin/assessors">لیست ارزیاب ها</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>بازرسی<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/inspect-new-requests">درخواست های بررسی نشده</a></li>
            <li><a href="/admin/inspect-result">نتیجه بازرسی ها</a></li>
            <li><a href="/admin/inspectors">لیست بازرس ها</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>کاربران<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/users">متقاضیان</a></li>
            <li><a href="/admin/assessors_list">ارزیاب</a></li>
            <li><a href="/admin/responsible">مسئولان در اداره کار</a></li>
            <li><a href="/admin/admins">کارشناسان سامانه</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>گزارشات<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="#">متقاضیان براساس تاریخ درخواست</a></li>
            <li><a href="#">درخواستهای رتبه بندی شده</a></li>
            <li><a href="#">درخواست های رتبه بندی نشده</a></li>
            <li><a href="/admin/expert-confirmed">شرکت های تایید شده توسط کارشناس</a></li>
            <li><a href="/admin/expert-not-confirmed">شرکت های تایید نشده توسط کارشناس</a></li>
            <li><a href="/admin/office-confirmed">شرکت های تایید شده توسط اداره کار</a></li>
            <li><a href="/admin/office-not-confirmed">شرکت های تایید نشده توسط اداره کار</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>آمار<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="#">ثبت نام شده ها</a></li>
            <li><a href="#">تعداد کاربران</a></li>
            <li><a href="#">تایید شده ها</a></li>
            <li><a href="#">تایید نشده ها</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>حسابداری<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/accounting/definition">تعریف حساب ها</a></li>
            <li><a href="/admin/accounting/income">تعرف درآمد</a></li>
            <li><a href="/admin/accounting/moein-codes">تعریف کد های معین</a></li>
            <li><a href="/admin/accounting/define-detailed">تعریف کد های تفصیل</a></li>
            <li><a href="/admin/accounting/received">ثبت دریافتی ها</a></li>
            <li><a href="/admin/accounting/payments">ثبت پرداختی ها</a></li>
            <li><a href="#">گزارش معین</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>دبیر خانه<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/receive-letters">نامه های دریافتی</a></li>
            <li><a href="/admin/sent-letters">نامه های ارسالی</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>عضویت<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/member-list">لیست اعضا</a></li>
            <li><a href="/admin/payments-made">پرداخت های انجام شده</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>تعاریف<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/admin/accounting/definition">تعریف حساب ها</a></li>
            <li><a href="/admin/accounting/income">تعرف درآمد</a></li>
            <li><a href="/admin/accounting/moein-codes">تعریف کد های معین</a></li>
            <li><a href="/admin/accounting/define-detailed">تعریف کد های تفصیل</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>اطلاعات پایه<span class="fa fa-chevron-down"></span></a>

        <ul class="nav child_menu">
            <li><a><i class="fa fa-table"></i>کانون<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#">استان ها</a></li>
                    <li><a href="#">شهر ها</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-table"></i>تعیین طلاحیت<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#">---</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-table"></i>رتبه بندی<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#">ضرایب رتبه بندی</a></li>
                </ul>
            </li>

        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>پیامک<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="#">ارسال گروهی پیامک</a></li>
            <li><a href="#">ارسال پیامک</a></li>
            <li><a href="#">محتوای پیامک هشدار</a></li>
            <li><a href="#">تنظیم پنل پیامک</a></li>
        </ul>
    </li>
@endsection

@section('menu')
@endsection