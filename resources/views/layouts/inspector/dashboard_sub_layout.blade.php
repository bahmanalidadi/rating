@extends('layouts.dashboard_layout')

@section('menu')
    <li><a href="/inspector/new-requests"><i class="fa fa-table"></i>درخواست های بررسی نشده</a></li>
    <li><a href="/inspector/all-requests"><i class="fa fa-table"></i>همه درخواست ها</a></li>
    <li><a href="/inspector/inspect-result"><i class="fa fa-table"></i>سوابق بازرسی ها</a></li>
    <li><a href="/inspector/template-forms"><i class="fa fa-table"></i>فرم های خام بازرسی</a></li>
@endsection

@section('menu')
@endsection