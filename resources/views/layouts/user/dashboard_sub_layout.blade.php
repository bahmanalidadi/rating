@extends('layouts.dashboard_layout')

@section('menu')
    <li><a><i class="fa fa-table"></i>اطلاعات  شرکت <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/user/register-info">مشخصات شرکت</a></li>
            <li><a href="#">اعضای هیئت مدیره</a></li>
            <li><a href="#">دفتر مرکزی</a></li>
            <li><a href="#">مشخصات پیمان ها</a></li>
            <li><a href="#">امکانات و تجهیزات کار</a></li>
            <li><a href="#">جذب خانواده شهداء، ایثارگران، معلولین غیر جنگی</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>آرشیو مدارک آپلودی</a></li>
    <li><a href="/user/request-membership"><i class="fa fa-table"></i>درخواست عضویت</a></li>
    <li><a href="/user/pay-factor"><i class="fa fa-table"></i>پرداخت حق عضویت</a></li>
    <li><a href="/user/request-eligibility"><i class="fa fa-table"></i>درخواست تعیین صلاحیت</a></li>
    <li><a href="/user/request-rating"><i class="fa fa-table"></i>درخواست رتبه بندی</a></li>
    <li><a href="/user/rating-result-single"><i class="fa fa-table"></i>نتیجه رتبه بندی</a></li>
@endsection

@section('menu')
@endsection