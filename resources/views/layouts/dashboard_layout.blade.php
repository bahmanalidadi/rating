<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>سامانه اخذ صلاحیت و رتبه بندی</title>

    <!-- Bootstrap -->
    <link href="/public_vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/public_vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/public_vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/public_vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="/public_vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/public_vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/public_vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    @yield('style')
    <!-- Custom Theme Style -->
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/bootstrap-rtl.min.css" rel="stylesheet">
    <link href="/css/dashboard_style.css" rel="stylesheet">
</head>

<body class="nav-md footer_fixed">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="#" class="site_title">
                        {{--<i class="fa fa-paw"></i>--}}
                        <img class="dashboard_logo" src="/images/logo.png">
                        <span>سامانه اخذ صلاحیت و رتبه بندی</span>
                    </a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="/images/alireza.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>خوش آمدید,</span>
                        <h2>{{$user->name}}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        {{--<h3>General</h3>--}}
                        <ul class="nav side-menu">
                            <li><a href="/dashboard"><i class="fa fa-home"></i> داشبورد </a></li>
                            @yield('menu')
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle pull-right">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-left personal_section">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="/images/alireza.jpg" alt="">{{$user->name}}
                                <span class=" fa fa-angle-down" style="display: inline-block !important"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;"> پروفایل</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>تنظیمات</span>
                                    </a>
                                </li>
                                {{--<li><a href="javascript:;">Help</a></li>--}}
                                <li>
                                    <a href="/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out pull-right"></i> خروج
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="/images/alireza.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        @yield('content')
        <!-- footer content -->
        <footer>
            <div class="pull-left">
                طراحی و توسعه توسط شرک
                <a href="http://hodarayaneh.ir">هدی رایانه زاگرس</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="/public_vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/public_vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/public_vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/public_vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="/public_vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="/public_vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="/public_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="/public_vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="/public_vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="/public_vendors/Flot/jquery.flot.js"></script>
<script src="/public_vendors/Flot/jquery.flot.pie.js"></script>
<script src="/public_vendors/Flot/jquery.flot.time.js"></script>
<script src="/public_vendors/Flot/jquery.flot.stack.js"></script>
<script src="/public_vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="/public_vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="/public_vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="/public_vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="/public_vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="/public_vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="/public_vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="/public_vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="/public_vendors/moment/min/moment.min.js"></script>
<script src="/public_vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- FastClick -->
<script src="/public_vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/public_vendors/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="/public_vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script src="/js/script.js"></script>

        <!-- Custom Theme Scripts -->

<script src="/js/custom.min.js"></script>
@yield('script')

</body>
</html>
