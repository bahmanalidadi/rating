@extends('layouts.dashboard_layout')

@section('menu')
    <li><a><i class="fa fa-table"></i>ارزیابی<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/responsible-company/new-requests">درخواست های بررسی نشده</a></li>
            <li><a href="/responsible-company/all-requests?mode=view">همه درخواست ها</a></li>
            <li><a href="/responsible-company/rating-result">سوابق ارزیابی ها</a></li>
            <li><a href="/responsible-company/assessors">لیست ارزیاب ها</a></li>
            <li><a href="/responsible-company/rating-template-forms">فرم های خام ارزیابی</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>بازرسی<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/responsible-company/inspect-new-requests">درخواست های بررسی نشده</a></li>
            <li><a href="/responsible-company/inspect-all-requests?mode=view">همه درخواست ها</a></li>
            <li><a href="/responsible-company/inspect-result?mode=view">نتایج بازرسی</a></li>
            <li><a href="/responsible-company/inspectors">لیست بازرس ها</a></li>
            <li><a href="/responsible-company/inspect-template-forms">فرم های خام بازرسی</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>کمیته تجدید نظر<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/responsible-company/new-requests">درخواست های بررسی نشده</a></li>
            <li><a href="/responsible-company/all-requests?mode=view">همه درخواست ها</a></li>
            <li><a href="/responsible-company/rating-result?mode=view">نتایج ارزیابی</a></li>
        </ul>
    </li>
    <li><a><i class="fa fa-table"></i>کمیته استانی<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="/responsible-company/inspect-new-requests">درخواست های بررسی نشده</a></li>
            <li><a href="/responsible-company/inspect-all-requests?mode=view">همه درخواست ها</a></li>
            <li><a href="/responsible-company/inspect-result?mode=view">نتایج بازرسی</a></li>
        </ul>
    </li>
@endsection