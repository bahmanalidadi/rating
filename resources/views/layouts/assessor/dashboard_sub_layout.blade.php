@extends('layouts.dashboard_layout')

@section('menu')
    <li><a href="/assessor/new-requests"><i class="fa fa-table"></i>درخواست های بررسی نشده</a></li>
    <li><a href="/assessor/requests"><i class="fa fa-table"></i>درخواست های بررسی شده</a></li>
    <li><a href="/assessor/history"><i class="fa fa-table"></i>سوابق ارزیابی</a></li>
    <li><a href="/assessor/download-forms"><i class="fa fa-table"></i>فرم خام ارزیابی</a></li>
@endsection