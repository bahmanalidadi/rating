@extends('layouts.assessor.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        @include('subviews.responsible_company.rating_result_second_step')
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#rating_wizard').smartWizard({
            // Properties
            selected: 0,  // Selected Step, 0 = first step
            keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
            enableAllSteps: false,  // Enable/Disable all steps on first load
            transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
            contentURL:null, // specifying content url enables ajax content loading
            contentURLData:null, // override ajax query parameters
            contentCache:true, // cache step contents, if false content is fetched always from ajax url
            cycleSteps: false, // cycle step navigation
            enableFinishButton: false, // makes finish button enabled always
            hideButtonsOnDisabled: false, // when the previous/next/finish buttons are disabled, hide them instead
            errorSteps:[],    // array of step numbers to highlighting as error steps
            labelNext:'مرحله بعد', // label for Next button
            labelPrevious:'مرحله قبل', // label for Previous button
            labelFinish:'فرم محاسبه امتیاز',  // label for Finish button
            noForwardJumping:false,
            ajaxType: 'POST',
            // Events
            onLeaveStep: null, // triggers when leaving a step
            onShowStep: null,  // triggers when showing a step
            onFinish: function(){
                window.location= "/assessor/calculate-score";
            },  // triggers when Finish button is clicked
            buttonOrder: ['finish', 'next', 'prev']  // button order, to hide a button remove it from the list
        });

        $('td .fa-times').parents('td').remove();
        $('.reject_title').remove();

    </script>
@endsection