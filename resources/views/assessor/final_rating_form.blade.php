@extends('layouts.assessor.dashboard_sub_layout')

@section('content')

    <div class="right_col" role="main">
        @include('subviews.responsible_company.rating_result_first_step')
    </div>
@endsection
@section('script')
    @if($mode == 'view')
        <script type="text/javascript">
            $('input').attr('disabled', 'disabled');
        </script>
    @endif
@endsection