@extends('layouts.assessor.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>فرم های ۹ مرحله ای رتبه بندی</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                فرم های ۹ مرحله ای رتبه بندی شرکت هدی رایانه زاگرس
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <!-- Smart Wizard -->
                            <p style="padding-bottom: 20px;">
                                این فرم برای رتبه بندی اطلاعات مربوط به شرکت، هیئت مدیره، کارکنان شرکت و ... می باشد
                                <span style="color: #1ABB9C;">
                                    (اطلاعات در هر مرحله ذخیره می شود)
                                </span>
                            </p>
                            <div id="rating_wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a href="#step-1">
                                            <span class="step_no">۱</span>
                            <span class="step_descr">
                                              مرحله ۱<br />
                                              <small>تحصیلات هیئت مدیره</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <span class="step_no">۲</span>
                            <span class="step_descr">
                                              مرحله ۲<br />
                                              <small>سوابق تجربی هیئت مدیره</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <span class="step_no">۳</span>
                            <span class="step_descr">
                                              مرحله ۳<br />
                                              <small>نیروی انسانی</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-4">
                                            <span class="step_no">۴</span>
                            <span class="step_descr">
                                              مرحله ۴<br />
                                              <small>سابقه فعالیت مالی</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-5">
                                            <span class="step_no">۵</span>
                            <span class="step_descr">
                                              مرحله ۵<br />
                                              <small>کارکنان دفتر مرکزی</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-6">
                                            <span class="step_no">۶</span>
                            <span class="step_descr">
                                              مرحله ۶<br />
                                              <small>سابقه تاسیس شرکت</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-7">
                                            <span class="step_no">۷</span>
                            <span class="step_descr">
                                              مرحله ۷<br />
                                              <small>موقعیت اداری دفتر مرکزی</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-8">
                                            <span class="step_no">۸</span>
                            <span class="step_descr">
                                              مرحله ۸<br />
                                              <small>جذب خانواده شهدا و ...</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-9">
                                            <span class="step_no">۹</span>
                            <span class="step_descr">
                                              مرحله ۹<br />
                                              <small>امکانات و تجهیزات</small>
                                          </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="step-1">
                                    @include('subviews.assessor.members_rating')
                                </div>
                                <div id="step-2">
                                    @include('subviews.assessor.experience_member_rating')
                                </div>
                                <div id="step-3">
                                    @include('subviews.assessor.human_resource_rating')
                                </div>
                                <div id="step-4">
                                    @include('subviews.assessor.financial_activity_rating')
                                </div>
                                <div id="step-5">
                                    @include('subviews.assessor.employees_rating')
                                </div>
                                <div id="step-6">
                                    @include('subviews.assessor.established_history_rating')
                                </div>
                                <div id="step-7">
                                    @include('subviews.assessor.office_rating')
                                </div>
                                <div id="step-8">
                                    @include('subviews.assessor.jazb_shohada_rating')
                                </div>
                                <div id="step-9">
                                    @include('subviews.assessor.equipment_rating')
                                </div>
                            </div>
                            <!-- End SmartWizard Content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#rating_wizard').smartWizard({
            // Properties
            selected: 0,  // Selected Step, 0 = first step
            keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
            enableAllSteps: false,  // Enable/Disable all steps on first load
            transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
            contentURL:null, // specifying content url enables ajax content loading
            contentURLData:null, // override ajax query parameters
            contentCache:true, // cache step contents, if false content is fetched always from ajax url
            cycleSteps: false, // cycle step navigation
            enableFinishButton: false, // makes finish button enabled always
            hideButtonsOnDisabled: false, // when the previous/next/finish buttons are disabled, hide them instead
            errorSteps:[],    // array of step numbers to highlighting as error steps
            labelNext:'مرحله بعد', // label for Next button
            labelPrevious:'مرحله قبل', // label for Previous button
            labelFinish:'فرم محاسبه امتیاز',  // label for Finish button
            noForwardJumping:false,
            ajaxType: 'POST',
            // Events
            onLeaveStep: null, // triggers when leaving a step
            onShowStep: null,  // triggers when showing a step
            onFinish: function(){
                window.location= "/assessor/calculate-score";
            },  // triggers when Finish button is clicked
            buttonOrder: ['finish', 'next', 'prev']  // button order, to hide a button remove it from the list
        });

        $('td .fa-times').click(function(){
            $(this).parents('tr').css('background-color', '#ffb0b0').css('color', '#fff');
            $('textarea').css('color', '#000');

            $('.reject_description').css('display', 'table-cell');
            $('.reject_description_title').css('display', 'table-cell');
        })

    </script>
    @if($mode=='view')
        <script type="text/javascript">
            $('.reject_description').hide();
            $('.reject_description_title').hide();
            $('td .fa-times').hide();
        </script>
    @endif
@endsection