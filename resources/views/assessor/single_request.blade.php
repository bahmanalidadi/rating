@extends('layouts.assessor.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>درخواست بازرسی</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                درخواست بازرسی شرکت هدی رایانه زاگرس
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <!-- start accordion -->
                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel">
                                    <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel-title">اطلاعات مربوط به شرکت</h4>
                                    </a>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            @include('subviews.user.register_company_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel-title">اطلاعات مربوط به هیئت مدیره</h4>
                                    </a>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            @include('subviews.user.register_manager_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <h4 class="panel-title">اطلاعات مربوط به دفتر مرکزی</h4>
                                    </a>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            @include('subviews.user.register_central_office_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <h4 class="panel-title">اطلاعات مربوط به پیمان ها</h4>
                                    </a>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            @include('subviews.user.register_treaty_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFive" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <h4 class="panel-title">اطلاعات مربوط به امکانات و تجهیزات</h4>
                                    </a>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                        <div class="panel-body">
                                            @include('subviews.user.register_equipment_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingSix" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <h4 class="panel-title">اطلاعات مربوط به جذب خانواده شهدا</h4>
                                    </a>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                        <div class="panel-body">
                                            @include('subviews.user.register_jazb_shohada_form')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of accordion -->
                        </div>
                        <p style="color: #e91e64;margin-top: 30px;margin-bottom: 30px;font-size: 16px;font-weight: bold;">موارد مورد رتبه بندی که در زیر نشانه گذاری شده برای رتبه بندی ارسال شده است</p>
                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" style="padding-right: 30px;">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat" checked="checked">
                                    کد 1 - امور حمل و نقل شامل : رانندگی و اداره امور نقلیه و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 2 - تعمیر و نگهداری شامل : تعمیر و نگهداری تجهیزات و وسایل اداری (به غیر از امور رایانه)،خدمات فنی خودرو و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 3 - امور آشپزخانه و رستوران شامل : طبخ و توزیع غذا،اداره رستوران و بوفه و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 4 - خدمات عمومی شامل : تنظیفات، نامه رسانی،پیشخدمتی،امور آبدارخانه، خدمات پاسخگویی تلفنی و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 5 - نگهداری و خدمات فضای سبز شامل:خدمات باغبانی و نگهداری فضای سبز و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 6 - امور چاپ و تکثیر شامل:صحافی،حروفچینی،غلط گیری،خطاطی، چاپ و تکثیر و سایر امور فنی و هنری چاپ و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 7 -امور تاسیساتی شامل: تعمیر و نگهداری و بهره برداری از تاسیسات برودتی و حرارتی وموارد مشابه
                                </label>
                            </div>
                        </form>
                        <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;">
                            <p>بعد از مطالعه اطلاعات شرکت فرم های ۹ مرحله ای رتبه بندی را تکمیل نمایید</p>

                            @if($mode == 'view')
                                <a href="/assessor/rating-form-steps?mode=view">
                                    <button type="button" id="second_inspector_form_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
                                        نمایش جزئیات رتبه بندی
                                    </button>
                                </a>
                            @else
                                <a href="/assessor/rating-form-steps">
                                    <button type="button" id="second_inspector_form_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
                                        تکمیل فرم های ۹ مرحله ای رتبه بندی
                                    </button>
                                </a>
                            @endif
                            @if($mode == 'view')
                                <button type="button" id="upload_form_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#upload_form_modal">
                                    فرم های ارزیابی آپلود شده
                                </button>
                            @else

                                <button type="button" id="upload_form_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#upload_form_modal">
                                    آپلود فرم های ارزیابی
                                </button>
                            @endif

                                    <!-- upload form modal -->
                                <div id="upload_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myModalLabel">آپلود فرم های ارزیابی</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="col-md-4 upload_item" id="form1_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۱</span>
                                                                    <input disabled=disabled type="file" name="form1_upload_file" id="form1_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form2_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۲</span>
                                                                    <input disabled=disabled type="file" name="form2_upload_file" id="form2_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form3_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۳</span>
                                                                    <input disabled=disabled type="file" name="form3_upload_file" id="form3_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form4_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۴</span>
                                                                    <input disabled=disabled type="file" name="form4_upload_file" id="form4_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form5_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۵</span>
                                                                    <input disabled=disabled type="file" name="form5_upload_file" id="form5_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form6_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۶</span>
                                                                    <input disabled=disabled type="file" name="form6_upload_file" id="form6_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form7_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۷</span>
                                                                    <input disabled=disabled type="file" name="form7_upload_file" id="form7_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form8_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۸</span>
                                                                    <input disabled=disabled type="file" name="form8_upload_file" id="form8_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="form9_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="/images/upload-icon.png">
                                                                    </div>
                                                                    <span>فرم ۹</span>
                                                                    <input disabled=disabled type="file" name="form9_upload_file" id="form9_upload_file" class="upload_input_file form_upload">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">بستن</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- /upload form modal -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('input').attr('disabled', 'disabled');
            $('.edit-form input').removeAttr('disabled');
            $('.modal-footer button').removeAttr('disabled');
            $('.modal-dialog textarea').removeAttr('disabled');
            $('textarea').attr('disabled', 'disabled');
            $('.dropdown-btn').attr('disabled', 'disabled');
            $('button').hide();
            $('.display-btn').show();
            $('#back_to_certificate_list').show();
            $('#back_to_employees_list').show();
            $('#back_to_treaty_employees_list').show();
            $('#back_to_equipment_list').show();
            $('#back_to_jazb_shohada_list').show();
            $('#back_to_treaty_list').show();
            $('#bill_upload_box').show();
            $('.upload_box_title').text('مدارک آپلود شده');
            $('.extra_title').remove();
            $('.member_card_box').children()[$('.member_card_box').children().length - 1].remove()
            $('.edit-btn').hide();
            $('.delete-btn').hide();

            $('#first_inspector_form_button').show();
            $('#second_inspector_form_button').show();
            $('#upload_form_button').show();
            $('.modal-footer button').show();
            $('.modal-dialog textarea').removeAttr('disabled');

            @if($mode != 'view')
                $('.form_upload').removeAttr('disabled')
            @endif

    })
    </script>

@endsection
