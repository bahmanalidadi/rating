@extends('layouts.user.dashboard_sub_layout')
@section('content')
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_right">
                        <h3>پرداخت حق عضویت</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row" style="padding-bottom: 50px;">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>فرم  پرداخت حق عضویت

                                </h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p>
                                    مبلغ ماهیانه حق عضویت ‌:
                                    <span style="color: #F00;">250</span>
                                    هزار تومان
                                </p>
                                <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-right" style="margin-top: 50px;">
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-4 col-xs-12" for="pay_period">مدت
                                        </label>
                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <select name="pay_period" id="pay_period"class="form-control col-md-12 col-xs-12">
                                                <option value="3">۳ ماهه</option>
                                                <option value="6">۶ ماهه</option>
                                                <option value="9">۹ ماهه</option>
                                                <option value="12">یک ساله</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 100px">
                                        <h3 class="col-md-7 col-sm-12 col-xs-12">
                                            کل مبلغ قابل پرداخت:
                                            <span style="color: #F00;">500</span>
                                            هزار تومان
                                        </h3>

                                        <button type="button" id="add_equipment_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
                                            انتقال به درگاه بانک برای پرداخت
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
