@extends('layouts.user.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>ثبت درخواست رتبه بندی</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                کلیه اطلاعات  به همراه نواقص
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p style="padding-bottom: 20px;">
                                اطلاعات زیر کلیه اطلاعاتی است که شما در مراحل قبل کامل نموده اید
                                <span style="color: #E91E63;">
                                    (نواقص با رنگ قرمز مشخص شده و باید برطرف گردند تا بتوانید درخواست خود را ثبت کنید)
                                </span>
                            </p>
                            <!-- start accordion -->
                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel">
                                    <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel-title">اطلاعات مربوط به شرکت</h4>
                                    </a>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            @include('subviews.user.register_company_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel-title">اطلاعات مربوط به هیئت مدیره</h4>
                                    </a>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            @include('subviews.user.register_manager_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <h4 class="panel-title">اطلاعات مربوط به دفتر مرکزی</h4>
                                    </a>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            @include('subviews.user.register_central_office_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <h4 class="panel-title">اطلاعات مربوط به پیمان ها</h4>
                                    </a>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            @include('subviews.user.register_treaty_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFive" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <h4 class="panel-title">اطلاعات مربوط به امکانات و تجهیزات</h4>
                                    </a>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                        <div class="panel-body">
                                            @include('subviews.user.register_equipment_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingSix" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <h4 class="panel-title">اطلاعات مربوط به جذب خانواده شهدا</h4>
                                    </a>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                        <div class="panel-body">
                                            @include('subviews.user.register_jazb_shohada_form')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of accordion -->

                            <p style="color: #e91e64;margin-top: 30px;margin-bottom: 30px;font-size: 16px;font-weight: bold;">موارد مورد رتبه بندی را در زیر نشانه گذاری و سپس درخواست خود را ارسال کنید</p>
                            <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" style="padding-right: 30px;">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rating_option"class="flat" checked="checked">
                                        کد 1 - امور حمل و نقل شامل : رانندگی و اداره امور نقلیه و موارد مشابه
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rating_option"class="flat">
                                        کد 2 - تعمیر و نگهداری شامل : تعمیر و نگهداری تجهیزات و وسایل اداری (به غیر از امور رایانه)،خدمات فنی خودرو و موارد مشابه
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rating_option"class="flat">
                                        کد 3 - امور آشپزخانه و رستوران شامل : طبخ و توزیع غذا،اداره رستوران و بوفه و موارد مشابه
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rating_option"class="flat">
                                        کد 4 - خدمات عمومی شامل : تنظیفات، نامه رسانی،پیشخدمتی،امور آبدارخانه، خدمات پاسخگویی تلفنی و موارد مشابه
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rating_option"class="flat">
                                        کد 5 - نگهداری و خدمات فضای سبز شامل:خدمات باغبانی و نگهداری فضای سبز و موارد مشابه
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rating_option"class="flat">
                                        کد 6 - امور چاپ و تکثیر شامل:صحافی،حروفچینی،غلط گیری،خطاطی، چاپ و تکثیر و سایر امور فنی و هنری چاپ و موارد مشابه
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rating_option"class="flat">
                                        کد 7 -امور تاسیساتی شامل: تعمیر و نگهداری و بهره برداری از تاسیسات برودتی و حرارتی وموارد مشابه
                                    </label>
                                </div>
                            </form>
                            <button type="button" id="send_eligibility_request_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 30px;font-size: 15px;" data-toggle="modal" data-target="#message_modal">
                                ارسال درخواست رتبه بندی
                            </button>


                        </div>
                    </div>
                </div>
                <!-- message modal -->
                <div id="message_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">درخواست رتبه بندی</h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    درخواست رتبه بندی برای شما با موفقیت ارسال گردید <br>
                                    نتیجه متعاقباً به شما اعلام می گردد<br>
                                    کد رهگیری : ۱۲۳۴۵
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">تایید</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /message modal -->
            </div>
        </div>
    </div>
@endsection
