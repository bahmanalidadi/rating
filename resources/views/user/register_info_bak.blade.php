@extends('layouts.user.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>فرم ثبت اطلاعات</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>فرم ثبت اطلاعات شرکت
                                {{--<small>Sessions</small>--}}
                            </h2>
                            {{--<ul class="nav navbar-left panel_toolbox">--}}
                                {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                                {{--</li>--}}
                                {{--<li class="dropdown">--}}
                                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                    {{--<ul class="dropdown-menu" role="menu">--}}
                                        {{--<li><a href="#">Settings 1</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="#">Settings 2</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                                {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <!-- Smart Wizard -->
                            <p style="padding-bottom: 20px;">
                                این فرم برای ثبت اطلاعات مربوط به شرکت، هیئت مدیره، کارکنان شرکت و ... می باشد
                                <span style="color: #1ABB9C;">
                                    (اطلاعات در هر مرحله ذخیره می شود)
                                </span>
                            </p>
                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a href="#step-1">
                                            <span class="step_no">۱</span>
                            <span class="step_descr">
                                              مرحله ۱<br />
                                              <small>اطلاعات شرکت</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <span class="step_no">۲</span>
                            <span class="step_descr">
                                              مرحله ۲<br />
                                              <small>اعضای هیئت مدیره</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <span class="step_no">۳</span>
                            <span class="step_descr">
                                              مرحله ۳<br />
                                              <small>دفتر مرکزی</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-4">
                                            <span class="step_no">۴</span>
                            <span class="step_descr">
                                              مرحله ۴<br />
                                              <small>پیمان ها</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-5">
                                            <span class="step_no">۵</span>
                            <span class="step_descr">
                                              مرحله ۵<br />
                                              <small>امکانات و تجهیزات</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-6">
                                            <span class="step_no">۶</span>
                            <span class="step_descr">
                                              مرحله ۶<br />
                                              <small>جذب خانواده شهدا و ...</small>
                                          </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="step-1">
                                    <h2 class="StepTitle"> اطلاعات مربوط به شرکت</h2>
                                    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="company_name">نام شرکت
                                                </label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input type="text" id="company_name" name="company_name" required="required" class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="company_national_id">شناسه ملی شرکت
                                                </label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input type="text" id="company_national_id" name="company_national_id" required="required" class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4">نوع شرکت</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" class="flat" checked name="company_type"> سهامی خاص
                                                        </label>
                                                        <label>
                                                            <input type="radio" class="flat" name="company_type"> سهامی عام
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="register_number">شماره ثبت
                                                </label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input type="text" id="register_number" name="register_number" required="required" class="form-control col-md-12 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_date" class="control-label col-md-4 col-sm-4 col-xs-12">تاریخ ثبت</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="register_date" name="register_date" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_place" class="control-label col-md-4 col-sm-4 col-xs-12">محل ثبت</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="register_place" name="register_place" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="company_subject" class="control-label col-md-4 col-sm-4 col-xs-12">موضوع شرکت</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="company_subject" name="company_subject" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="activity_type" class="control-label col-md-4 col-sm-4 col-xs-12">نوع فعالیت</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="activity_type" name="activity_type" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="activity_type" class="control-label col-md-4 col-sm-4 col-xs-12">نشانی قانونی شرکت</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <textarea id="activity_type" name="activity_type" class="form-control col-md-12 col-xs-12" type="text"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="province" class="control-label col-md-4 col-sm-4 col-xs-12">استان</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group col-md-12" style="padding: 0">
                                                        <button data-toggle="dropdown" style="text-align: right" class="col-md-12 btn btn-default dropdown-toggle" type="button"> انتخاب استان <span class="caret pull-left" style="margin-top: 8px;"></span> </button>
                                                        <ul class="dropdown-menu col-md-12" style="padding: 0">
                                                            <li><a href="#">تهران</a>
                                                            </li>
                                                            <li><a href="#">لرستان</a>
                                                            </li>
                                                            <li><a href="#">مرکزی</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="city" class="control-label col-md-4 col-sm-4 col-xs-12">شهرستان</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <div class="btn-group col-md-12" style="padding: 0">
                                                        <button data-toggle="dropdown" style="text-align: right" class="col-md-12 btn btn-default dropdown-toggle" type="button"> انتخاب شهرستان <span class="caret pull-left" style="margin-top: 8px;"></span> </button>
                                                        <ul class="dropdown-menu col-md-12" style="padding: 0">
                                                            <li><a href="#">تهران</a>
                                                            </li>
                                                            <li><a href="#">بروجرد</a>
                                                            </li>
                                                            <li><a href="#">اراک</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="shipment_licence_number" class="control-label col-md-4 col-sm-4 col-xs-12">شماره مجوز امور حمل و نقل</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="shipment_licence_number" name="shipment_licence_number" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="printing_licence_number" class="control-label col-md-4 col-sm-4 col-xs-12">شماره مجوز امور چاپ و تکثیر</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="printing_licence_number" name="printing_licence_number" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="established_history" class="control-label col-md-4 col-sm-4 col-xs-12">سابقه تاسیس</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="established_history" name="established_history" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="last_modified_date" class="control-label col-md-4 col-sm-4 col-xs-12">تاریخ آخرین تغییرات</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="last_modified_date" name="last_modified_date" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="current_company_subject" class="control-label col-md-4 col-sm-4 col-xs-12">موضوع شرکت فعلی برابر اخرین تغییرات</label>
                                                <div class="col-md-8 col-sm-8 col-xs-12">
                                                    <input id="current_company_subject" name="current_company_subject" class="form-control col-md-12 col-xs-12" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                                        <p>مدارک مربوطه زیر را پلود کنید</p>
                                        <form action="">
                                            <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                <div class="col-md-7 col-sm-12 col-xs-12">
                                                    <div class="col-md-4 upload_item" id="asasname_upload_item">
                                                        <div class="col-md-12 file_box">
                                                            <div class="image_box">
                                                                <img src="images/upload-icon.png">
                                                            </div>
                                                            <span>تصویر اساسنامه</span>
                                                            <input type="file" name="asasname_upload_file" id="asasname_upload_file" class="upload_input_file">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 upload_item" id="newspaper_upload_item">
                                                        <div class="col-md-12 file_box">
                                                            <div class="image_box">
                                                                <img src="images/upload-icon.png">
                                                            </div>
                                                            <span>تصویر روزنامه</span>
                                                            <input type="file" name="newspaper_upload_file" id="newspaper_upload_file" class="upload_input_file">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 upload_item" id="company_letter_upload_item">
                                                        <div class="col-md-12 file_box">
                                                            <div class="image_box">
                                                                <img src="images/upload-icon.png">
                                                            </div>
                                                            <span>تصویر شرکت نامه</span>
                                                            <input type="file" name="company_letter_upload_file" id="company_letter_upload_file" class="upload_input_file">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <div id="step-2">
                                    <div class="col-md-12 col-sm-12 col-xs-12 member_card_box">
                                        <h2 class="StepTitle"> اعضای هیئت مدیره</h2>
                                        <div style="clear: both"></div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                            <div class="well profile_view">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                                        <img src="images/alireza.jpg" alt="" class="col-md-6 col-md-offset-3 img-circle img-responsive">
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <h2 style="text-align: center; margin-bottom: 20px;">علیرضا مرادی</h2>
                                                        <p><strong>تحصیلات: </strong> مهندسی برق </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="fa fa-building"></i> کد ملی: ۴۱۲۰۳۳۶۵۸۱</li>
                                                            <li><i class="fa fa-phone"></i> تلفن : ۰۹۳۷۶۲۳۵۶۴۴</li>
                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12 bottom text-center">
                                                    <div class="col-xs-12 col-sm-8 title_position_card emphasis">
                                                        <h4 class="brief"><i>مدیر عامل</i></h4>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 edit_buttons emphasis">
                                                        <button type="button" title="حذف کردن" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <button type="button" title="مشاهده پروفایل" class="btn btn-success btn-xs">
                                                            <i class="fa fa-eye"></i>
                                                        </button>
                                                        <button type="button" title="ویرایش اطلاعات" class="btn btn-warning btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                            <div class="well profile_view">
                                                <div class="col-sm-12">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                                        <img src="images/bahman.jpg" alt="" class="col-md-6 col-md-offset-3 img-circle img-responsive">
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <h2 style="text-align: center; margin-bottom: 20px;">بهمن علیدادی</h2>
                                                        <p><strong>تحصیلات: </strong> مهندسی کامپیوتر</p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="fa fa-building"></i> کد ملی: ۴۱۲۰۳۳۶۵۸۱</li>
                                                            <li><i class="fa fa-phone"></i> تلفن : ۰۹۳۷۶۲۳۵۶۴۴</li>
                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12 bottom text-center">
                                                    <div class="col-xs-12 col-sm-8 emphasis title_position_card">
                                                        <h4 class="brief"><i>عضو هیئت مدیره</i></h4>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 emphasis edit_buttons">
                                                        <button type="button" title="حذف کردن" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <button type="button" title="مشاهده پروفایل" class="btn btn-success btn-xs">
                                                            <i class="fa fa-eye"></i>
                                                        </button>
                                                        <button type="button" title="ویرایش اطلاعات" class="btn btn-warning btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 profile_details" style="clear:none">
                                            <div class="well col-md-12 profile_view">
                                                <div class="col-sm-12" style="height: 230px;">
                                                    <i class="fa fa-plus add_user"></i>
                                                </div>
                                                <div class="col-xs-12 bottom text-center" style="height: 44px;">
                                                    <h2>+ افزودن عضو جدید</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="add_user_form" style="padding: 0">
                                        <h2 class="StepTitle"> افزودن عضو جدید به هیئت مدیره</h2>
                                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first_name">نام
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="first_name" name="first_name" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last_name">نام خانوادگی
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="last_name" name="last_name" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="father_name">نام پدر
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="father_name" name="father_name" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="birth_date">تاریخ تولد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="birth_date" name="birth_date" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4">جنسیت</label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="flat" checked name="gender"> آقا
                                                            </label>
                                                            <label>
                                                                <input type="radio" class="flat" name="gender"> خانم
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="national_code">کد ملی
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="national_code" name="national_code" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="id_number">شماره شناسنامه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="id_number" name="id_number" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="place_of_issue">محل صدور
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="place_of_issue" name="place_of_issue" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="phone">تلفن ثابت
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="phone" name="phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="mobile">تلفن همراه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="mobile" name="mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="province" class="control-label col-md-4 col-sm-4 col-xs-12">مدرک تحصیلی</label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <div class="btn-group col-md-12" style="padding: 0">
                                                            <button data-toggle="dropdown" style="text-align: right" class="col-md-12 btn btn-default dropdown-toggle" type="button"> انتخاب کنید <span class="caret pull-left" style="margin-top: 8px;"></span> </button>
                                                            <ul class="dropdown-menu col-md-12" style="padding: 0">
                                                                <li><a href="#">دکترا</a>
                                                                </li>
                                                                <li><a href="#">کارشناسی ارشد</a>
                                                                </li>
                                                                <li><a href="#">کارشناسی</a>
                                                                </li>
                                                                <li><a href="#">کاردانی</a>
                                                                </li>
                                                                <li><a href="#">دیپلم</a>
                                                                </li>
                                                                <li><a href="#">زیر دیپلم</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="major">رشته تحصیلی
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="major" name="major" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4">وضعیت رشته تحصیلی</label>
                                                    <div class="col-md-8 col-sm-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="flat" checked name="major_status"> مرتبط
                                                            </label>
                                                            <label>
                                                                <input type="radio" class="flat" name="major_status"> نیمه مرتبط
                                                            </label>
                                                            <label>
                                                                <input type="radio" class="flat" name="major_status"> غیر مرتبط
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="work_place">محل کار
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="work_place" name="work_place" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4">سمت در شرکت</label>
                                                    <div class="col-md-8 col-sm-8">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="flat" checked name="position"> عضو هیئت مدیره
                                                            </label>
                                                            <label>
                                                                <input type="radio" class="flat" name="position"> مدیر عامل
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="start_work_date">تاریخ شروع به کار
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="start_work_date" name="start_work_date" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_date">تاریخ ترک کار
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="end_work_date" name="end_work_date" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="experience">سابقه کار
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="experience" name="experience" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                                            <p>مدارک مربوطه زیر را پلود کنید</p>
                                            <form action="">
                                                <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                    <div class="col-md-7 col-sm-12 col-xs-12 sub_upload_box">
                                                        <p>تصاویر صفحات شماسنامه</p>
                                                        <div class="col-md-4 upload_item" id="birth_certificate_1_upload_item">
                                                            <div class="col-md-12 file_box">
                                                                <div class="image_box">
                                                                    <img src="images/upload-icon.png">
                                                                </div>
                                                                <span>صفحه اول</span>
                                                                <input type="file" name="birth_certificate_1_upload_file" id="birth_certificate_1_upload_file" class="upload_input_file">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 upload_item" id="birth_certificate_2_upload_item">
                                                            <div class="col-md-12 file_box">
                                                                <div class="image_box">
                                                                    <img src="images/upload-icon.png">
                                                                </div>
                                                                <span>صفحه دوم</span>
                                                                <input type="file" name="birth_certificate_2_upload_file" id="birth_certificate_2_upload_file" class="upload_input_file">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 upload_item" id="birth_certificate_3_upload_item">
                                                            <div class="col-md-12 file_box">
                                                                <div class="image_box">
                                                                    <img src="images/upload-icon.png">
                                                                </div>
                                                                <span>صفحه سوم</span>
                                                                <input type="file" name="birth_certificate_3_upload_file" id="birth_certificate_3_upload_file" class="upload_input_file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-12 col-xs 12">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 sub_upload_box">
                                                            <p>تصاویر کارت ملی</p>
                                                            <div class="col-md-6 upload_item" id="id_card_front_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>روی کارت</span>
                                                                    <input type="file" name="id_card_front_upload_file" id="id_card_front_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 upload_item" id="id_card_back_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>پشت کارت</span>
                                                                    <input type="file" name="id_card_back_upload_file" id="id_card_back_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-12 col-xs-12 sub_upload_box" style="margin-top: 20px">
                                                        <p>تصاویر مدرک تحصیلی</p>
                                                        <div class="col-md-4 upload_item" id="certificate_1_upload_item">
                                                            <div class="col-md-12 file_box">
                                                                <div class="image_box">
                                                                    <img src="images/upload-icon.png">
                                                                </div>
                                                                <span>مدرک تحصیلی ۱</span>
                                                                <input type="file" name="certificate_1_upload_file" id="certificate_1_upload_file" class="upload_input_file">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 upload_item" id="certificate_2_upload_item">
                                                            <div class="col-md-12 file_box">
                                                                <div class="image_box">
                                                                    <img src="images/upload-icon.png">
                                                                </div>
                                                                <span>مدرک تحصیلی ۲</span>
                                                                <input type="file" name="certificate_2_upload_file" id="certificate_2_upload_file" class="upload_input_file">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 upload_item" id="certificate_3_upload_item">
                                                            <div class="col-md-12 file_box">
                                                                <div class="image_box">
                                                                    <img src="images/upload-icon.png">
                                                                </div>
                                                                <span>مدرک تحصیلی ۳</span>
                                                                <input type="file" name="certificate_3_upload_file" id="certificate_3_upload_file" class="upload_input_file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-12 col-xs 12" style="margin-top: 20px;">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 sub_upload_box">
                                                            <p>تصویر شخص</p>
                                                            <div class="col-md-6 upload_item" id="avatar_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>تصویر شخص</span>
                                                                    <input type="file" name="avatar_upload_file" id="avatar_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div style="clear: both;margin-bottom: 50px;"></div>
                                        <div class="StepTitle" style="float:right;text-align:right;font-size:15px;margin-bottom: 10px;margin-top: 10px;">گواهینامه های کسب شده</div>
                                        <button type="button" id="add_certificate_manager_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-top: 0px;font-size: 15px;">
                                            +                                                 اضافه کردن گواهینامه
                                        </button>
                                        <div style="clear: both;"></div>
                                        <div class="table-responsive" id="certificate_manager_table">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                <tr class="headings">
                                                    <th>
                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                    </th>
                                                    <th class="column-title" style="display: table-cell;">عنوان دوره</th>
                                                    <th class="column-title" style="display: table-cell;">تاریخ شروع دوره</th>
                                                    <th class="column-title" style="display: table-cell;">تاریخ پایان دوره</th>
                                                    <th class="column-title" style="display: table-cell;">تعداد ساعت</th>
                                                    <th class="column-title" style="display: table-cell;">نمره</th>
                                                    <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">ویرایش</span>
                                                    </th>
                                                    <th class="bulk-actions" colspan="7" style="display: none;">
                                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                        </td>
                                                        <td class=" ">گرافیک</td>
                                                        <td class=" ">۱۳۹۶/۰۱/۱۸</td>
                                                        <td class=" ">۱۳۹۶/۰۴/۲۰</td>
                                                        <td class=" ">۲۰</td>
                                                        <td class=" ">۸۵</td>
                                                        <td class=" last"><a href="#">ویرایش</a>
                                                        </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                        </td>
                                                        <td class=" ">برنامه نویسی</td>
                                                        <td class=" ">۱۳۹۶/۰۱/۱۸</td>
                                                        <td class=" ">۱۳۹۶/۰۴/۲۰</td>
                                                        <td class=" ">۲۰</td>
                                                        <td class=" ">۸۵</td>
                                                        <td class=" last"><a href="#">ویرایش</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <button type="button" id="save_add_user_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 15px;margin-top: 40px;font-size: 15px;">
                                            +                                                 اضافه کردن به اعضای هیئت مدیره
                                        </button>
                                    </div>
                                    <div class="col-md-12" id="certificate_manager_form" style="padding: 0">
                                        <h2 class="StepTitle">افزودن گواهینامه</h2>
                                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">عنوان دوره
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">تاریخ شروع دوره
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">تاریخ پایان دوره
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">تعداد ساعت
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">نمره
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;" id="certificate_jobs_upload_box">
                                                <p style="float: right;margin-top: 10px;color: #e91e63;">گواهینامه طرح طبقه بندی مشاغل را آپلود کنید</p>
                                                <button type="button" id="not_certificate_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="font-size: 15px;">
                                                    گواهینامه طرح طبقه بندی مشاغل ندارد
                                                </button>
                                                <form action="">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                        <div class="col-md-7 col-sm-12 col-xs-12">
                                                            <div class="col-md-4 upload_item" id="certificate_jobs_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>تصویر گواهینامه</span>
                                                                    <input type="file" name="certificate_jobs_upload_file" id="certificate_jobs_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-12 upload_title_box" id="bill_upload_box" style="color: #1ABB9C;margin-top: 50px;">
                                                <p style="float: right;margin-top: 10px;color: #e91e63;">
                                                    فیش واریزی را آپلود نمایید
                                                </p>

                                                <button type="button" id="have_certificate_button" class="btn btn-success col-md-4 btn-lg pull-left" style="font-size: 15px;">
                                                    گواهینامه طرح طبقه بندی مشاغل دارد
                                                </button>
                                                <form action="">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                        <div class="col-md-7 col-sm-12 col-xs-12">
                                                            <div class="col-md-4 upload_item" id="bill_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>تصویر فیش واریزی</span>
                                                                    <input type="file" name="bill_upload_file" id="bill_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </form>

                                        <button type="button" id="save_certificate_manager_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 30px;margin-top: 30px;font-size: 15px;">
                                            +                                                 اضافه کردن به لیست گواهینامه ها
                                        </button>
                                    </div>

                                </div>
                                <div id="step-3">
                                    <div class="col-md-12 colsm-12 col-xs-12" id="central_office">
                                        <div class="col-md-12 col-sm-12 col-xs-12" id="summary_office_info">
                                            <h2 class="StepTitle">اطلاعات دفتر مرکزی</h2>
                                            <div class="col-md-12" id="central_office_form" style="padding: 0">
                                                <div class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">مساحت ملکی
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">کاربری
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">پلاک ثبتی
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">طبقه
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">تلفن ثابت
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">تلفن همراه
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">کد پستی
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_postal_code" name="office_postal_code" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_address">آدرس
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="office_address" name="office_address" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_number">شماره  پایان کار - عدم خلاف
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="end_work_number" name="end_work_number" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_date">تاریخ پایان کار - عدم خلاف
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="end_work_date" name="end_work_date" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_number">شماره  سند مالکیت
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="evidence_number" name="evidence_number" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_date">تاریخ  سند مالکیت
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="evidence_date" name="evidence_date" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_number">شماره اجاره نامه محضری
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="rental_number" name="rental_number" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_date">تاریخ اجاره نامه محضری
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="rental_date" name="rental_date" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_code">کد رهگیری استیجاری
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="rental_code" name="rental_code" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_duration">مدت اجاره
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="rent_duration" name="rent_duration" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_start">تاریخ شروع اجاره
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="rent_start" name="rent_start" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تاریخ خاتمه اجاره
                                                            </label>
                                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                                <input disabled=disabled type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                                                    <p>مدارک مربوطه آپلود شده</p>
                                                    <form action="">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                            <div class="col-md-7 col-sm-12 col-xs-12">
                                                                <div class="col-md-4 upload_item" id="koruki_upload_item">
                                                                    <div class="col-md-12 file_box">
                                                                        <div class="image_box">
                                                                            <img src="images/upload-icon.png">
                                                                        </div>
                                                                        <span>تصویر کروکی</span>
                                                                        <input disabled=disabled type="file" name="koruki_upload_file" id="koruki_upload_file" class="upload_input_file">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 upload_item" id="rental_upload_item">
                                                                    <div class="col-md-12 file_box">
                                                                        <div class="image_box">
                                                                            <img src="images/upload-icon.png">
                                                                        </div>
                                                                        <span>تصویر اجاره نامه</span>
                                                                        <input disabled=disabled type="file" name="rental_upload_file" id="rental_upload_file" class="upload_input_file">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 upload_item" id="evidence_upload_item">
                                                                    <div class="col-md-12 file_box">
                                                                        <div class="image_box">
                                                                            <img src="images/upload-icon.png">
                                                                        </div>
                                                                        <span>تصویر سند مالکیت</span>
                                                                        <input disabled=disabled type="file" name="evidence_upload_file" id="evidence_upload_file" class="upload_input_file">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <button type="button" id="edit_office_info" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 20px;font-size: 15px;">
                                                    ویرایش اطلاعات
                                                </button>
                                                <div class="col-md-12" style="padding: 0;" id="employee_view">
                                                    <div class="StepTitle" style="text-align:right;font-size:15px;margin-bottom: 10px;margin-top: 20px;">اطلاعات کارمندان دفتر مرکزی</div>
                                                    <div style="clear: both"></div>
                                                    <div id="col-md-12 col-xs-12 col-sm-12 central_office_employee_table">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped jambo_table bulk_action">
                                                                <thead>
                                                                <tr class="headings">
                                                                    <th>
                                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </th>
                                                                    <th class="column-title" style="display: table-cell;">نام </th>
                                                                    <th class="column-title" style="display: table-cell;">نام خانوادگی </th>
                                                                    <th class="column-title" style="display: table-cell;">گروه شغلی </th>
                                                                    <th class="column-title" style="display: table-cell;">عنوان شغل </th>
                                                                    <th class="column-title" style="display: table-cell;">تاریخ شروع قرارداد </th>
                                                                    <th class="column-title" style="display: table-cell;">حقوق (تومان)</th>
                                                                    <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">ویرایش</span>
                                                                    </th>
                                                                    <th class="bulk-actions" colspan="7" style="display: none;">
                                                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                                                    </th>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                <tr class="even pointer">
                                                                    <td class="a-center ">
                                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </td>
                                                                    <td class=" ">علیرضا</td>
                                                                    <td class=" ">مرادی</td>
                                                                    <td class=" ">تیم فنی</td>
                                                                    <td class=" ">طراح گرافیک</td>
                                                                    <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                                                    <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                                                    <td class=" last"><a href="#">ویرایش</a>
                                                                    </td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="a-center ">
                                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </td>
                                                                    <td class=" ">بهمن</td>
                                                                    <td class=" ">علیدادی</td>
                                                                    <td class=" ">تیم فنی</td>
                                                                    <td class=" ">برنامه نویس و طراح وب</td>
                                                                    <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                                                    <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                                                    <td class=" last"><a href="#">ویرایش</a>
                                                                    </td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="a-center ">
                                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </td>
                                                                    <td class=" ">علیرضا</td>
                                                                    <td class=" ">مرادی</td>
                                                                    <td class=" ">تیم فنی</td>
                                                                    <td class=" ">طراح گرافیک</td>
                                                                    <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                                                    <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                                                    <td class=" last"><a href="#">ویرایش</a>
                                                                    </td>
                                                                </tr>
                                                                <tr class="even pointer">
                                                                    <td class="a-center ">
                                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                                    </td>
                                                                    <td class=" ">بهمن</td>
                                                                    <td class=" ">علیدادی</td>
                                                                    <td class=" ">تیم فنی</td>
                                                                    <td class=" ">برنامه نویس و طراح وب</td>
                                                                    <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                                                    <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                                                    <td class=" last"><a href="#">ویرایش</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <button type="button" id="add_employee_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
                                                        +                                                 اضافه کردن کارمند جدید
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="central_office_edit_form" style="padding: 0;display:none;">
                                            <h2 class="StepTitle">ویرایش اطلاعات دفتر مرکزی</h2>
                                            <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">مساحت ملکی
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">کاربری
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">پلاک ثبتی
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">طبقه
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">تلفن ثابت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">تلفن همراه
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">کد پستی
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_postal_code" name="office_postal_code" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_address">آدرس
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_address" name="office_address" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_number">شماره  پایان کار - عدم خلاف
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="end_work_number" name="end_work_number" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_date">تاریخ پایان کار - عدم خلاف
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="end_work_date" name="end_work_date" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_number">شماره  سند مالکیت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="evidence_number" name="evidence_number" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_date">تاریخ  سند مالکیت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="evidence_date" name="evidence_date" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_number">شماره اجاره نامه محضری
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rental_number" name="rental_number" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_date">تاریخ اجاره نامه محضری
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rental_date" name="rental_date" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_code">کد رهگیری استیجاری
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rental_code" name="rental_code" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_duration">مدت اجاره
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_duration" name="rent_duration" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_start">تاریخ شروع اجاره
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_start" name="rent_start" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تاریخ خاتمه اجاره
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                                                <p>مدارک مربوطه زیر را پلود کنید</p>
                                                <form action="">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                        <div class="col-md-7 col-sm-12 col-xs-12">
                                                            <div class="col-md-4 upload_item" id="koruki_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>تصویر کروکی</span>
                                                                    <input type="file" name="koruki_upload_file" id="koruki_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="rental_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>تصویر اجاره نامه</span>
                                                                    <input type="file" name="rental_upload_file" id="rental_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 upload_item" id="evidence_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>تصویر سند مالکیت</span>
                                                                    <input type="file" name="evidence_upload_file" id="evidence_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <button type="button" id="save_office_info" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 40px;font-size: 15px;">
                                            ذخیره اطلاعات
                                            </button>
                                        </div>
                                        <div class="col-md-12" id="central_office_employee_form" style="padding: 0">
                                            <h2 class="StepTitle">افزودن کارمند جدید</h2>
                                            <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">نام
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">نام خانوادگی
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">کد ملی
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">نام پدر
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">شماره شناسنامه
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">محل صدور
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">تاریخ تولد
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_postal_code" name="office_postal_code" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_address">وضعیت تاهل
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="office_address" name="office_address" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_number">تعداد اولاد
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="end_work_number" name="end_work_number" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div><div class="form-group">
                                                        <label for="province" class="control-label col-md-4 col-sm-4 col-xs-12">مدرک تحصیلی</label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <div class="btn-group col-md-12" style="padding: 0">
                                                                <button data-toggle="dropdown" style="text-align: right" class="col-md-12 btn btn-default dropdown-toggle" type="button"> انتخاب کنید <span class="caret pull-left" style="margin-top: 8px;"></span> </button>
                                                                <ul class="dropdown-menu col-md-12" style="padding: 0">
                                                                    <li><a href="#">دکترا</a>
                                                                    </li>
                                                                    <li><a href="#">کارشناسی ارشد</a>
                                                                    </li>
                                                                    <li><a href="#">کارشناسی</a>
                                                                    </li>
                                                                    <li><a href="#">کاردانی</a>
                                                                    </li>
                                                                    <li><a href="#">دیپلم</a>
                                                                    </li>
                                                                    <li><a href="#">زیر دیپلم</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_number">وضعیت خدمت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="evidence_number" name="evidence_number" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_date">تاریخ  شروع به کار
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="evidence_date" name="evidence_date" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_number">تاریخ ترک کار
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rental_number" name="rental_number" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_date">سابقه کار
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rental_date" name="rental_date" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_code">عنوان شغل
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rental_code" name="rental_code" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_duration">گروه شغلی
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_duration" name="rent_duration" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_start">جمع مزد مبنا - ماهیانه ۳۰ روز
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_start" name="rent_start" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">مدت قرارداد از تاریخ
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">پایان خدمت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">به مدت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">عادی کار از ساعت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تا ساعت
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">جمع ساعت در هفته
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">نوبت کار
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">سایر حالات
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تعطیلات هفته
                                                        </label>
                                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                                                <p>مدارک مربوطه زیر را پلود کنید</p>
                                                <form action="">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                        <div class="col-md-7 col-sm-12 col-xs-12">
                                                            <div class="col-md-4 upload_item" id="office_employee_contract_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>تصویر قرارداد</span>
                                                                    <input type="file" name="office_employee_contract_upload_file" id="office_employee_contract_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <button type="button" id="save_add_employee_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 15px;margin-top: 40px;font-size: 15px;">
                                                +                                                 اضافه کردن به کارمندان شرکت
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-4">
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="treaty_view_info">
                                        <h2 class="StepTitle">اطلاعات مربوط به پیمان ها</h2>
                                        <div class="table-responsive" id="treaty_table">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                <tr class="headings">
                                                    <th>
                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                    </th>
                                                    <th class="column-title" style="display: table-cell;">شماره پیمان</th>
                                                    <th class="column-title" style="display: table-cell;">موضوع قرارداد</th>
                                                    <th class="column-title" style="display: table-cell;">تاریخ قرارداد</th>
                                                    <th class="column-title" style="display: table-cell;">تعداد افراد شاغل در پیمان</th>
                                                    <th class="column-title" style="display: table-cell;">مدت قرارداد</th>
                                                    <th class="column-title" style="display: table-cell;">مبلغ پیمان</th>
                                                    <th class="column-title" style="display: table-cell;">مبلغ نهایی پیمان</th>
                                                    <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">ویرایش</span>
                                                    </th>
                                                    <th class="bulk-actions" colspan="7" style="display: none;">
                                                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr class="even pointer">
                                                    <td class="a-center ">
                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                    </td>
                                                    <td class=" ">۱۲۳۴</td>
                                                    <td class=" ">پیمان تست</td>
                                                    <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                                    <td class=" ">۱۳</td>
                                                    <td class=" ">۲۰ ماه</td>
                                                    <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                                    <td class="a-right a-right ">۱,۸۰۰,۰۰۰</td>
                                                    <td class=" last"><a href="#">ویرایش</a></td>
                                                </tr>

                                                <tr class="even pointer">
                                                    <td class="a-center ">
                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                    </td>
                                                    <td class=" ">۱۲۳۴</td>
                                                    <td class=" ">پیمان تست</td>
                                                    <td class=" ">۱۳۶۹/۰۲/۱۱</td>
                                                    <td class=" ">۵</td>
                                                    <td class=" ">۱۲ ماه</td>
                                                    <td class="a-right a-right ">۳,۰۰۰,۰۰۰</td>
                                                    <td class="a-right a-right ">۲,۸۰۰,۰۰۰</td>
                                                    <td class=" last"><a href="#">ویرایش</a></td>
                                                </tr>

                                                <tr class="even pointer">
                                                    <td class="a-center ">
                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                    </td>
                                                    <td class=" ">۱۲۳۴</td>
                                                    <td class=" ">پیمان تست</td>
                                                    <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                                    <td class=" ">۱۳</td>
                                                    <td class=" ">۲۰ ماه</td>
                                                    <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                                    <td class="a-right a-right ">۱,۸۰۰,۰۰۰</td>
                                                    <td class=" last"><a href="#">ویرایش</a></td>
                                                </tr>

                                                <tr class="even pointer">
                                                    <td class="a-center ">
                                                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                    </td>
                                                    <td class=" ">۱۲۳۴</td>
                                                    <td class=" ">پیمان تست</td>
                                                    <td class=" ">۱۳۶۹/۰۲/۱۱</td>
                                                    <td class=" ">۵</td>
                                                    <td class=" ">۱۲ ماه</td>
                                                    <td class="a-right a-right ">۳,۰۰۰,۰۰۰</td>
                                                    <td class="a-right a-right ">۲,۸۰۰,۰۰۰</td>
                                                    <td class=" last"><a href="#">ویرایش</a></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="button" id="add_treaty_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 0px;font-size: 15px;">
                                            +                                                 اضافه کردن پیمان جدید
                                        </button>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="treaty_form">
                                        <h2 class="StepTitle">اطلاعات مربوط به پیمان</h2>
                                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_number">شماره پیمان
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_number" name="treaty_number" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_employees_number">تعداد افراد شاغل در پیمان
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_employees_number" name="treaty_employees_number" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_date">تاریخ قرارداد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_date" name="treaty_date" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_workhouse_code">کد کارگاه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_workhouse_code" name="treaty_workhouse_code" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_subject">موضوع قرارداد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_subject" name="treaty_subject" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_summary">خلاصه موضوع پیمان
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_summary" name="treaty_summary" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_price">مبلغ پیمان
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_price" name="treaty_price" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_total_price">مبلغ نهایی پیمان
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_total_price" name="treaty_total_price" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_duration">مدت قرارداد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_duration" name="treaty_duration" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_start_date">شروع قرارداد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_start_date" name="treaty_start_date" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_end_date">خاتمه قرارداد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_end_date" name="treaty_end_date" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_place">محل پیمان
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="treaty_place" name="treaty_place" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                                                <p>مدارک مربوطه زیر را پلود کنید</p>
                                                <form action="">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                        <div class="col-md-7 col-sm-12 col-xs-12">
                                                            <div class="col-md-4 upload_item" id="treaty_Insurance_list_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>لیست بیمه</span>
                                                                    <input type="file" name="treaty_Insurance_list_upload_file" id="treaty_Insurance_list_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        <div class="col-md-4 upload_item" id="treaty_salary_list_upload_item">
                                                                <div class="col-md-12 file_box">
                                                                    <div class="image_box">
                                                                        <img src="images/upload-icon.png">
                                                                    </div>
                                                                    <span>لیست حقوق</span>
                                                                    <input type="file" name="treaty_salary_list_upload_file" id="treaty_salary_list_upload_file" class="upload_input_file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div style="clear: both;margin-bottom: 50px;"></div>
                                            <div class="StepTitle" style="float:right;text-align:right;font-size:15px;margin-bottom: 10px;margin-top: 10px;">اطلاعات کارمندان مشغول در پیمان</div>
                                            <button type="button" id="add_employee_treaty_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-top: 0px;font-size: 15px;">
                                                +                                                 اضافه کردن کارمند جدید به پیمان
                                            </button>
                                            <div style="clear: both;"></div>
                                            <div class="table-responsive" id="treaty_employees_table">
                                                <table class="table table-striped jambo_table bulk_action">
                                                    <thead>
                                                    <tr class="headings">
                                                        <th>
                                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                        </th>
                                                        <th class="column-title" style="display: table-cell;">کد بیمه کارگاه</th>
                                                        <th class="column-title" style="display: table-cell;">نام </th>
                                                        <th class="column-title" style="display: table-cell;">نام خانوادگی </th>
                                                        <th class="column-title" style="display: table-cell;">کد ملی</th>
                                                        <th class="column-title" style="display: table-cell;">شماره بیمه</th>
                                                        <th class="column-title" style="display: table-cell;">نام ارگان</th>
                                                        <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">ویرایش</span>
                                                        </th>
                                                        <th class="bulk-actions" colspan="7" style="display: none;">
                                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                        <tr class="even pointer">
                                                            <td class="a-center ">
                                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                            </td>
                                                            <td class=" ">۱۳۴</td>
                                                            <td class=" ">علیرضا</td>
                                                            <td class=" ">مرادی</td>
                                                            <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                                                            <td class=" ">۱۳۴۲۳۴</td>
                                                            <td class=" ">تامین اجتماعی</td>
                                                            <td class=" last"><a href="#">ویرایش</a>
                                                            </td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                            <td class="a-center ">
                                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                            </td>
                                                            <td class=" ">۱۵۴</td>
                                                            <td class=" ">بهمن</td>
                                                            <td class=" ">علیدادی</td>
                                                            <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                                                            <td class=" ">۱۳۴۲۳۴</td>
                                                            <td class=" ">تامین اجتماعی</td>
                                                            <td class=" last"><a href="#">ویرایش</a>
                                                            </td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                            <td class="a-center ">
                                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                            </td>
                                                            <td class=" ">۱۳۴</td>
                                                            <td class=" ">علیرضا</td>
                                                            <td class=" ">مرادی</td>
                                                            <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                                                            <td class=" ">۱۳۴۲۳۴</td>
                                                            <td class=" ">تامین اجتماعی</td>
                                                            <td class=" last"><a href="#">ویرایش</a>
                                                            </td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                            <td class="a-center ">
                                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                            </td>
                                                            <td class=" ">۱۵۴</td>
                                                            <td class=" ">بهمن</td>
                                                            <td class=" ">علیدادی</td>
                                                            <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                                                            <td class=" ">۱۳۴۲۳۴</td>
                                                            <td class=" ">تامین اجتماعی</td>
                                                            <td class=" last"><a href="#">ویرایش</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <button type="button" id="save_treaty_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 20px;font-size: 15px;">
                                                ذخیره پیمان جدید
                                            </button>
                                        </form>
                                    </div>
                                    <div class="col-md-12" id="treaty_employee_form" style="padding: 0">
                                        <h2 class="StepTitle">افزودن کارمند جدید به پیمان</h2>
                                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">نام
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">نام خانوادگی
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">کد ملی
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">کد بیمه کارگاه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">شماره شناسنامه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">سریال شناسنامه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">نام ارگان
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">شماره بیمه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_postal_code" name="office_postal_code" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                                            <p>مدارک مربوطه زیر را پلود کنید</p>
                                            <form action="">
                                                <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                                                    <div class="col-md-7 col-sm-12 col-xs-12">
                                                        <div class="col-md-4 upload_item" id="treaty_employee_contract_upload_item">
                                                            <div class="col-md-12 file_box">
                                                                <div class="image_box">
                                                                    <img src="images/upload-icon.png">
                                                                </div>
                                                                <span>تصویر قرارداد</span>
                                                                <input type="file" name="treaty_employee_contract_upload_file" id="treaty_employee_contract_upload_file" class="upload_input_file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <button type="button" id="save_treaty_employee_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 15px;margin-top: 40px;font-size: 15px;">
                                            +                                                 اضافه کردن به کارمندان پیمان
                                        </button>
                                    </div>

                                </div>
                                <div id="step-5">
                                    <div class="col-md-12" style="padding: 0;" id="equipment_view">
                                        <h2 class="StepTitle">لیست امکانات و تجهیزات</h2>
                                        <div style="clear: both"></div>
                                        <div id="col-md-12 col-xs-12 col-sm-12 equipment_table">
                                            <div class="table-responsive">
                                                <table class="table table-striped jambo_table bulk_action">
                                                    <thead>
                                                    <tr class="headings">
                                                        <th>
                                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                        </th>
                                                        <th class="column-title" style="display: table-cell;">عنوان</th>
                                                        <th class="column-title" style="display: table-cell;">مدل</th>
                                                        <th class="column-title" style="display: table-cell;">شماره سریال</th>
                                                        <th class="column-title" style="display: table-cell;">تعداد</th>
                                                        <th class="column-title" style="display: table-cell;">گروه مرتبط</th>
                                                        <th class="column-title" style="display: table-cell;">شماره سند یا فاکتور خرید</th>
                                                        <th class="column-title" style="display: table-cell;">تاریخ</th>
                                                        <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">ویرایش</span>
                                                        </th>
                                                        <th class="bulk-actions" colspan="7" style="display: none;">
                                                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                        </td>
                                                        <td class=" ">لپ تاپ</td>
                                                        <td class=" ">macbook pro </td>
                                                        <td class=" ">۳۲۴۲۳۴۱۲۱۲۳</td>
                                                        <td class=" ">۲۱</td>
                                                        <td class=" ">IT</td>
                                                        <td class=" ">۲۱۳۴</td>
                                                        <td class=" ">۱۳۹۶/۰۴/۱۲</td>
                                                        <td class=" last"><a href="#">ویرایش</a>
                                                        </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                        </td>
                                                        <td class=" ">لپ تاپ</td>
                                                        <td class=" ">macbook Air </td>
                                                        <td class=" ">۵۶۷۳۴۲۳۴۲</td>
                                                        <td class=" ">۱۰</td>
                                                        <td class=" ">IT</td>
                                                        <td class=" ">۶۵۴۵</td>
                                                        <td class=" ">۱۳۹۶/۰۶/۲۹</td>
                                                        <td class=" last"><a href="#">ویرایش</a>
                                                        </td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <button type="button" id="add_equipment_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
                                            +                                                 اضافه کردن تجهیزات جدید
                                        </button>
                                    </div>
                                    <div class="col-md-12" id="equipment_form" style="padding: 0">
                                        <h2 class="StepTitle">افزودن تجهیزات جدید </h2>
                                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">عنوان امکانات یا تجهیزات
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">مدل
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">شماره سریال
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">تعداد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">شماره سند یا فاکتور خرید
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">گروه مرتبط
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">تاریخ
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <button type="button" id="save_equipment_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
                                            +                                                 اضافه کردن به لیست امکانات و تجهیزات
                                        </button>
                                    </div>
                                </div>
                                <div id="step-6">
                                    <div class="col-md-12" style="padding: 0;" id="jazb_shohada_view">
                                        <h2 class="StepTitle">لیست جذب خانواده شهدا، ایثارگران و معلولین غیر جنگی</h2>
                                        <div style="clear: both"></div>
                                        <div id="col-md-12 col-xs-12 col-sm-12 jazb_shohada_table">
                                            <div class="table-responsive">
                                                <table class="table table-striped jambo_table bulk_action">
                                                    <thead>
                                                        <tr class="headings">
                                                            <th>
                                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                            </th>
                                                            <th class="column-title" style="display: table-cell;">نام</th>
                                                            <th class="column-title" style="display: table-cell;">نام خانوادگی</th>
                                                            <th class="column-title" style="display: table-cell;">نوع امتیاز</th>
                                                            <th class="column-title" style="display: table-cell;">درصد</th>
                                                            <th class="column-title" style="display: table-cell;">تاریخ شروع به کار</th>
                                                            <th class="column-title" style="display: table-cell;">سابقه کار به ماه</th>
                                                            <th class="column-title" style="display: table-cell;">شماره و تاریخ گواهی واحد تأیید کننده</th>
                                                            <th class="column-title" style="display: table-cell;">شماره کد کارگاه بیمه</th>
                                                            <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">ویرایش</span>
                                                            </th>
                                                            <th class="bulk-actions" colspan="7" style="display: none;">
                                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                                            </th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <tr class="even pointer">
                                                            <td class="a-center ">
                                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                            </td>
                                                            <td class=" ">علی</td>
                                                            <td class=" ">علوی</td>
                                                            <td class=" ">تست</td>
                                                            <td class=" ">۲۵٪</td>
                                                            <td class=" ">۱۳۹۵/۲/۱۲</td>
                                                            <td class=" ">۲۰</td>
                                                            <td class=" ">۲۱۳۴۲۳۴   </td>
                                                            <td class=" ">۱۲۴۱</td>
                                                            <td class=" last"><a href="#">ویرایش</a>
                                                            </td>
                                                        </tr>
                                                        <tr class="even pointer">
                                                            <td class="a-center ">
                                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                                            </td>
                                                            <td class=" ">مهدی</td>
                                                            <td class=" ">رضایی</td>
                                                            <td class=" ">تست</td>
                                                            <td class=" ">۴۵٪</td>
                                                            <td class=" ">۱۳۹۵/۲/۱۲</td>
                                                            <td class=" ">۲۰</td>
                                                            <td class=" ">۲۱۳۴۲۳۴   </td>
                                                            <td class=" ">۱۲۴۱</td>
                                                            <td class=" last"><a href="#">ویرایش</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <button type="button" id="add_jazb_shohada_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
                                            +                                                 اضافه کردن فرد جدید
                                        </button>
                                    </div>
                                    <div class="col-md-12" id="jazb_shohada_form" style="padding: 0">
                                        <h2 class="StepTitle">افزودن فرد جدید به لیست جذب شهدا، ایثارگران و معلولین غیر جنگی</h2>
                                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">نام
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">نام خانوادگی
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">نوع امتیاز
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">درصد
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">تاریخ شروع به کار
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">سابقه کار ) به ماه (
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">شماره و تاریخ گواهی واحد تأیید کننده
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">شماره کد کارگاه بیمه
                                                    </label>
                                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                                        <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <button type="button" id="save_jazb_shohada_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
                                            +                                                 اضافه کردن به لیست امکانات و تجهیزات
                                        </button>
                                    </div>

                            </div>
                            <!-- End SmartWizard Content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

        <!-- FastClick -->
    <script src="public_vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="public_vendors/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="public_vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <script src="js/script.js"></script>
@endsection