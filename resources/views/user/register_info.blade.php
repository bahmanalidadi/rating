@extends('layouts.user.dashboard_sub_layout')

@section('style')
<link rel="stylesheet" href="/css/persian-datepicker-cheerup.css"/>
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>فرم ثبت اطلاعات</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>فرم ثبت اطلاعات شرکت</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <!-- Smart Wizard -->
                            <p style="padding-bottom: 20px;">
                                این فرم برای ثبت اطلاعات مربوط به شرکت، هیئت مدیره، کارکنان شرکت و ... می باشد
                                <span style="color: #1ABB9C;">
                                    (اطلاعات در هر مرحله ذخیره می شود)
                                </span>
                            </p>
                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a href="#step-1">
                                            <span class="step_no">۱</span>
                            <span class="step_descr">
                                              مرحله ۱<br />
                                              <small>اطلاعات شرکت</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <span class="step_no">۲</span>
                            <span class="step_descr">
                                              مرحله ۲<br />
                                              <small>اعضای هیئت مدیره</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <span class="step_no">۳</span>
                            <span class="step_descr">
                                              مرحله ۳<br />
                                              <small>دفتر مرکزی</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-4">
                                            <span class="step_no">۴</span>
                            <span class="step_descr">
                                              مرحله ۴<br />
                                              <small>پیمان ها</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-5">
                                            <span class="step_no">۵</span>
                            <span class="step_descr">
                                              مرحله ۵<br />
                                              <small>امکانات و تجهیزات</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-6">
                                            <span class="step_no">۶</span>
                            <span class="step_descr">
                                              مرحله ۶<br />
                                              <small>جذب خانواده شهدا و ...</small>
                                          </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="step-1">
                                    @include('subviews.user.register_company_form')
                                </div>
                                <div id="step-2">
                                    @include('subviews.user.register_manager_form')
                                </div>
                                <div id="step-3">
                                    @include('subviews.user.register_central_office_form')
                                </div>
                                <div id="step-4">
                                    @include('subviews.user.register_treaty_form')
                                </div>
                                <div id="step-5">
                                    @include('subviews.user.register_equipment_form')
                                </div>
                                <div id="step-6">
                                    @include('subviews.user.register_jazb_shohada_form')
                                </div>
                            </div>
                            <!-- End SmartWizard Content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/persian-date-0.1.8.min.js"></script>
    <script src="/js/persian-datepicker-0.4.5.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            aadatepicker = $(".date_input").pDatepicker({
                scrollEnabled: false,
                format : "YYYY - MM - DD ",
                navigator: {
                    text: {
                        btnNextText:">",
                        btnPrevText: "<"
                    }
                }

            });

            $(".date_input").attr('dir','auto')
        });

        $('.upload_input_file').change(function(){
            var formData = new FormData();
            formData.append('file', this.files[0]);
            formData.append('_token', $('input[name=_token]').val());
            that = this;

            $.ajax({
                url : '/user/upload-file',
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    file_path = data.file_path;
                    var parent = $(that).parent().get(0);
                    $(parent).children('.upload_file_type').val(data.file_name);
//                    debugger;

                    img_box = $(parent).children('.image_box');
                    $(img_box).children('img').attr('src', data.file_name);
                    $(parent).children('span').css('color', '#1ABB9C');
                },
                error :function(){
                    alert("عکس آپلود شده نا معتبر است");
                }
            });
        });


//    $(document).on('change', '#province', function () {
//
//    })
        $('#province').change( function(e){
            var formData = new FormData();
            formData.append('province_id', $('#province').val());
            formData.append('_token', $('input[name=_token]').val())
            $.ajax({
                url : '/user/get-cities',
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data) {
                    $('#city option').remove();
                    for(var i=0;i<data.cities.length; i++)
                    {
                        var id = data.cities[i].id;
                        var name = data.cities[i].name;

                        str_option = "<option value='"+id+"'>"+name+"</option>";

                        $('#city').append(str_option);

                    }
                },
                error :function(){

                }
            });
        });
    </script>
@endsection