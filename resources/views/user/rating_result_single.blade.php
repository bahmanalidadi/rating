@extends('layouts.user.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        @include('subviews.responsible_company.rating_result_first_step')
        <button type="button" id="accept_rating_button" class="btn btn-success col-md-3 btn-lg pull-left" style="margin-top: 30px;font-size: 15px;"  data-toggle="modal" data-target="#second_inspector_form_modal">
            ثبت اعتراض روی نتایج
        </button>
        <button type="button" id="certificate_rating_button" class="btn btn-success col-md-3 btn-lg pull-left" style="margin-top: 30px;font-size: 15px;"  data-toggle="modal" data-target="#certificate_form_modal">
            نمایش گواهینامه
        </button>
        <!-- second_inspector_form modal -->
        <div id="second_inspector_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">            ثبت اعتراض روی نتایج</h4>
                    </div>
                    <div class="modal-body">
                        @include('subviews.user.protest')
                        <div style="clear:both"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default antoclose" data-dismiss="modal">بستن</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="certificate_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog col-md-10" style="float: none;">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">تایید نتایج رتبه بندی و صدور گواهینامه</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            @include('subviews.responsible_company.certificate')
                        </p>
                        <div style="clear:both"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default antoclose" data-dismiss="modal">چاپ گواهینامه</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>

@endsection

@section('script')
<script type="text/javascript">
    $('#display_rating_details_button').click(function(){
        window.location = "/user/rating-result-details";
    })
    $('input').attr('disabled','disabled')
</script>
@endsection