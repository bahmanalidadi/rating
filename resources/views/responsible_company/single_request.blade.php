@extends('layouts.responsible_company.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>بررسی مدارک</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                بررسی کامل بودن مدارک و انتخاب بازرس
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <!-- start accordion -->
                            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel">
                                    <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel-title">اطلاعات مربوط به شرکت</h4>
                                    </a>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            @include('subviews.user.register_company_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel-title">اطلاعات مربوط به هیئت مدیره</h4>
                                    </a>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            @include('subviews.user.register_manager_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <h4 class="panel-title">اطلاعات مربوط به دفتر مرکزی</h4>
                                    </a>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            @include('subviews.user.register_central_office_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <h4 class="panel-title">اطلاعات مربوط به پیمان ها</h4>
                                    </a>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            @include('subviews.user.register_treaty_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingFive" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <h4 class="panel-title">اطلاعات مربوط به امکانات و تجهیزات</h4>
                                    </a>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                        <div class="panel-body">
                                            @include('subviews.user.register_equipment_form')
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="headingSix" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <h4 class="panel-title">اطلاعات مربوط به جذب خانواده شهدا</h4>
                                    </a>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                        <div class="panel-body">
                                            @include('subviews.user.register_jazb_shohada_form')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of accordion -->
                        </div>
                        <p style="color: #e91e64;margin-top: 30px;margin-bottom: 30px;font-size: 16px;font-weight: bold;">موارد مورد رتبه بندی که در زیر نشانه گذاری شده برای رتبه بندی ارسال شده است</p>
                        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" style="padding-right: 30px;">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat" checked="checked">
                                    کد 1 - امور حمل و نقل شامل : رانندگی و اداره امور نقلیه و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 2 - تعمیر و نگهداری شامل : تعمیر و نگهداری تجهیزات و وسایل اداری (به غیر از امور رایانه)،خدمات فنی خودرو و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 3 - امور آشپزخانه و رستوران شامل : طبخ و توزیع غذا،اداره رستوران و بوفه و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 4 - خدمات عمومی شامل : تنظیفات، نامه رسانی،پیشخدمتی،امور آبدارخانه، خدمات پاسخگویی تلفنی و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 5 - نگهداری و خدمات فضای سبز شامل:خدمات باغبانی و نگهداری فضای سبز و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 6 - امور چاپ و تکثیر شامل:صحافی،حروفچینی،غلط گیری،خطاطی، چاپ و تکثیر و سایر امور فنی و هنری چاپ و موارد مشابه
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="flat">
                                    کد 7 -امور تاسیساتی شامل: تعمیر و نگهداری و بهره برداری از تاسیسات برودتی و حرارتی وموارد مشابه
                                </label>
                            </div>
                        </form>
                        @if($mode == 'view')
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;">
                                <p>
                                    مدارک به ارزیاب مورد انتخاب شما آقای علیرضا مرادی ارسال شد
                                </p>
                            </div>
                            <button type="button" id="second_form_button" class="btn btn-warning col-md-3 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#second_form_modal">
                                فرم ب
                            </button>
                            <button type="button" id="first_form_button" class="btn btn-warning col-md-3 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#first_form_modal">
                                فرم الف
                            </button>
                        @else
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;">
                                <p>بعد از مطالعه اطلاعات شرکت ارزیاب را انتخاب کنید</p>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="btn-group col-md-12" style="padding: 0">
                                            <button data-toggle="dropdown" style="text-align: right" class="assessor_dropdown display-btn dropdown-btn col-md-12 btn btn-default dropdown-toggle" type="button"> انتخاب بازرس <span class="caret pull-left" style="margin-top: 8px;"></span> </button>
                                            <ul class="dropdown-menu col-md-12" style="padding: 0">
                                                <li><a href="#">بهمن علیدادی</a>
                                                </li>
                                                <li><a href="#">علیرضا مرادی</a>
                                                </li>
                                                <li><a href="#">سعید محمودی</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both;margin-bottom: 20px"></div>
                                <button type="button" id="second_inspector_form_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#second_inspector_form_modal">
                                    ارسال مدارک به ارزیاب انتخاب شده
                                </button>
                                <button type="button" id="second_form_button" class="btn btn-warning col-md-3 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#second_form_modal">
                                    تکمیل فرم ب
                                </button>
                                <button type="button" id="first_form_button" class="btn btn-warning col-md-3 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;" data-toggle="modal" data-target="#first_form_modal">
                                    تکمیل فرم الف
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- second_inspector_form modal -->
    <div id="second_inspector_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">ارسال مدارک به ارزیاب</h4>
                </div>
                <div class="modal-body">
                    <p>
                        مدارک به ارزیاب مورد انتخاب شما آقای علیرضا مرادی ارسال شد
                    </p>
                    <div style="clear:both"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">متوجه شدم</button>
                </div>
            </div>
        </div>
    </div>

    <div id="first_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  col-md-10" style="float: none">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">فرم الف ارزیابی</h4>
                </div>
                <div class="modal-body">
                    <p>
                        @include('subviews.responsible_company.first_assessor_form')
                    </p>
                    <div style="clear:both"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">لغو</button>
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">ثبت</button>
                </div>
            </div>
        </div>
    </div>

    <div id="second_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  col-md-10" style="float: none">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">فرم ب ارزیابی</h4>
                </div>
                <div class="modal-body">
                    <p>
                        @include('subviews.responsible_company.second_assessor_form')
                    </p>
                    <div style="clear:both"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">لغو</button>
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">ثبت</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /second_inspector_form modal -->
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('input').attr('disabled', 'disabled');
            $('.edit-form input').removeAttr('disabled');
            $('.modal-footer button').removeAttr('disabled');
            $('.modal-dialog textarea').removeAttr('disabled');
            $('textarea').attr('disabled', 'disabled');
            $('.dropdown-btn').attr('disabled', 'disabled');
            $('button').hide();
            $('.display-btn').show();
            $('#back_to_certificate_list').show();
            $('#back_to_employees_list').show();
            $('#back_to_treaty_employees_list').show();
            $('#back_to_equipment_list').show();
            $('#back_to_jazb_shohada_list').show();
            $('#back_to_treaty_list').show();
            $('#bill_upload_box').show();
            $('.upload_box_title').text('مدارک آپلود شده');
            $('.extra_title').remove();
            $('.member_card_box').children()[$('.member_card_box').children().length - 1].remove()
            $('.edit-btn').hide();
            $('.delete-btn').hide();

            $('#first_inspector_form_button').show();
            $('#second_inspector_form_button').show();
            $('#first_form_button').show();
            $('#second_form_button').show();
            $('.modal-footer button').show();
            $('.modal-dialog textarea').removeAttr('disabled');

            $('.assessor_dropdown').removeAttr('disabled');
            $('#second_form_modal input').removeAttr('disabled')
            $('#first_form_modal input').removeAttr('disabled')

        })
    </script>
@endsection
