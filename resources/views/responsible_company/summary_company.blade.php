@extends("layouts.responsible_company.dashboard_sub_layout")

@section("content")
    <div class="right_col" role="main">

        <div class="container" style="border: 2px solid #000;margin: 70px 0 20px;">
            <div class="col-md-5" style="border-left: 2px solid #000;padding: 0;">
                <div class="col-md-12 summary-title">مشخصات شرکت</div>
                <div class="col-md-6" style="border-bottom: 2px solid #000;">
                    <p>نام شرکت:</p>
                    <p>شماره ثبت:</p>
                    <p>تاریخ ثبت:</p>
                    <p>نوع شرکت:</p>
                    <p>شماره عضویت انجمن:</p>
                    <p>شناسه کلی شرکت:</p>
                </div>
                <div class="col-md-6" style="border-bottom: 2px solid #000;">
                    <p>.</p>
                    <p>.</p>
                    <p>.</p>
                    <p>سهامی خاص</p>
                    <p>.</p>
                    <p>.</p>
                </div>
                <div class="col-md-12 summary-title">مشخصات مدیر عامل</div>
                <div class="col-md-6">
                    <p>کد ملی:</p>
                    <p>نام و نام خانوادگی:</p>
                    <p>جنسیت:</p>
                    <p>شماره شناسنامه:</p>
                    <p>نام پدر:</p>
                    <p>تاریخ تولد:</p>
                    <p>محل تولد:</p>
                    <p>مدرک تحصیلی و رشته:</p>
                </div>
                <div class="col-md-6">
                    <p>.</p>
                    <p>.</p>
                    <p>.</p>
                    <p>.</p>
                    <p>.</p>
                    <p>.</p>
                    <p>.</p>
                    <p>.</p>
                </div>

            </div>
            <div class="col-md-7">
                <p>-عضو انجمن صنفی: هست نیست</p>
                <p>-وضعیت فعالیت: فعال متقاضی فعالیت</p>
                <p>-تعداد شکایاتی که علیه مدیر عامل از ناحیه کارگران مطرح شده است</p>
                <p>-از این تعداد....مورد منجر به محکومیت وی شده است.</p>
                <p>-تعداد حوادث ناشی از کار......مورد</p>
                <p>-تاریخ گزارش بازرسی</p>
                <p>-شماره گزارش بازرسی</p>
                <p>تعداد بازرسی های به عمل آمده از شرکت....</p>
                <p>-نام بازرس....</p>
                <p>نتیجه بازرسی: مثبت منفی</p>
                <p>وضعیت تایید صلاحیت: تایید رد</p>
                <p>تاریخ صورتجلسه کمیته انسانی.....</p>
                <p>-شماره نامه.....</p>
                <p>-تاریخ صورتجلسه کمیته مرکزی.....</p>
                <p>تاریخ نامه.....</p>
            </div>
        </div>
        <div class="col-md-12">
            <p>آدرس دفتر مرکزی:</p>
            <p>کد پستی:</p>
            <p>تلفن ثابت سا همراه کد:</p>
            <p>تلفن همراه:</p>
            <p>موقعیت شرکت:</p>
            <p>مساحت(متر مربع):</p>
            <p>نوع فعالیت:</p>
        </div>
        <div style="clear: both;height: 70px;"></div>
    </div>



@endsection