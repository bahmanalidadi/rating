@extends('layouts.responsible_company.dashboard_sub_layout')
@section('content')
    <div class="right_col" role="main" style="margin-bottom: 50px">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12" style="margin-top:50px; margin-bottom: 50px">

                <div class="col-md-3">
                    <center><img src="/images/allah.png" width="30%"></center>
                </div>
                <div class="col-md-6">
                    <center>
                        <p>جمهوری اسلامی ایران</p>
                        <p>وزارت تعاون کار و رفاه اجتماعی</p>
                        <p>وزارت کل تعاونُُ کار و رفاه اجتماعی استان لرستان</p>
                    </center>
                </div>
                <div class="col-md-3">
                    <p>شماره:</p>
                    <p>تاریخ:</p>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <center><h1>گواهینامه رتبه بندی</h1></center>
                    <h2>شرکت: آرشام گستر کسا</h2>
                    <h2>مدیر عامل: محمد محسنی</h2>
                    <h2>به شماره شناسنامه ملی: 10320643166</h2>
                    <p>در اجرای بند 4 تصویبنامه شماره 38326/ت 27506 هیات محترم وزیرانُ رتبه شرکت فوق الذکر در کمیته تعیین صلاحیت . رتبه بندی شرکتهای پشتیبانی و خدماتی مورد بررسی قرار گیرد.</p>
                        <div class="well">
                            امور اشپزخانه و رستوران - امور بهره داری از تاسیسات - تعمیر و نگعهداری ساختمان و ماشین آلات - چاپ و تکثیر - حمل و نقل - خدمات
                        </div>
                    <p>از تاریخ 1391/04/24 تا تاریخ 1394/04/24 با رتبه هفت بلامانع اعلام می گردد.</p>
                    <p> بدیهی است شرکت ملزم به رعایت کلیه مقررات مربوط به قانون کار تامین اجتماعی و مندرجات جدول ذیل و سایر  40بط بوده و ضمنا در زمینه حمل و نقل 40 درصد به مبلغ جدول ذیل افزوذه می شود</p>
                    <div class="table-responsive" style="margin: 40px 0;">
                        <table class="table table-striped jambo_table bulk_action">
                            <caption style="text-align: center;font-size: 18px;">
                                مشخصات حداکثر فعالیت مجاز شرکت
                            </caption>
                            <thead>
                                <tr>
                                    <th>تعداد کار مجاز</th>
                                    <th>تعداد افراد مجاز</th>
                                    <th>حداکثر مبلغ پیمان(بر حسب میلیون ریال)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2</td>
                                    <td>100-299</td>
                                    <td>1500</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-3 pull-left">
                    <p>احمد لطفی نژاد</p>
                    <p>مدیر کل</p>
                </div>
            </div>
        </div>
    </div>
@endsection