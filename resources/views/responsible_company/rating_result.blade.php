@extends('layouts.responsible_company.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>درخواست های بررسی نشده</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                لیست درخواست های بررسی نشده
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title" style="display: table-cell;">نام شرکت</th>
                                            <th class="column-title" style="display: table-cell;">شماره ثبت شرکت</th>
                                            <th class="column-title" style="display: table-cell;">شناسه ملی شرکت</th>
                                            <th class="column-title" style="display: table-cell;">موضوع شرکت</th>
                                            <th class="column-title" style="display: table-cell;">نام مدیر عامل</th>
                                            <th class="column-title" style="display: table-cell;">کد ملی مدیر عامل</th>
                                            <th class="column-title" style="display: table-cell;">تلفن شرکت</th>
                                            <th class="column-title" style="display: table-cell;">آدرس شرکت</th>
                                            <th class="column-title" style="display: table-cell;">نام بازرس</th>
                                            <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">نمایش نتایج بازرسی</span>
                                            </th>
                                            <th class="bulk-actions" colspan="7" style="display: none;">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="even pointer">
                                            <td class=" ">هدی رایانه زاگرس</td>
                                            <td class=" ">۲۱۴۳۲۴۱</td>
                                            <td class=" ">۲۳۵۴۳۵۳۴۵۳</td>
                                            <td class=" ">IT</td>
                                            <td class=" ">سعید محمودی</td>
                                            <td class=" ">۳۵۴۲۳۵۴۲۵</td>
                                            <td class=" ">۲۱۳۲۳۲۱۲</td>
                                            <td class=" ">بروجرد- حافظ جنوبی</td>
                                            <td class=" ">مهدی علوی</td>
                                            <td class=" last"><a href="/responsible-company/rating-result-single">نمایش نتایج</a>
                                            </td>
                                        </tr>
                                        <tr class="even pointer">
                                            <td class=" ">دیباگران اندیشه</td>
                                            <td class=" ">۲۱۴۳۲۴۱</td>
                                            <td class=" ">۲۳۵۴۳۵۳۴۵۳</td>
                                            <td class=" ">IT</td>
                                            <td class=" ">علیرضا مرادی</td>
                                            <td class=" ">۳۵۴۲۳۵۴۲۵</td>
                                            <td class=" ">۲۱۳۲۳۲۱۲</td>
                                            <td class=" ">بروجرد- حافظ شمالی</td>
                                            <td class=" ">مهدی علوی</td>
                                            <td class=" last"><a href="/responsible-company/rating-result-single">نمایش نتایج</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
