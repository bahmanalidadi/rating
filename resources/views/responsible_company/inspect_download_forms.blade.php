@extends('layouts.responsible_company.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <form action="">
            <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-2 upload_item" id="form1_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم بازرسی الف</span>
                            <input disabled=disabled type="file" name="form1_upload_file" id="form1_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form2_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم بازرسی ب</span>
                            <input disabled=disabled type="file" name="form2_upload_file" id="form2_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
