@extends('layouts.responsible_company.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <form action="">
            <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-2 upload_item" id="form1_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۱</span>
                            <input disabled=disabled type="file" name="form1_upload_file" id="form1_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form2_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۲</span>
                            <input disabled=disabled type="file" name="form2_upload_file" id="form2_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form3_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۳</span>
                            <input disabled=disabled type="file" name="form3_upload_file" id="form3_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form4_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۴</span>
                            <input disabled=disabled type="file" name="form4_upload_file" id="form4_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form5_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۵</span>
                            <input disabled=disabled type="file" name="form5_upload_file" id="form5_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form6_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۶</span>
                            <input disabled=disabled type="file" name="form6_upload_file" id="form6_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form7_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۷</span>
                            <input disabled=disabled type="file" name="form7_upload_file" id="form7_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form8_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۸</span>
                            <input disabled=disabled type="file" name="form8_upload_file" id="form8_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                    <div class="col-md-2 upload_item" id="form9_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/download-icon.png">
                            </div>
                            <span>دانلود  فرم ۹</span>
                            <input disabled=disabled type="file" name="form9_upload_file" id="form9_upload_file" class="upload_input_file form_upload">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
