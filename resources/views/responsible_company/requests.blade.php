@extends('layouts.responsible_company.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>درخواست های بررسی نشده</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                لیست درخواست های بررسی نشده
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th>
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </th>
                                            <th class="column-title" style="display: table-cell;">نام شرکت</th>
                                            <th class="column-title" style="display: table-cell;">شماره ثبت شرکت</th>
                                            <th class="column-title" style="display: table-cell;">شناسه ملی شرکت</th>
                                            <th class="column-title" style="display: table-cell;">موضوع شرکت</th>
                                            <th class="column-title" style="display: table-cell;">نام مدیر عامل</th>
                                            <th class="column-title" style="display: table-cell;">کد ملی مدیر عامل</th>
                                            <th class="column-title" style="display: table-cell;">تلفن شرکت</th>
                                            <th class="column-title" style="display: table-cell;">آدرس شرکت</th>
                                            <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">نمایش اطلاعات</span>
                                            </th>
                                            <th class="bulk-actions" colspan="7" style="display: none;">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </td>
                                            <td class=" ">هدی رایانه زاگرس</td>
                                            <td class=" ">۲۱۴۳۲۴۱</td>
                                            <td class=" ">۲۳۵۴۳۵۳۴۵۳</td>
                                            <td class=" ">IT</td>
                                            <td class=" ">سعید محمودی</td>
                                            <td class=" ">۳۵۴۲۳۵۴۲۵</td>
                                            <td class=" ">۲۱۳۲۳۲۱۲</td>
                                            <td class=" ">بروجرد- حافظ جنوبی</td>
                                            @if($mode == 'view')
                                                <td class=" last"><a href="/responsible-company/single-request?mode=view"><i class="fa fa-eye"></i></a> <a href="#"><i class="fa fa-list"></i> </a></td>
                                            @else
                                                <td class=" last"><a href="/responsible-company/single-request"><i class="fa fa-eye"></i></a> <a href="#"><i class="fa fa-list"></i> </a></td>
                                            @endif
                                        </tr>
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                            </td>
                                            <td class=" ">دیباگران اندیشه</td>
                                            <td class=" ">۲۱۴۳۲۴۱</td>
                                            <td class=" ">۲۳۵۴۳۵۳۴۵۳</td>
                                            <td class=" ">IT</td>
                                            <td class=" ">علیرضا مرادی</td>
                                            <td class=" ">۳۵۴۲۳۵۴۲۵</td>
                                            <td class=" ">۲۱۳۲۳۲۱۲</td>
                                            <td class=" ">بروجرد- حافظ شمالی</td>
                                            @if($mode == 'view')
                                                <td class=" last"><a href="/responsible-company/single-request?mode=view"><i class="fa fa-eye"></i></a> <a href="#"><i class="fa fa-list"></i> </a></td>
                                            @else
                                                <td class=" last"><a href="/responsible-company/single-request"><i class="fa fa-eye"></i></a> <a href="#"><i class="fa fa-list"></i> </a></td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="summary_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog col-md-10" style="float: none;">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">خلاصه اطلاعات شرکت</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            @include('subviews.responsible_company.summary_company')
                        </p>
                        <div style="clear:both"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default antoclose" data-dismiss="modal">بستن</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $('.fa-list').click(function(){
           $('#summary_modal').modal('show');
        });

    </script>
@endsection
