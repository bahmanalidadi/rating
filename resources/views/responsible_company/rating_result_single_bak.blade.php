@extends('layouts.responsible_company.dashboard_sub_layout')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_right">
                    <h3>نتیجه کلی بازرسی</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                نتایج کلی بازرسی شرکت هدی رایانه
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-sm-3">
                                    ۱- نام شرکت
                                </div>
                                <div class="col-sm-3">
                                    ۲- نام مدیر عامل
                                </div>
                                <div class="col-sm-3">
                                    ۳- مدرک و رشته تحصیلی
                                </div>
                                <div class="col-sm-3">
                                    ۴- نشانه قانونی شرکت
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    ۵- کد فعالیت به نظر کمیته استانی:
                                    <label for="1">1</label>
                                    <input type="radio" id="1">
                                    <label for="2">2</label>
                                    <input type="radio" id="2">
                                    <label for="3">3</label>
                                    <input type="radio" id="3">
                                    <label for="4">4</label>
                                    <input type="radio" id="4">
                                    <label for="5">5</label>
                                    <input type="radio" id="5">
                                    <label for="6">6</label>
                                    <input type="radio" id="6">
                                    <label for="7">7</label>
                                    <input type="radio" id="7">
                                </div>
                                <div class="col-sm-3">
                                    (موضوع ماده 4 فصل 2 دستورالعمل)
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    ۶- عوامل امتیازی
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                    <tr class="headings">
                                        <th>رديف </th>
                                        <th>عامل</th>
                                        <th>امتياز</th>
                                        <th colspan="۵">توضيحات</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr class="even pointer">
                                        <td>۶-۱</td>
                                        <td>تحصیلات</td>
                                        <td><input type="number" /></td>
                                        <td>
                                            <span>دکتری...زن....مرد.....(۱نفر)</span>
                                            <span>فوق لیسانس...زن....مرد.....(۱نفر)</span>
                                            <span>لیسانس...زن...مرد....(۱نفر)</span>
                                            <span>فوق دیپلم...زن...مرد...(۱نفر)</span>
                                            <span>دیپلم...زن...مرد...(۱نفر)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>۶-۲</td>
                                        <td>سوابق تجربي</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">سابقه مدیت سال سابقه غیر مدیریت سال (در این مورد کل افراد)</td>
                                    </tr>
                                    <tr>
                                        <td>۶-۳</td>
                                        <td>کارآفرینی</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">تعداد قراردادهای کار منطبق به تعداد کارگران مشمول طرح کلا و با توجه به مدارک هستند ... نفر بوده است.</td>
                                    </tr>
                                    <tr>
                                        <td>۶-۴</td>
                                        <td>فعالیت مالی</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">با توجه به مستندات ارایه شده کلا ....میلیون ریال.</td>
                                    </tr>
                                    <tr>
                                        <td>۶-۵</td>
                                        <td>کارکنان دفتر مرکزی</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">دکتری...زن....مرد....(نفر) فوق لیسانس...زن....مرد....(نفر) لیسانس...زن....مرد....(نفر) فوق دیپلم...زن....مرد....(نفر)دیپلم...زن....مرد....(نفر)</td>
                                    </tr>
                                    <tr>
                                        <td>۶-۶</td>
                                        <td>سابق تاسیس</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">بر اساس مستندات مورد ملاحظه تاریخ تاسیس می باشد.</td>
                                    </tr>
                                    <tr>
                                        <td>۶-۷</td>
                                        <td>موقعیت اداری</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">متراژ .....متر مربع ملکی و سند اداری (تجاری)            ملکی و سند غیر اداری استجباری و سند اداری (تجاری) استجباری و سند غیر اداری</td>
                                    </tr>
                                    <tr>
                                        <td>۶-۸</td>
                                        <td>جذب خانواده شهدا و ایثارگر و معمولین غیر جنگی</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">با توجه به مستندات ارایه شده نفر می باشد.</td>
                                    </tr>
                                    <tr>
                                        <td>۷</td>
                                        <td>جمع امتیازات ردیف ۱-۶ تا 6-8</td>
                                        <td><input type="number" /></td>
                                        <td colspan="5">(A)</td>
                                    </tr>
                                    <td>۸</td>
                                    <td>امتيازات ارزشیابی</td>
                                    <td><input type="number" /></td>
                                    <td colspan="5">9- امتیاز شرکت برای احراز رتبه(موضوع ماده 9 فصل 2 دستورالعمل C = A + B)</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <button type="button" id="display_rating_details_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 30px;font-size: 15px;">
                                نمایش جزئیات رتبه بندی
                            </button>
                            <button type="button" id="accept_rating_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 30px;font-size: 15px;"  data-toggle="modal" data-target="#second_inspector_form_modal">
                                تایید نتایج رتبه بندی و نمایش به متقاضی
                            </button>
                        </div>
                        <!-- second_inspector_form modal -->
                        <div id="second_inspector_form_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">تایید نتایج رتبه بندی</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            نتایج رتبه بندی شرکت هدی رایانه تایید شد  و نتیجه به ایشان اعلام می گردد
                                        </p>
                                        <div style="clear:both"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default antoclose" data-dismiss="modal">متوجه شدم</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /second_inspector_form modal -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#display_rating_details_button').click(function(){
            window.location = "/responsible-company/rating-result-details";
        })
    </script>
@endsection
