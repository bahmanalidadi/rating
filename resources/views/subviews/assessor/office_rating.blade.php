<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title" style="display: table-cell;">ردیف</th>
            <th class="column-title" style="display: table-cell;">مساحت ملک</th>
            <th class="column-title" style="display: table-cell;">کاربری</th>
            <th class="column-title" style="display: table-cell;">پلاک ثبتی</th>
            <th class="column-title" style="display: table-cell;">طبقه</th>
            <th class="column-title" style="display: table-cell;">شماره پایان کار</th>
            <th class="column-title" style="display: table-cell;">تاریخ پایان کار</th>
            <th class="column-title" style="display: table-cell;">شماره سند مالکیت</th>
            <th class="column-title" style="display: table-cell;">تاریخ سند مالکیت</th>
            <th class="column-title" style="display: table-cell;">شماره اجاره نامه محضری</th>
            <th class="column-title" style="display: table-cell;">تاریخ اجاره نامه محضری</th>
            <th class="column-title" style="display: table-cell;">تاریخ شروع اجاره</th>
            <th class="column-title" style="display: table-cell;">تاریخ پایان اجاره</th>
            <th class="column-title" style="display: table-cell;">مدت اجاره</th>
            <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
            <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
            <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
        </tr>
        </thead>
        <tbody>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">۲۰۰ متر</td>
            <td class=" ">اداری</td>
            <td class=" ">۲۵</td>
            <td class=" ">۳</td>
            <td class=" ">۳۵۴۳۶۴۷</td>
            <td class=" ">۱۳۹۰/۰۴/۰۴</td>
            <td class=" ">۵۴۳۶۵۴</td>
            <td class=" ">۱۳۹۴/۰۲/۰۱</td>
            <td class=" ">---</td>
            <td class=" ">---</td>
            <td class=" ">----</td>
            <td class=" ">----</td>
            <td class=" ">----</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        </tbody>
    </table>
</div>