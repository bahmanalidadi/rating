<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th class="column-title" style="display: table-cell;">ردیف</th>
                <th class="column-title" style="display: table-cell;">نام شرکت</th>
                <th class="column-title" style="display: table-cell;">شماره ثبت</th>
                <th class="column-title" style="display: table-cell;">تاریخ ثبت</th>
                <th class="column-title" style="display: table-cell;">محل ثبت</th>
                <th class="column-title" style="display: table-cell;">موضوع شرکت</th>
                <th class="column-title" style="display: table-cell;">تاریخ آخرین تغییرات</th>
                <th class="column-title" style="display: table-cell;">موضوع شرکت فغلی بر اساس آخرین تغییرات</th>
                <th class="column-title" style="display: table-cell;">سابقه تاسیس</th>
                <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
                <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
                <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
            </tr>
        </thead>
        <tbody>
            <tr class="even pointer">
                <td class=" ">۱</td>
                <td class=" ">هدی رایانه</td>
                <td class=" ">۲۱۳۴۱۲۲۵۳۵۳۴</td>
                <td class=" ">۱۳۹۰/۰۴/۰۴</td>
                <td class=" ">بروجرد</td>
                <td class=" ">IT</td>
                <td class=" ">۱۳۹۶/۰۲/۲۸</td>
                <td class=" ">طراحی نرم افزار</td>
                <td class=" ">۶ سال</td>
                <td class=" " style="font-size: 18px">
                    <a href="#"><i class="fa fa-times"></i></a>
                </td>
                <td class="reject_description">
                    <textarea></textarea>
                </td>
                <td class="score_number">
                    ۲۰
                </td>
            </tr>
        </tbody>
    </table>
</div>