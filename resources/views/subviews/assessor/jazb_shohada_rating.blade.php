<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title" style="display: table-cell;">ردیف</th>
            <th class="column-title" style="display: table-cell;">نام و نام خانوادگی</th>
            <th class="column-title" style="display: table-cell;">نوع امتیاز</th>
            <th class="column-title" style="display: table-cell;">درصد</th>
            <th class="column-title" style="display: table-cell;">تاریخ شروع به کار</th>
            <th class="column-title" style="display: table-cell;">سابقه کار (به ماه)</th>
            <th class="column-title" style="display: table-cell;">شماره و تاریخ گواهی واحد تایید کننده</th>
            <th class="column-title" style="display: table-cell;">شماره کد کارگاه بیمه</th>
            <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
            <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
            <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
        </tr>
        </thead>

        <tbody>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">محمود خسروی</td>
            <td class=" ">.....</td>
            <td class=" ">5</td>
            <td class=" ">1390/01/01</td>
            <td class=" ">1396/06/01</td>
            <td class=" ">......</td>
            <td class=" ">.....</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">محمود خسروی</td>
            <td class=" ">.....</td>
            <td class=" ">5</td>
            <td class=" ">1390/01/01</td>
            <td class=" ">1396/06/01</td>
            <td class=" ">......</td>
            <td class=" ">.....</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        </tbody>
    </table>
</div>