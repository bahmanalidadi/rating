<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title" style="display: table-cell;">ردیف</th>
            <th class="column-title" style="display: table-cell;">محل پیمان</th>
            <th class="column-title" style="display: table-cell;">شماره قرارداد</th>
            <th class="column-title" style="display: table-cell;">تاریخ قرارداد</th>
            <th class="column-title" style="display: table-cell;">موضوع قرارداد</th>
            <th class="column-title" style="display: table-cell;">شروع قرارداد</th>
            <th class="column-title" style="display: table-cell;">خاتمه قرارداد</th>
            <th class="column-title" style="display: table-cell;">مبلغ پیمان (تومان)</th>
            <th class="column-title" style="display: table-cell;">مبلغ نهایی پیمان (تومان)</th>
            <th class="column-title" style="display: table-cell;">کد کارگاه</th>
            <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
            <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
            <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
        </tr>
        </thead>

        <tbody>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">بروجرد</td>
            <td class=" ">۲۱۳۴۱۲</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">طراحی وب سایت</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">۱۳۹۶/۰۵/۱۴</td>
            <td class=" ">۲,۰۰۰,۰۰۰</td>
            <td class=" ">۱,۸۰۰,۰۰۰</td>
            <td class=" ">۴۲۳۴۲۳</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        <tr class="even pointer">
            <td class=" ">۲</td>
            <td class=" ">تهران</td>
            <td class=" ">۴۳۵۶۴۶</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">طراحی وب سایت</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">۱۳۹۶/۰۵/۱۴</td>
            <td class=" ">۴,۰۰۰,۰۰۰</td>
            <td class=" ">۴,۸۰۰,۰۰۰</td>
            <td class=" ">۴۲۳۴۲۳</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        <tr class="even pointer">
            <td class=" ">۳</td>
            <td class=" ">شیراز</td>
            <td class=" ">۱۲۳۱۲۴</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">طراحی وب سایت</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">۱۳۹۶/۰۵/۱۴</td>
            <td class=" ">۳,۵۰۰,۰۰۰</td>
            <td class=" ">۳,۳۰۰,۰۰۰</td>
            <td class=" ">۴۲۳۴۲۳</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>

        </tbody>
    </table>
</div>