<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title" style="display: table-cell;">ردیف</th>
            <th class="column-title" style="display: table-cell;">کد ملی</th>
            <th class="column-title" style="display: table-cell;">نام و نام خانوادگی</th>
            <th class="column-title" style="display: table-cell;">سمت</th>
            <th class="column-title" style="display: table-cell;">مدرک تحصیلی</th>
            <th class="column-title" style="display: table-cell;">رشته تحصیلی</th>
            <th class="column-title" style="display: table-cell;">مرتبط/غیر مرتبط/نیمه مرتبط</th>
            <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
            <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
            <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
        </tr>
        </thead>

        <tbody>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">4133286103</td>
            <td class=" ">محمود خسروی</td>
            <td class=" ">برنامه نویسی</td>
            <td class=" ">دیپلم</td>
            <td class=" ">کامپیوتر</td>
            <td class=" ">مرتبط</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        <tr class="even pointer">
            <td class=" ">۲</td>
            <td class=" ">4133286103</td>
            <td class=" ">علیرضا مرادی</td>
            <td class=" ">گرافیک</td>
            <td class=" ">لیسانس</td>
            <td class=" ">برق</td>
            <td class=" ">غیر مرتبط</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        </tbody>
    </table>
</div>