<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title" style="display: table-cell;">ردیف</th>
            <th class="column-title" style="display: table-cell;">نام و نام خانوادگی</th>
            <th class="column-title" style="display: table-cell;">کد ملی</th>
            <th class="column-title" style="display: table-cell;">سمت</th>
            <th class="column-title" style="display: table-cell;">مدرک تحصیلی</th>
            <th class="column-title" style="display: table-cell;">رشته تحصیلی</th>
            <th class="column-title" style="display: table-cell;">وضعیت</th>
            <th class="column-title" style="display: table-cell;">تاریخ شروع به کار</th>
            <th class="column-title" style="display: table-cell;">تاریخ ترک کار</th>
            <th class="column-title" style="display: table-cell;">سابقه کار</th>
            <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
            <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
            <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
        </tr>
        </thead>

        <tbody>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">بهمن علیدادی</td>
            <td class=" ">۲۱۳۴۱۲۲۵۳۵۳۴</td>
            <td class=" ">طراح وب سایت</td>
            <td class=" ">کارشناسی</td>
            <td class=" ">کامپیوتر - نرم افزار</td>
            <td class=" ">---</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">۱۳۹۶/۰۵/۱۴</td>
            <td class=" ">۳ سال</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۳۰
            </td>
        </tr>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">علیرضا مرادی</td>
            <td class=" ">۴۲۳۴۲۳۵۲۳۵۱</td>
            <td class=" ">طراح گرافیک</td>
            <td class=" ">کارشناسی</td>
            <td class=" ">برق و قدرت</td>
            <td class=" ">---</td>
            <td class=" ">۱۳۹۶/۰۲/۲۸</td>
            <td class=" ">۱۳۹۶/۰۵/۱۴</td>
            <td class=" ">۱ سال</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>

        </tbody>
    </table>
</div>