<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title" style="display: table-cell;">ردیف</th>
            <th class="column-title" style="display: table-cell;">محل پیمان</th>
            <th class="column-title" style="display: table-cell;">شماره و تاریخ قرارداد</th>
            <th class="column-title" style="display: table-cell;">موضوع قرارداد</th>
            <th class="column-title" style="display: table-cell;">شروع و خاتمه قرارداد</th>
            <th class="column-title" style="display: table-cell;">مبلغ پیمان</th>
            <th class="column-title" style="display: table-cell;">مبلغ نهایی پیمان</th>
            <th class="column-title" style="display: table-cell;">کد کارگاه</th>
            <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
            <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
            <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
        </tr>
        </thead>

        <tbody>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">بروجرد</td>
            <td class=" ">1396</td>
            <td class=" ">.....</td>
            <td class=" ">.....</td>
            <td class=" ">.....</td>
            <td class=" ">20000</td>
            <td class=" ">563</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        <tr class="even pointer">
            <td class=" ">2</td>
            <td class=" ">بروجرد</td>
            <td class=" ">1396</td>
            <td class=" ">.....</td>
            <td class=" ">.....</td>
            <td class=" ">.....</td>
            <td class=" ">20000</td>
            <td class=" ">563</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        </tbody>
    </table>
</div>