<div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
        <thead>
        <tr class="headings">
            <th class="column-title" style="display: table-cell;">ردیف</th>
            <th class="column-title" style="display: table-cell;">نام و نام خانوادگی</th>
            <th class="column-title" style="display: table-cell;">سمت</th>
            <th class="column-title" style="display: table-cell;">شروع شروع به کار</th>
            <th class="column-title" style="display: table-cell;">تاریخ ترک کار</th>
            <th class="column-title" style="display: table-cell;">رشته تحصیلی</th>
            <th class="column-title" style="display: table-cell;">سابقه کار</th>
            <th class="column-title" style="display: table-cell;">محل کار</th>
            <th class="column-title reject_title" style="display: table-cell;">عدم تایید اطلاعات</th>
            <th class="column-title reject_description_title" style="display: none;">دلیل عدم تایید اطلاعات</th>
            <th class="column-title" style="display: table-cell;">امتیاز متعلقه</th>
        </tr>
        </thead>

        <tbody>
        <tr class="even pointer">
            <td class=" ">۱</td>
            <td class=" ">محمود خسروی</td>
            <td class=" ">برنامه نویسی</td>
            <td class=" ">1390/01/01</td>
            <td class=" ">1396/06/01</td>
            <td class=" ">کامیوتر</td>
            <td class=" ">.....</td>
            <td class=" ">یروجرد</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
            </td>
        </tr>
        <tr class="even pointer">
            <td class=" ">۲</td>
            <td class=" ">علیرضا مرادی</td>
            <td class=" ">گرافیک</td>
            <td class=" ">1390/01/01</td>
            <td class=" ">1394/01/01</td>
            <td class=" ">برق</td>
            <td class=" ">......</td>
            <td class=" ">یروجرد</td>
            <td class=" " style="font-size: 18px">
                <a href="#"><i class="fa fa-times"></i></a>
            </td>
            <td class="reject_description">
                <textarea></textarea>
            </td>
            <td class="score_number">
                ۲۰
            </td>
        </tr>
        </tbody>
    </table>
</div>