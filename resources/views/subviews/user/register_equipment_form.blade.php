
<div class="col-md-12" style="padding: 0;" id="equipment_view">
    <h2 class="StepTitle">لیست امکانات و تجهیزات</h2>
    <div style="clear: both"></div>
    <div id="col-md-12 col-xs-12 col-sm-12 equipment_table">
        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action">
                <thead>
                <tr class="headings">
                    <th>
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </th>
                    <th class="column-title" style="display: table-cell;">عنوان</th>
                    <th class="column-title" style="display: table-cell;">مدل</th>
                    <th class="column-title" style="display: table-cell;">شماره سریال</th>
                    <th class="column-title" style="display: table-cell;">تعداد</th>
                    <th class="column-title" style="display: table-cell;">گروه مرتبط</th>
                    <th class="column-title" style="display: table-cell;">شماره سند یا فاکتور خرید</th>
                    <th class="column-title" style="display: table-cell;">تاریخ</th>
                    <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">عملیات</span>
                    </th>
                    <th class="bulk-actions" colspan="7" style="display: none;">
                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
                </thead>

                <tbody>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">لپ تاپ</td>
                    <td class=" ">macbook pro </td>
                    <td class=" ">۳۲۴۲۳۴۱۲۱۲۳</td>
                    <td class=" ">۲۱</td>
                    <td class=" ">IT</td>
                    <td class=" ">۲۱۳۴</td>
                    <td class=" ">۱۳۹۶/۰۴/۱۲</td>
                    <td class=" last">
                        <a class="display-btn display_equipment_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_equipment_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_equipment" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">لپ تاپ</td>
                    <td class=" ">macbook Air </td>
                    <td class=" ">۵۶۷۳۴۲۳۴۲</td>
                    <td class=" ">۱۰</td>
                    <td class=" ">IT</td>
                    <td class=" ">۶۵۴۵</td>
                    <td class=" ">۱۳۹۶/۰۶/۲۹</td>
                    <td class=" last">
                        <a class="display-btn display_equipment_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_equipment_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_equipment" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <button type="button" id="add_equipment_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
        +                                                 اضافه کردن تجهیزات جدید
    </button>
</div>
<div class="col-md-12" id="equipment_form" style="padding: 0">
    <h2 class="extra_title StepTitle">افزودن تجهیزات جدید </h2>
    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_title">عنوان امکانات یا تجهیزات
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="equipment_title" name="equipment_title" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_model">مدل
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="equipment_model" name="equipment_model" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_serial">شماره سریال
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="equipment_serial" name="equipment_serial" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_quantity">تعداد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="equipment_quantity" name="equipment_quantity" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_factor_number">شماره سند یا فاکتور خرید
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="equipment_factor_number" name="equipment_factor_number" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_group">گروه مرتبط
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="equipment_group" name="equipment_group" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="equipment_date">تاریخ
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="equipment_date" name="equipment_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
    </form>

    <button type="button" id="save_equipment_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
        +                                                 اضافه کردن به لیست امکانات و تجهیزات
    </button>
    <button type="button" id="back_to_equipment_list" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
        بازگشت
    </button>
</div>