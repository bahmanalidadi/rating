<form method="post" action="/user/add-company" class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left"  id="register_company_form">
    {{ csrf_field() }}
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="company_name">نام شرکت
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="company_name" name="company_name" required="required" class="form-control col-md-12 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="company_national_id">شناسه ملی شرکت
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="company_national_id" name="company_national_id" required="required" class="form-control col-md-12 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4">نوع شرکت</label>
            <div class="col-md-6 col-sm-6">
                <div class="radio">
                    <label>
                        <input type="radio" class="flat" checked name="company_type"> سهامی خاص
                    </label>
                    <label>
                        <input type="radio" class="flat" name="company_type"> سهامی عام
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="register_number">شماره ثبت
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="register_number" name="register_number" required="required" class="form-control col-md-12 col-xs-12">
            </div>
        </div>
        <div class="form-group">
            <label for="register_date" class="control-label col-md-4 col-sm-4 col-xs-12">تاریخ ثبت</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="register_date" name="register_date" class="date_input form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="register_place" class="control-label col-md-4 col-sm-4 col-xs-12">محل ثبت</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="register_place" name="register_place" class="form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="company_subject" class="control-label col-md-4 col-sm-4 col-xs-12">موضوع شرکت</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="company_subject" name="company_subject" class="form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="activity_type" class="control-label col-md-4 col-sm-4 col-xs-12">نوع فعالیت</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="activity_type" name="activity_type" class="form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="company_address" class="control-label col-md-4 col-sm-4 col-xs-12">نشانی قانونی شرکت</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <textarea id="company_address" name="company_address" class="form-control col-md-12 col-xs-12" type="text"></textarea>
            </div>
        </div>

    </div>
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label for="province" class="control-label col-md-4 col-sm-4 col-xs-12">استان</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <select class="from-control col-md-12" name="province" id="province">
                    <option value="0">انتخاب استان</option>
                    @foreach($province as $item)
                        <option value="{{ $item->id}}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="city" class="control-label col-md-4 col-sm-4 col-xs-12">شهرستان</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <select class="from-control col-md-12" name="city" id="city">
                    <option value="0">انتخاب شهرستان</option>

                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="shipment_licence_number" class="control-label col-md-4 col-sm-4 col-xs-12">شماره مجوز امور حمل و نقل</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="shipment_licence_number" name="shipment_licence_number" class="form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="printing_licence_number" class="control-label col-md-4 col-sm-4 col-xs-12">شماره مجوز امور چاپ و تکثیر</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="printing_licence_number" name="printing_licence_number" class="form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="established_history" class="control-label col-md-4 col-sm-4 col-xs-12">سابقه تاسیس</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="established_history" name="established_history" class="form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="last_modified_date" class="control-label col-md-4 col-sm-4 col-xs-12">تاریخ آخرین تغییرات</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="last_modified_date" name="last_modified_date" class="date_input form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="current_company_subject" class="control-label col-md-4 col-sm-4 col-xs-12">موضوع شرکت فعلی برابر اخرین تغییرات</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input id="current_company_subject" name="current_company_subject" class="form-control col-md-12 col-xs-12" type="text">
            </div>
        </div>
    </div>
    <input type="submit">
</form>

<div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
    <p class="upload_box_title">مدارک مربوطه زیر را آپلود کنید</p>
    <form method="post" enctype="multipart/form-data" name="upload_form" action="/user/upload_file">
        {{csrf_field()}}
        <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
            <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="col-md-4 upload_item" id="asasname_upload_item">
                    <div class="col-md-12 file_box">
                        <div class="image_box">
                            <img src="/images/upload-icon.png">
                        </div>
                        <span>تصویر اساسنامه</span>
                        <input type="file" name="asasname_upload_file" id="asasname_upload_file" class="upload_input_file">
                        <input type="hidden" name="upload_file_type" class="upload_file_type" >
                    </div>
                </div>
                <div class="col-md-4 upload_item" id="newspaper_upload_item">
                    <div class="col-md-12 file_box">
                        <div class="image_box">
                            <img src="/images/upload-icon.png">
                        </div>
                        <span>تصویر روزنامه</span>
                        <input type="file" name="newspaper_upload_file" id="newspaper_upload_file" class="upload_input_file">
                        <input type="hidden" name="upload_file_type" class="upload_file_type" >
                    </div>
                </div>
                <div class="col-md-4 upload_item" id="company_letter_upload_item">
                    <div class="col-md-12 file_box">
                        <div class="image_box">
                            <img src="/images/upload-icon.png">
                        </div>
                        <span>تصویر شرکت نامه</span>
                        <input type="file" name="company_letter_upload_file" id="company_letter_upload_file" class="upload_input_file">
                        <input type="hidden" name="upload_file_type" class="upload_file_type" >
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
