<div class="col-md-12 colsm-12 col-xs-12" id="central_office">
    <div class="col-md-12 col-sm-12 col-xs-12" id="summary_office_info">
        <h2 class="StepTitle">اطلاعات دفتر مرکزی</h2>
        <div class="col-md-12" id="central_office_form" style="padding: 0">
            <div class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">مساحت ملکی
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">کاربری
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">پلاک ثبتی
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">طبقه
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">تلفن ثابت
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">تلفن همراه
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">کد پستی
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_postal_code" name="office_postal_code" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_address">آدرس
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="office_address" name="office_address" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_number">شماره  پایان کار - عدم خلاف
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="end_work_number" name="end_work_number" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_date">تاریخ پایان کار - عدم خلاف
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="end_work_date" name="end_work_date" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_number">شماره  سند مالکیت
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="evidence_number" name="evidence_number" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_date">تاریخ  سند مالکیت
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="evidence_date" name="evidence_date" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_number">شماره اجاره نامه محضری
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="rental_number" name="rental_number" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_date">تاریخ اجاره نامه محضری
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="rental_date" name="rental_date" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_code">کد رهگیری استیجاری
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="rental_code" name="rental_code" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_duration">مدت اجاره
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="rent_duration" name="rent_duration" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_start">تاریخ شروع اجاره
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="rent_start" name="rent_start" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تاریخ خاتمه اجاره
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input disabled=disabled type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
                <p class="upload_box_title">مدارک مربوطه آپلود شده</p>
                <form action="">
                    <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <div class="col-md-4 upload_item" id="koruki_upload_item">
                                <div class="col-md-12 file_box">
                                    <div class="image_box">
                                        <img src="/images/upload-icon.png">
                                    </div>
                                    <span>تصویر کروکی</span>
                                    <input disabled=disabled type="file" name="koruki_upload_file" id="koruki_upload_file" class="upload_input_file">
                                </div>
                            </div>
                            <div class="col-md-4 upload_item" id="rental_upload_item">
                                <div class="col-md-12 file_box">
                                    <div class="image_box">
                                        <img src="/images/upload-icon.png">
                                    </div>
                                    <span>تصویر اجاره نامه</span>
                                    <input disabled=disabled type="file" name="rental_upload_file" id="rental_upload_file" class="upload_input_file">
                                </div>
                            </div>
                            <div class="col-md-4 upload_item" id="evidence_upload_item">
                                <div class="col-md-12 file_box">
                                    <div class="image_box">
                                        <img src="/images/upload-icon.png">
                                    </div>
                                    <span>تصویر سند مالکیت</span>
                                    <input disabled=disabled type="file" name="evidence_upload_file" id="evidence_upload_file" class="upload_input_file">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <button type="button" id="edit_office_info" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 20px;font-size: 15px;">
                ویرایش اطلاعات
            </button>
            <div class="col-md-12" style="padding: 0;" id="employee_view">
                <div class="StepTitle" style="text-align:right;font-size:15px;margin-bottom: 10px;margin-top: 20px;">اطلاعات کارمندان دفتر مرکزی</div>
                <div style="clear: both"></div>
                <div id="col-md-12 col-xs-12 col-sm-12 central_office_employee_table">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th>
                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                </th>
                                <th class="column-title" style="display: table-cell;">نام </th>
                                <th class="column-title" style="display: table-cell;">نام خانوادگی </th>
                                <th class="column-title" style="display: table-cell;">گروه شغلی </th>
                                <th class="column-title" style="display: table-cell;">عنوان شغل </th>
                                <th class="column-title" style="display: table-cell;">تاریخ شروع قرارداد </th>
                                <th class="column-title" style="display: table-cell;">حقوق (تومان)</th>
                                <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">عملیات</span>
                                </th>
                                <th class="bulk-actions" colspan="7" style="display: none;">
                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr class="even pointer">
                                <td class="a-center ">
                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                </td>
                                <td class=" ">علیرضا</td>
                                <td class=" ">مرادی</td>
                                <td class=" ">تیم فنی</td>
                                <td class=" ">طراح گرافیک</td>
                                <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                <td class=" last">
                                    <a class="display-btn display_employee_info" href="#"><i class="fa fa-eye"></i></a>
                                    <a class="edit-btn edit_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                                    <a class="delete-btn delete_employee" href="#"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <tr class="even pointer">
                                <td class="a-center ">
                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                </td>
                                <td class=" ">بهمن</td>
                                <td class=" ">علیدادی</td>
                                <td class=" ">تیم فنی</td>
                                <td class=" ">برنامه نویس و طراح وب</td>
                                <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                <td class=" last">
                                    <a class="display-btn display_employee_info" href="#"><i class="fa fa-eye"></i></a>
                                    <a class="edit-btn edit_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                                    <a class="delete-btn delete_employee" href="#"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <tr class="even pointer">
                                <td class="a-center ">
                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                </td>
                                <td class=" ">علیرضا</td>
                                <td class=" ">مرادی</td>
                                <td class=" ">تیم فنی</td>
                                <td class=" ">طراح گرافیک</td>
                                <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                <td class=" last">
                                    <a class="display-btn display_employee_info" href="#"><i class="fa fa-eye"></i></a>
                                    <a class="edit-btn edit_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                                    <a class="delete-btn delete_employee" href="#"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <tr class="even pointer">
                                <td class="a-center ">
                                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                </td>
                                <td class=" ">بهمن</td>
                                <td class=" ">علیدادی</td>
                                <td class=" ">تیم فنی</td>
                                <td class=" ">برنامه نویس و طراح وب</td>
                                <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                                <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                                <td class=" last">
                                    <a class="display-btn display_employee_info" href="#"><i class="fa fa-eye"></i></a>
                                    <a class="edit-btn edit_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                                    <a class="delete-btn delete_employee" href="#"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <button type="button" id="add_employee_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
                    +                                                 اضافه کردن کارمند جدید
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-12" id="central_office_edit_form" style="padding: 0;display:none;">
        <h2 class="extra_title StepTitle">ویرایش اطلاعات دفتر مرکزی</h2>
        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">مساحت ملکی
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">کاربری
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">پلاک ثبتی
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">طبقه
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">تلفن ثابت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">تلفن همراه
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">کد پستی
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_postal_code" name="office_postal_code" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_address">آدرس
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_address" name="office_address" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_number">شماره  پایان کار - عدم خلاف
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="end_work_number" name="end_work_number" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_date">تاریخ پایان کار - عدم خلاف
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="end_work_date" name="end_work_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_number">شماره  سند مالکیت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="evidence_number" name="evidence_number" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_date">تاریخ  سند مالکیت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="evidence_date" name="evidence_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_number">شماره اجاره نامه محضری
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rental_number" name="rental_number" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_date">تاریخ اجاره نامه محضری
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rental_date" name="rental_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_code">کد رهگیری استیجاری
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rental_code" name="rental_code" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_duration">مدت اجاره
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_duration" name="rent_duration" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_start">تاریخ شروع اجاره
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_start" name="rent_start" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تاریخ خاتمه اجاره
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>
            </div>
        </form>
        <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
            <p class="upload_box_title">مدارک مربوطه زیر را پلود کنید</p>
            <form action="">
                <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="col-md-4 upload_item" id="koruki_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>تصویر کروکی</span>
                                <input type="file" name="koruki_upload_file" id="koruki_upload_file" class="upload_input_file">
                            </div>
                        </div>
                        <div class="col-md-4 upload_item" id="rental_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>تصویر اجاره نامه</span>
                                <input type="file" name="rental_upload_file" id="rental_upload_file" class="upload_input_file">
                            </div>
                        </div>
                        <div class="col-md-4 upload_item" id="evidence_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>تصویر سند مالکیت</span>
                                <input type="file" name="evidence_upload_file" id="evidence_upload_file" class="upload_input_file">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <button type="button" id="save_office_info" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 40px;font-size: 15px;">
            ذخیره اطلاعات
        </button>
    </div>
    <div class="col-md-12" id="central_office_employee_form" style="padding: 0">
        <h2 class="extra_title StepTitle">افزودن کارمند جدید</h2>
        <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">نام
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">نام خانوادگی
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">کد ملی
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">نام پدر
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">شماره شناسنامه
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">محل صدور
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">تاریخ تولد
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_postal_code" name="office_postal_code" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_address">وضعیت تاهل
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="office_address" name="office_address" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_number">تعداد اولاد
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="end_work_number" name="end_work_number" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div><div class="form-group">
                    <label for="province" class="control-label col-md-4 col-sm-4 col-xs-12">مدرک تحصیلی</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="btn-group col-md-12" style="padding: 0">
                            <button data-toggle="dropdown" style="text-align: right" class="col-md-12 btn btn-default dropdown-toggle" type="button"> انتخاب کنید <span class="caret pull-left" style="margin-top: 8px;"></span> </button>
                            <ul class="dropdown-menu col-md-12" style="padding: 0">
                                <li><a href="#">دکترا</a>
                                </li>
                                <li><a href="#">کارشناسی ارشد</a>
                                </li>
                                <li><a href="#">کارشناسی</a>
                                </li>
                                <li><a href="#">کاردانی</a>
                                </li>
                                <li><a href="#">دیپلم</a>
                                </li>
                                <li><a href="#">زیر دیپلم</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_number">وضعیت خدمت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="evidence_number" name="evidence_number" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="evidence_date">تاریخ  شروع به کار
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="evidence_date" name="evidence_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_number">تاریخ ترک کار
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rental_number" name="rental_number" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_date">سابقه کار
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rental_date" name="rental_date" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rental_code">عنوان شغل
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rental_code" name="rental_code" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_duration">گروه شغلی
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_duration" name="rent_duration" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_start">جمع مزد مبنا - ماهیانه ۳۰ روز
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_start" name="rent_start" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">مدت قرارداد از تاریخ
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="date_input form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">پایان خدمت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">به مدت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">عادی کار از ساعت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تا ساعت
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">جمع ساعت در هفته
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">نوبت کار
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">سایر حالات
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12" for="rent_end">تعطیلات هفته
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" id="rent_end" name="rent_end" required="required" class="form-control col-md-12 col-xs-12">
                    </div>
                </div>
            </div>
        </form>
        <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
            <p class="upload_box_title">مدارک مربوطه زیر را پلود کنید</p>
            <form action="">
                <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="col-md-4 upload_item" id="office_employee_contract_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>تصویر قرارداد</span>
                                <input type="file" name="office_employee_contract_upload_file" id="office_employee_contract_upload_file" class="upload_input_file">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <button type="button" id="save_add_employee_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 15px;margin-top: 40px;font-size: 15px;">
            +                                                 اضافه کردن به کارمندان شرکت
        </button>
        <button type="button" id="back_to_employees_list" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 15px;margin-top: 40px;font-size: 15px;">
            بازگشت به لیست کارمندان
        </button>
    </div>
</div>