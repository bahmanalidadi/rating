
<div class="col-md-12 col-sm-12 col-xs-12" id="treaty_view_info">
    <h2 class="StepTitle">اطلاعات مربوط به پیمان ها</h2>
    <div class="table-responsive" id="treaty_table">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
            <tr class="headings">
                <th>
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </th>
                <th class="column-title" style="display: table-cell;">شماره پیمان</th>
                <th class="column-title" style="display: table-cell;">موضوع قرارداد</th>
                <th class="column-title" style="display: table-cell;">تاریخ قرارداد</th>
                <th class="column-title" style="display: table-cell;">تعداد افراد شاغل در پیمان</th>
                <th class="column-title" style="display: table-cell;">مدت قرارداد</th>
                <th class="column-title" style="display: table-cell;">مبلغ پیمان</th>
                <th class="column-title" style="display: table-cell;">مبلغ نهایی پیمان</th>
                <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">عملیات</span>
                </th>
                <th class="bulk-actions" colspan="7" style="display: none;">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                </th>
            </tr>
            </thead>

            <tbody>
            <tr class="even pointer">
                <td class="a-center ">
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </td>
                <td class=" ">۱۲۳۴</td>
                <td class=" ">پیمان تست</td>
                <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                <td class=" ">۱۳</td>
                <td class=" ">۲۰ ماه</td>
                <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                <td class="a-right a-right ">۱,۸۰۰,۰۰۰</td>
                <td class=" last">
                    <a class="display-btn display_treaty_info" href="#"><i class="fa fa-eye"></i></a>
                    <a class="edit-btn edit_treaty_info" href="#"><i class="fa fa-pencil"></i></a>
                    <a class="delete-btn delete_treaty" href="#"><i class="fa fa-times"></i></a>
                </td>
            </tr>

            <tr class="even pointer">
                <td class="a-center ">
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </td>
                <td class=" ">۱۲۳۴</td>
                <td class=" ">پیمان تست</td>
                <td class=" ">۱۳۶۹/۰۲/۱۱</td>
                <td class=" ">۵</td>
                <td class=" ">۱۲ ماه</td>
                <td class="a-right a-right ">۳,۰۰۰,۰۰۰</td>
                <td class="a-right a-right ">۲,۸۰۰,۰۰۰</td>
                <td class=" last">
                    <a class="display-btn display_treaty_info" href="#"><i class="fa fa-eye"></i></a>
                    <a class="edit-btn edit_treaty_info" href="#"><i class="fa fa-pencil"></i></a>
                    <a class="delete-btn delete_treaty" href="#"><i class="fa fa-times"></i></a>
                </td>
            </tr>

            <tr class="even pointer">
                <td class="a-center ">
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </td>
                <td class=" ">۱۲۳۴</td>
                <td class=" ">پیمان تست</td>
                <td class=" ">۱۳۶۹/۰۱/۱۵</td>
                <td class=" ">۱۳</td>
                <td class=" ">۲۰ ماه</td>
                <td class="a-right a-right ">۲,۰۰۰,۰۰۰</td>
                <td class="a-right a-right ">۱,۸۰۰,۰۰۰</td>
                <td class=" last">
                    <a class="display-btn display_treaty_info" href="#"><i class="fa fa-eye"></i></a>
                    <a class="edit-btn edit_treaty_info" href="#"><i class="fa fa-pencil"></i></a>
                    <a class="delete-btn delete_treaty" href="#"><i class="fa fa-times"></i></a>
                </td>
            </tr>

            <tr class="even pointer">
                <td class="a-center ">
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </td>
                <td class=" ">۱۲۳۴</td>
                <td class=" ">پیمان تست</td>
                <td class=" ">۱۳۶۹/۰۲/۱۱</td>
                <td class=" ">۵</td>
                <td class=" ">۱۲ ماه</td>
                <td class="a-right a-right ">۳,۰۰۰,۰۰۰</td>
                <td class="a-right a-right ">۲,۸۰۰,۰۰۰</td>
                <td class=" last">
                    <a class="display-btn display_treaty_info" href="#"><i class="fa fa-eye"></i></a>
                    <a class="edit-btn edit_treaty_info" href="#"><i class="fa fa-pencil"></i></a>
                    <a class="delete-btn delete_treaty" href="#"><i class="fa fa-times"></i></a>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
    <button type="button" id="add_treaty_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 0px;font-size: 15px;">
        +                                                 اضافه کردن پیمان جدید
    </button>
</div>
<div class="col-md-12 col-sm-12 col-xs-12" id="treaty_form">
    <h2 class="StepTitle">اطلاعات مربوط به پیمان</h2>
    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_number">شماره پیمان
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_number" name="treaty_number" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_employees_number">تعداد افراد شاغل در پیمان
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_employees_number" name="treaty_employees_number" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_date">تاریخ قرارداد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_date" name="treaty_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_workhouse_code">کد کارگاه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_workhouse_code" name="treaty_workhouse_code" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_subject">موضوع قرارداد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_subject" name="treaty_subject" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_summary">خلاصه موضوع پیمان
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_summary" name="treaty_summary" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_price">مبلغ پیمان
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_price" name="treaty_price" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_total_price">مبلغ نهایی پیمان
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_total_price" name="treaty_total_price" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_duration">مدت قرارداد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_duration" name="treaty_duration" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_start_date">شروع قرارداد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_start_date" name="treaty_start_date" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_end_date">خاتمه قرارداد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_end_date" name="treaty_end_date" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="treaty_place">محل پیمان
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="treaty_place" name="treaty_place" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
            <p class="upload_box_title">مدارک مربوطه زیر را پلود کنید</p>
            <form action="">
                <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="col-md-4 upload_item" id="treaty_Insurance_list_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>لیست بیمه</span>
                                <input type="file" name="treaty_Insurance_list_upload_file" id="treaty_Insurance_list_upload_file" class="upload_input_file">
                            </div>
                        </div>
                        <div class="col-md-4 upload_item" id="treaty_salary_list_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>لیست حقوق</span>
                                <input type="file" name="treaty_salary_list_upload_file" id="treaty_salary_list_upload_file" class="upload_input_file">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div style="clear: both;margin-bottom: 50px;"></div>
        <div class="StepTitle" style="float:right;text-align:right;font-size:15px;margin-bottom: 10px;margin-top: 10px;">اطلاعات کارمندان مشغول در پیمان</div>
        <button type="button" id="add_employee_treaty_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-top: 0px;font-size: 15px;">
            +                                                 اضافه کردن کارمند جدید به پیمان
        </button>
        <div style="clear: both;"></div>
        <div class="table-responsive" id="treaty_employees_table">
            <table class="table table-striped jambo_table bulk_action">
                <thead>
                <tr class="headings">
                    <th>
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </th>
                    <th class="column-title" style="display: table-cell;">کد بیمه کارگاه</th>
                    <th class="column-title" style="display: table-cell;">نام </th>
                    <th class="column-title" style="display: table-cell;">نام خانوادگی </th>
                    <th class="column-title" style="display: table-cell;">کد ملی</th>
                    <th class="column-title" style="display: table-cell;">شماره بیمه</th>
                    <th class="column-title" style="display: table-cell;">نام ارگان</th>
                    <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">عملیات</span>
                    </th>
                    <th class="bulk-actions" colspan="7" style="display: none;">
                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
                </thead>

                <tbody>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">۱۳۴</td>
                    <td class=" ">علیرضا</td>
                    <td class=" ">مرادی</td>
                    <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                    <td class=" ">۱۳۴۲۳۴</td>
                    <td class=" ">تامین اجتماعی</td>
                    <td class=" last">
                        <a class="display-btn display_treaty_employee_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_treaty_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_treaty_employee" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">۱۵۴</td>
                    <td class=" ">بهمن</td>
                    <td class=" ">علیدادی</td>
                    <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                    <td class=" ">۱۳۴۲۳۴</td>
                    <td class=" ">تامین اجتماعی</td>
                    <td class=" last">
                        <a class="display-btn display_treaty_employee_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_treaty_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_treaty_employee" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">۱۳۴</td>
                    <td class=" ">علیرضا</td>
                    <td class=" ">مرادی</td>
                    <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                    <td class=" ">۱۳۴۲۳۴</td>
                    <td class=" ">تامین اجتماعی</td>
                    <td class=" last">
                        <a class="display-btn display_treaty_employee_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_treaty_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_treaty_employee" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">۱۵۴</td>
                    <td class=" ">بهمن</td>
                    <td class=" ">علیدادی</td>
                    <td class=" ">۴۱۲۳۴۴۵۶۷۸</td>
                    <td class=" ">۱۳۴۲۳۴</td>
                    <td class=" ">تامین اجتماعی</td>
                    <td class=" last">
                        <a class="display-btn display_treaty_employee_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_treaty_employee_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_treaty_employee" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <button type="button" id="save_treaty_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 20px;font-size: 15px;">
            ذخیره پیمان جدید
        </button>
        <button type="button" id="back_to_treaty_list" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 20px;font-size: 15px;">
            بازگشت به لیست پیمان ها
        </button>
    </form>
</div>
<div class="col-md-12" id="treaty_employee_form" style="padding: 0">
    <h2 class="extra_title StepTitle">افزودن کارمند جدید به پیمان</h2>
    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_space">نام
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_space" name="office_space" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">نام خانوادگی
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">کد ملی
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_floor">کد بیمه کارگاه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_floor" name="office_floor" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">شماره شناسنامه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_phone">سریال شناسنامه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_phone" name="office_phone" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_mobile">نام ارگان
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_mobile" name="office_mobile" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_postal_code">شماره بیمه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_postal_code" name="office_postal_code" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
    </form>
    <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
        <p class="upload_box_title">مدارک مربوطه زیر را پلود کنید</p>
        <form action="">
            <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="col-md-4 upload_item" id="treaty_employee_contract_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/upload-icon.png">
                            </div>
                            <span>تصویر قرارداد</span>
                            <input type="file" name="treaty_employee_contract_upload_file" id="treaty_employee_contract_upload_file" class="upload_input_file">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <button type="button" id="save_treaty_employee_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 15px;margin-top: 40px;font-size: 15px;">
        +                                                 اضافه کردن به کارمندان پیمان
    </button>
    <button type="button" id="back_to_treaty_employees_list" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-top: 20px;font-size: 15px;">
        بازگشت به پیمان
    </button>
</div>
