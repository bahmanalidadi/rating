<div class="col-md-12 col-sm-12 col-xs-12 member_card_box">
    <h2 class="StepTitle"> اعضای هیئت مدیره</h2>
    <div style="clear: both"></div>
    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
            <div class="col-sm-12">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <img src="/images/alireza.jpg" alt="" class="col-md-6 col-md-offset-3 img-circle img-responsive">
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 style="text-align: center; margin-bottom: 20px;">علیرضا مرادی</h2>
                    <p><strong>تحصیلات: </strong> مهندسی برق </p>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-building"></i> کد ملی: ۴۱۲۰۳۳۶۵۸۱</li>
                        <li><i class="fa fa-phone"></i> تلفن : ۰۹۳۷۶۲۳۵۶۴۴</li>
                    </ul>
                </div>

            </div>
            <div class="col-xs-12 bottom text-center">
                <div class="col-xs-12 col-sm-8 title_position_card emphasis">
                    <h4 class="brief"><i>مدیر عامل</i></h4>
                </div>
                <div class="col-xs-12 col-sm-4 edit_buttons emphasis">
                    <button type="button" title="حذف کردن" class="btn btn-danger btn-xs">
                        <i class="fa fa-times"></i>
                    </button>
                    <button type="button" title="مشاهده پروفایل" class="display-btn member_card_info_btn btn btn-success btn-xs">
                        <i class="fa fa-eye"></i>
                    </button>
                    <button type="button" title="ویرایش اطلاعات" class="btn btn-warning btn-xs">
                        <i class="fa fa-pencil"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
            <div class="col-sm-12">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <img src="/images/bahman.jpg" alt="" class="col-md-6 col-md-offset-3 img-circle img-responsive">
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 style="text-align: center; margin-bottom: 20px;">بهمن علیدادی</h2>
                    <p><strong>تحصیلات: </strong> مهندسی کامپیوتر</p>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-building"></i> کد ملی: ۴۱۲۰۳۳۶۵۸۱</li>
                        <li><i class="fa fa-phone"></i> تلفن : ۰۹۳۷۶۲۳۵۶۴۴</li>
                    </ul>
                </div>

            </div>
            <div class="col-xs-12 bottom text-center">
                <div class="col-xs-12 col-sm-8 emphasis title_position_card">
                    <h4 class="brief"><i>عضو هیئت مدیره</i></h4>
                </div>
                <div class="col-xs-12 col-sm-4 emphasis edit_buttons">
                    <button type="button" title="حذف کردن" class="btn btn-danger btn-xs">
                        <i class="fa fa-times"></i>
                    </button>
                    <button type="button" title="مشاهده پروفایل" class="display-btn member_card_info_btn btn btn-success btn-xs">
                        <i class="fa fa-eye"></i>
                    </button>
                    <button type="button" title="ویرایش اطلاعات" class="btn btn-warning btn-xs">
                        <i class="fa fa-pencil"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 profile_details" style="clear:none">
        <div class="well col-md-12 profile_view">
            <div class="col-sm-12" style="height: 230px;">
                <i class="fa fa-plus add_user"></i>
            </div>
            <div class="col-xs-12 bottom text-center" style="height: 44px;">
                <h2>+ افزودن عضو جدید</h2>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" id="add_user_form" style="padding: 0">
    <h2 class="extra_title StepTitle"> افزودن عضو جدید به هیئت مدیره</h2>
    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="first_name">نام
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="first_name" name="first_name" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last_name">نام خانوادگی
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="last_name" name="last_name" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="father_name">نام پدر
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="father_name" name="father_name" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="birth_date">تاریخ تولد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="birth_date" name="birth_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4">جنسیت</label>
                <div class="col-md-6 col-sm-6">
                    <div class="radio">
                        <label>
                            <input type="radio" class="flat" checked name="gender"> آقا
                        </label>
                        <label>
                            <input type="radio" class="flat" name="gender"> خانم
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="national_code">کد ملی
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="national_code" name="national_code" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="id_number">شماره شناسنامه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="id_number" name="id_number" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="place_of_issue">محل صدور
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="place_of_issue" name="place_of_issue" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="phone">تلفن ثابت
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="phone" name="phone" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="mobile">تلفن همراه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="mobile" name="mobile" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="degree" class="control-label col-md-4 col-sm-4 col-xs-12">مدرک تحصیلی</label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="btn-group col-md-12" style="padding: 0">
                        <button data-toggle="dropdown" style="text-align: right" class="col-md-12 btn btn-default dropdown-toggle" type="button"> انتخاب کنید <span class="caret pull-left" style="margin-top: 8px;"></span> </button>
                        <ul class="dropdown-menu col-md-12" style="padding: 0">
                            <li><a href="#">دکترا</a>
                            </li>
                            <li><a href="#">کارشناسی ارشد</a>
                            </li>
                            <li><a href="#">کارشناسی</a>
                            </li>
                            <li><a href="#">کاردانی</a>
                            </li>
                            <li><a href="#">دیپلم</a>
                            </li>
                            <li><a href="#">زیر دیپلم</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="major">رشته تحصیلی
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="major" name="major" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4">وضعیت رشته تحصیلی</label>
                <div class="col-md-8 col-sm-8">
                    <div class="radio">
                        <label>
                            <input type="radio" class="flat" checked name="major_status"> مرتبط
                        </label>
                        <label>
                            <input type="radio" class="flat" name="major_status"> نیمه مرتبط
                        </label>
                        <label>
                            <input type="radio" class="flat" name="major_status"> غیر مرتبط
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="work_place">محل کار
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="work_place" name="work_place" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4">سمت در شرکت</label>
                <div class="col-md-8 col-sm-8">
                    <div class="radio">
                        <label>
                            <input type="radio" class="flat" checked name="position"> عضو هیئت مدیره
                        </label>
                        <label>
                            <input type="radio" class="flat" name="position"> مدیر عامل
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="start_work_date">تاریخ شروع به کار
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="start_work_date" name="start_work_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="end_work_date">تاریخ ترک کار
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="end_work_date" name="end_work_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="experience">سابقه کار
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="experience" name="experience" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
    </form>
    <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;">
        <p class="upload_box_title">مدارک مربوطه زیر را پلود کنید</p>
        <form action="">
            <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                <div class="col-md-7 col-sm-12 col-xs-12 sub_upload_box">
                    <p>تصاویر صفحات شماسنامه</p>
                    <div class="col-md-4 upload_item" id="birth_certificate_1_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/upload-icon.png">
                            </div>
                            <span>صفحه اول</span>
                            <input type="file" name="birth_certificate_1_upload_file" id="birth_certificate_1_upload_file" class="upload_input_file">
                        </div>
                    </div>
                    <div class="col-md-4 upload_item" id="birth_certificate_2_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/upload-icon.png">
                            </div>
                            <span>صفحه دوم</span>
                            <input type="file" name="birth_certificate_2_upload_file" id="birth_certificate_2_upload_file" class="upload_input_file">
                        </div>
                    </div>
                    <div class="col-md-4 upload_item" id="birth_certificate_3_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/upload-icon.png">
                            </div>
                            <span>صفحه سوم</span>
                            <input type="file" name="birth_certificate_3_upload_file" id="birth_certificate_3_upload_file" class="upload_input_file">
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs 12">
                    <div class="col-md-12 col-sm-12 col-xs-12 sub_upload_box">
                        <p>تصاویر کارت ملی</p>
                        <div class="col-md-6 upload_item" id="id_card_front_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>روی کارت</span>
                                <input type="file" name="id_card_front_upload_file" id="id_card_front_upload_file" class="upload_input_file">
                            </div>
                        </div>
                        <div class="col-md-6 upload_item" id="id_card_back_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>پشت کارت</span>
                                <input type="file" name="id_card_back_upload_file" id="id_card_back_upload_file" class="upload_input_file">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12 sub_upload_box" style="margin-top: 20px">
                    <p>تصاویر مدرک تحصیلی</p>
                    <div class="col-md-4 upload_item" id="certificate_1_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/upload-icon.png">
                            </div>
                            <span>مدرک تحصیلی ۱</span>
                            <input type="file" name="certificate_1_upload_file" id="certificate_1_upload_file" class="upload_input_file">
                        </div>
                    </div>
                    <div class="col-md-4 upload_item" id="certificate_2_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/upload-icon.png">
                            </div>
                            <span>مدرک تحصیلی ۲</span>
                            <input type="file" name="certificate_2_upload_file" id="certificate_2_upload_file" class="upload_input_file">
                        </div>
                    </div>
                    <div class="col-md-4 upload_item" id="certificate_3_upload_item">
                        <div class="col-md-12 file_box">
                            <div class="image_box">
                                <img src="/images/upload-icon.png">
                            </div>
                            <span>مدرک تحصیلی ۳</span>
                            <input type="file" name="certificate_3_upload_file" id="certificate_3_upload_file" class="upload_input_file">
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs 12" style="margin-top: 20px;">
                    <div class="col-md-12 col-sm-12 col-xs-12 sub_upload_box">
                        <p>تصویر شخص</p>
                        <div class="col-md-6 upload_item" id="avatar_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>تصویر شخص</span>
                                <input type="file" name="avatar_upload_file" id="avatar_upload_file" class="upload_input_file">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div style="clear: both;margin-bottom: 50px;"></div>
    <div class="StepTitle" style="float:right;text-align:right;font-size:15px;margin-bottom: 10px;margin-top: 10px;">گواهینامه های کسب شده</div>
    <button type="button" id="add_certificate_manager_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="margin-top: 0px;font-size: 15px;">
        +                                                 اضافه کردن گواهینامه
    </button>
    <div style="clear: both;"></div>
    <div class="table-responsive" id="certificate_manager_table">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
            <tr class="headings">
                <th>
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </th>
                <th class="column-title" style="display: table-cell;">عنوان دوره</th>
                <th class="column-title" style="display: table-cell;">تاریخ شروع دوره</th>
                <th class="column-title" style="display: table-cell;">تاریخ پایان دوره</th>
                <th class="column-title" style="display: table-cell;">تعداد ساعت</th>
                <th class="column-title" style="display: table-cell;">نمره</th>
                <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">عملیات</span>
                </th>
                <th class="bulk-actions" colspan="7" style="display: none;">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                </th>
            </tr>
            </thead>

            <tbody>
            <tr class="even pointer">
                <td class="a-center ">
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </td>
                <td class=" ">گرافیک</td>
                <td class=" ">۱۳۹۶/۰۱/۱۸</td>
                <td class=" ">۱۳۹۶/۰۴/۲۰</td>
                <td class=" ">۲۰</td>
                <td class=" ">۸۵</td>
                <td class=" last">
                    <a class="display-btn display_certificate_info" href="#"><i class="fa fa-eye"></i></a>
                    <a class="edit-btn edit_certificate_info" href="#"><i class="fa fa-pencil"></i></a>
                    <a class="delete-btn delete_certificate" href="#"><i class="fa fa-times"></i></a>
                </td>
            </tr>
            <tr class="even pointer">
                <td class="a-center ">
                    <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                </td>
                <td class=" ">برنامه نویسی</td>
                <td class=" ">۱۳۹۶/۰۱/۱۸</td>
                <td class=" ">۱۳۹۶/۰۴/۲۰</td>
                <td class=" ">۲۰</td>
                <td class=" ">۸۵</td>
                <td class=" last">
                    <a class="display-btn display_certificate_info" href="#"><i class="fa fa-eye"></i></a>
                    <a class="edit-btn edit_certificate_info" href="#"><i class="fa fa-pencil"></i></a>
                    <a class="delete-btn delete_certificate" href="#"><i class="fa fa-times"></i></a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <button type="button" id="save_add_user_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 15px;margin-top: 40px;font-size: 15px;">
        +                                                 اضافه کردن به اعضای هیئت مدیره
    </button>
</div>
<div class="col-md-12" id="certificate_manager_form" style="padding: 0">
    <h2 class="extra_title StepTitle">افزودن گواهینامه</h2>
    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="certificate_title">عنوان دوره
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="certificate_title" name="certificate_title" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="certificate_start_date">تاریخ شروع دوره
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="certificate_start_date" name="certificate_start_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="certificate_end_date">تاریخ پایان دوره
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="certificate_end_date" name="certificate_end_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_type">تعداد ساعت
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_type" name="office_type" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="office_no">نمره
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="office_no" name="office_no" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-12 upload_title_box" style="color: #1ABB9C;margin-top: 50px;" id="certificate_jobs_upload_box">
            <p class="upload_box_title" style="float: right;margin-top: 10px;color: #e91e63;">گواهینامه طرح طبقه بندی مشاغل را آپلود کنید</p>
            <button type="button" id="not_certificate_button" class="btn btn-warning col-md-4 btn-lg pull-left" style="font-size: 15px;">
                گواهینامه طرح طبقه بندی مشاغل ندارد
            </button>
            <form action="">
                <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="col-md-4 upload_item" id="certificate_jobs_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>تصویر گواهینامه</span>
                                <input type="file" name="certificate_jobs_upload_file" id="certificate_jobs_upload_file" class="upload_input_file">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12 upload_title_box" id="bill_upload_box" style="color: #1ABB9C;margin-top: 50px;">
            <p class="upload_box_title" style="float: right;margin-top: 10px;color: #e91e63;">
                فیش واریزی را آپلود نمایید
            </p>

            <button type="button" id="have_certificate_button" class="btn btn-success col-md-4 btn-lg pull-left" style="font-size: 15px;">
                گواهینامه طرح طبقه بندی مشاغل دارد
            </button>
            <form action="">
                <div class="col-md-12 col-sm-12 col-xs-12 upload_box">
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="col-md-4 upload_item" id="bill_upload_item">
                            <div class="col-md-12 file_box">
                                <div class="image_box">
                                    <img src="/images/upload-icon.png">
                                </div>
                                <span>تصویر فیش واریزی</span>
                                <input type="file" name="bill_upload_file" id="bill_upload_file" class="upload_input_file">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </form>

    <button type="button" id="back_to_certificate_list" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 30px;margin-top: 30px;font-size: 15px;">
        بازگشت به لیست گواهینامه ها
    </button>
    <button type="button" id="save_certificate_manager_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 30px;margin-top: 30px;font-size: 15px;">
        +                                                 اضافه کردن به لیست گواهینامه ها
    </button>
</div>