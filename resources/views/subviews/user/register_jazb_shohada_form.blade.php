
<div class="col-md-12" style="padding: 0;" id="jazb_shohada_view">
    <h2 class="StepTitle">لیست جذب خانواده شهدا، ایثارگران و معلولین غیر جنگی</h2>
    <div style="clear: both"></div>
    <div id="col-md-12 col-xs-12 col-sm-12 jazb_shohada_table">
        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action">
                <thead>
                <tr class="headings">
                    <th>
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" id="check-all" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </th>
                    <th class="column-title" style="display: table-cell;">نام</th>
                    <th class="column-title" style="display: table-cell;">نام خانوادگی</th>
                    <th class="column-title" style="display: table-cell;">نوع امتیاز</th>
                    <th class="column-title" style="display: table-cell;">درصد</th>
                    <th class="column-title" style="display: table-cell;">تاریخ شروع به کار</th>
                    <th class="column-title" style="display: table-cell;">سابقه کار به ماه</th>
                    <th class="column-title" style="display: table-cell;">شماره و تاریخ گواهی واحد تأیید کننده</th>
                    <th class="column-title" style="display: table-cell;">شماره کد کارگاه بیمه</th>
                    <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">عملیات</span>
                    </th>
                    <th class="bulk-actions" colspan="7" style="display: none;">
                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
                    </th>
                </tr>
                </thead>

                <tbody>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">علی</td>
                    <td class=" ">علوی</td>
                    <td class=" ">تست</td>
                    <td class=" ">۲۵٪</td>
                    <td class=" ">۱۳۹۵/۲/۱۲</td>
                    <td class=" ">۲۰</td>
                    <td class=" ">۲۱۳۴۲۳۴   </td>
                    <td class=" ">۱۲۴۱</td>
                    <td class=" last">
                        <a class="display-btn display_jazb_shohada_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_jazb_shohada_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_jazb_shohada" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                <tr class="even pointer">
                    <td class="a-center ">
                        <div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" name="table_records" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                    <td class=" ">مهدی</td>
                    <td class=" ">رضایی</td>
                    <td class=" ">تست</td>
                    <td class=" ">۴۵٪</td>
                    <td class=" ">۱۳۹۵/۲/۱۲</td>
                    <td class=" ">۲۰</td>
                    <td class=" ">۲۱۳۴۲۳۴   </td>
                    <td class=" ">۱۲۴۱</td>
                    <td class=" last">
                        <a class="display-btn display_jazb_shohada_info" href="#"><i class="fa fa-eye"></i></a>
                        <a class="edit-btn edit_jazb_shohada_info" href="#"><i class="fa fa-pencil"></i></a>
                        <a class="delete-btn delete_jazb_shohada" href="#"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <button type="button" id="add_jazb_shohada_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-bottom: 10px;margin-top: 0px;font-size: 15px;">
        +                                                 اضافه کردن فرد جدید
    </button>
</div>
<div class="col-md-12" id="jazb_shohada_form" style="padding: 0">
    <h2 class="extra_title StepTitle">افزودن فرد جدید به لیست جذب شهدا، ایثارگران و معلولین غیر جنگی</h2>
    <form class="form-horizontal col-md-12 col-sm12 col-xs-12 form-label-left" >
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_first_name">نام
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_first_name" name="jazb_first_name" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_last_name">نام خانوادگی
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_last_name" name="jazb_last_name" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_score_type">نوع امتیاز
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_score_type" name="jazb_score_type" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_percent">درصد
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_percent" name="jazb_percent" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_start_date">تاریخ شروع به کار
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_start_date" name="jazb_start_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_experience">سابقه کار ) به ماه (
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_experience" name="jazb_experience" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_accept_number">شماره گواهی واحد تأیید کننده
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_accept_number" name="jazb_accept_number" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_accept_date"> تاریخ گواهی واحد تأیید کننده
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_accept_date" name="jazb_accept_date" required="required" class="date_input form-control col-md-12 col-xs-12">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="jazb_workhouse_code">شماره کد کارگاه بیمه
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="jazb_workhouse_code" name="jazb_workhouse_code" required="required" class="form-control col-md-12 col-xs-12">
                </div>
            </div>
        </div>
    </form>

    <button type="button" id="save_jazb_shohada_button" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
        +                                                 اضافه کردن به لیست امکانات و تجهیزات
    </button>
    <button type="button" id="back_to_jazb_shohada_list" class="btn btn-success col-md-4 btn-lg pull-left" style="margin-left: 45px;margin-top: 30px;font-size: 15px;">
        بازگشت به لیست امکانات و تجهیزات
    </button>
</div>
