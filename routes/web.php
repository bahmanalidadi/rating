<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/user/register-info', 'DashboardController@register_info');
Route::get('/user/request-membership', 'DashboardController@request_membership');
Route::get('/user/pay-factor', 'DashboardController@pay_factor');
Route::get('/user/request-eligibility', 'DashboardController@request_eligibility');
Route::get('/user/request-rating', 'DashboardController@request_rating');
Route::get('/user/rating-result', 'DashboardController@rating_result');
Route::get('/user/rating-result-single', 'DashboardController@rating_result_single');
Route::get('/user/rating-result-details', 'DashboardController@rating_result_details');

Route::post('/user/upload-file', 'UserController@upload_file');
Route::post('/user/get-cities', 'UserController@get_cities');

Route::get('/inspector/new-requests', 'InspectorController@new_requests');
Route::get('/inspector/single-request', 'InspectorController@single_request');
Route::get('/inspector/all-requests', 'InspectorController@all_requests');
Route::get('/inspector/inspect-result', 'InspectorController@inspect_result');
Route::get('/inspector/single-inspect', 'InspectorController@single_inspect');
Route::get('/inspector/template-forms', 'InspectorController@template_forms');

Route::get('/assessor/new-requests', 'AssessorController@new_requests');
Route::get('/assessor/requests', 'AssessorController@requests');
Route::get('/assessor/single-request', 'AssessorController@single_request');
Route::get('/assessor/rating-form-steps', 'AssessorController@rating_form_steps');
Route::get('/assessor/calculate-score', 'AssessorController@calculate_score');
Route::get('/assessor/rating-result-details', 'AssessorController@rating_result_details');
Route::get('/assessor/history', 'AssessorController@history');
Route::get('/assessor/download-forms', 'AssessorController@download_forms');

Route::get('/admin/expert-confirmed', 'AdminController@expert_confirmed');
Route::get('/admin/expert-not-confirmed', 'AdminController@expert_not_confirmed');
Route::get('/admin/office-confirmed', 'AdminController@office_confirmed');
Route::get('/admin/office-not-confirmed', 'AdminController@office_not_confirmed');
Route::get('/admin/new-requests', 'AdminController@new_requests');
Route::get('/admin/inspect-new-requests', 'AdminController@inspect_new_requests');
Route::get('/admin/all-users', 'AdminController@all_users');
Route::get('/admin/all-requests', 'AdminController@all_requests');
Route::get('/admin/single-request', 'AdminController@single_request');
Route::get('/admin/inspect-single-request', 'AdminController@inspect_single_request');
Route::get('/admin/single-inspect', 'AdminController@single_inspect');
Route::get('/admin/rating-result', 'AdminController@rating_result');
Route::get('/admin/rating-result-single', 'AdminController@rating_result_single');
Route::get('/admin/rating-result-details', 'AdminController@rating_result_details');
Route::get('/admin/assessors', 'AdminController@assessors');
Route::get('/admin/assessor-history', 'AdminController@assessor_history');

Route::get('/admin/accounting/definition', 'AdminController@account_definition');
Route::get('/admin/accounting/income', 'AdminController@account_income');
Route::get('/admin/accounting/moein-codes', 'AdminController@moein_codes');
Route::get('/admin/accounting/define-detailed', 'AdminController@define_detailed');
Route::get('/admin/accounting/received', 'AdminController@received');

Route::get('/admin/member-list', 'AdminController@member_list');
Route::get('/admin/payments-made', 'AdminController@payments_made');
Route::get('/admin/receive-letters', 'AdminController@receive_letters');
Route::get('/admin/sent-letters', 'AdminController@sent_letters');

Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/assessors_list', 'AdminController@assessors');
Route::get('/admin/inspectors', 'AdminController@inspectors');
Route::get('/admin/inspect-result', 'AdminController@inspect_result');
Route::get('/admin/responsible', 'AdminController@responsible');
Route::get('/admin/admins', 'AdminController@admins');

Route::get('/admin/accounting/definition','AdminController@account_definition');
Route::get('/admin/accounting/income','AdminController@account_income');
Route::get('/admin/accounting/payments','AdminController@admin_payments');
Route::get('/admin/accounting/received','AdminController@admin_received');

Route::get('/responsible-company/assessors', 'ResponsibleCompanyController@assessors');
Route::get('/responsible-company/history', 'ResponsibleCompanyController@history');
Route::get('/responsible-company/new-requests', 'ResponsibleCompanyController@new_requests');
Route::get('/responsible-company/all-requests', 'ResponsibleCompanyController@all_requests');
Route::get('/responsible-company/single-request', 'ResponsibleCompanyController@single_request');
Route::get('/responsible-company/inspect-single-request', 'ResponsibleCompanyController@inspect_single_request');
Route::get('/responsible-company/rating-result', 'ResponsibleCompanyController@rating_result');
Route::get('/responsible-company/rating-result-single', 'ResponsibleCompanyController@rating_result_single');
Route::get('/responsible-company/rating-result-details', 'ResponsibleCompanyController@rating_result_details');
Route::get('/responsible-company/certificate', 'ResponsibleCompanyController@certificate');
Route::get('/responsible-company/summary-company', 'ResponsibleCompanyController@summary_company');

Route::get('/responsible-company/inspectors', 'ResponsibleCompanyController@inspectors');
Route::get('/responsible-company/inspect-all-requests', 'ResponsibleCompanyController@inspect_all_requests');
Route::get('/responsible-company/inspect-new-requests', 'ResponsibleCompanyController@inspect_new_requests');
Route::get('/responsible-company/inspect-result', 'ResponsibleCompanyController@inspect_result');
Route::get('/responsible-company/inspect-template-forms', 'ResponsibleCompanyController@inspect_template_forms');
Route::get('/responsible-company/rating-template-forms', 'ResponsibleCompanyController@rating_template_forms');


Route::post('/user/add-company', 'UserController@add_company');


Route::get('/', function () {
//    return view('welcome');
    return redirect('dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
