/**
 * Created by macintosh on 6/6/17.
 */
var selected_id = '';
$('.upload_item').click(function(){
    selected_id = '#'+$(this).attr('id');
    $(this).children('.file_box').children('.upload_input_file')[0].click()
});

$(".upload_input_file").change(function () {
    if (this.files && this.files[0]) {
        //var reader = new FileReader();
        //reader.onload = imageIsLoaded;
        //reader.readAsDataURL(this.files[0]);
    }
});

function imageIsLoaded(e) {
    //$(selected_id + ' .image_box img').attr('src', e.target.result);
    //$(selected_id + ' span').css('color', '#1ABB9C');
}

$('.add_user').click(function(){
    $('.member_card_box').hide();
    $('#add_user_form').fadeIn();
    window.scrollTo(0, 0);
});
$('.member_card_info_btn').click(function(){
    $('.member_card_box').hide();
    $('#add_user_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#save_add_user_button').click(function(){
    $('#add_user_form').hide();
    $('.member_card_box').fadeIn();
    window.scrollTo(0, 0);
});
$('#add_employee_button').click(function(){
    $('#summary_office_info').hide();
    $('#central_office_employee_form').fadeIn();
    window.scrollTo(0, 0);
});
$('.display_employee_info').click(function(){
    $('#summary_office_info').hide();
    $('#central_office_employee_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#save_add_employee_button').click(function(){
    $('#central_office_employee_form').hide();
    $('#summary_office_info').fadeIn();
    window.scrollTo(0, 0);
});
$('#back_to_employees_list').click(function(){
    $('#central_office_employee_form').hide();
    $('#summary_office_info').fadeIn();
    window.scrollTo(0, 0);
});

$('#edit_office_info').click(function(){
    $('#summary_office_info').hide();
    $('#central_office_employee_form').hide();
    $('#central_office_edit_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#save_office_info').click(function(){
    $('#central_office_edit_form').hide();
    $('#central_office_employee_form').hide();
    $('#summary_office_info').fadeIn();
    window.scrollTo(0, 0);

});
$('#add_treaty_button').click(function(){
    $('#treaty_view_info').hide();
    $('#treaty_form').fadeIn();
    window.scrollTo(0, 0);
});
$('save_treaty_button').click(function(){
    $('#treaty_form').hide();
    $('#treaty_view_info').fadeIn();
    window.scrollTo(0, 0);
});
$('.display_treaty_info').click(function(){
    $('#treaty_view_info').hide();
    $('#treaty_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#back_to_treaty_list').click(function(){
    $('#treaty_form').hide();
    $('#treaty_view_info').fadeIn();
    window.scrollTo(0, 0);
});
$('#add_employee_treaty_button').click(function(){
    $('#treaty_form').hide();
    $('#treaty_view_info').hide();
    $('#treaty_employee_form').fadeIn();
    window.scrollTo(0, 0);
});
$('.display_treaty_employee_info').click(function(){
    $('#treaty_form').hide();
    $('#treaty_view_info').hide();
    $('#treaty_employee_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#save_treaty_employee_button').click(function(){
    $('#treaty_employee_form').hide();
    $('#treaty_view_info').hide();
    $('#treaty_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#back_to_treaty_employees_list').click(function(){
    $('#treaty_employee_form').hide();
    $('#treaty_view_info').hide();
    $('#treaty_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#not_certificate_button').click(function(){
    $('#certificate_jobs_upload_box').hide();
    $('#bill_upload_box').fadeIn();
    window.scrollTo(0, 0);
});
$('#have_certificate_button').click(function(){
    $('#bill_upload_box').hide();
    $('#certificate_jobs_upload_box').fadeIn();
    window.scrollTo(0, 0);
});
$('#add_equipment_button').click(function(){
    $('#equipment_view').hide();
    $('#equipment_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#save_equipment_button').click(function(){
    $('#equipment_view').fadeIn();
    $('#equipment_form').hide();
    window.scrollTo(0, 0);
});
$('.display_equipment_info').click(function(){
    $('#equipment_view').hide();
    $('#equipment_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#back_to_equipment_list').click(function(){
    $('#equipment_view').fadeIn();
    $('#equipment_form').hide();
    window.scrollTo(0, 0);
});
$('#add_jazb_shohada_button').click(function(){
    $('#jazb_shohada_view').hide();
    $('#jazb_shohada_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#save_jazb_shohada_button').click(function(){
    $('#jazb_shohada_view').fadeIn();
    $('#jazb_shohada_form').hide();
    window.scrollTo(0, 0);
});
$('.display_jazb_shohada_info').click(function(){
    $('#jazb_shohada_view').hide();
    $('#jazb_shohada_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#back_to_jazb_shohada_list').click(function(){
    $('#jazb_shohada_view').fadeIn();
    $('#jazb_shohada_form').hide();
    window.scrollTo(0, 0);
});
$('#add_certificate_manager_button').click(function(){
    $('#add_user_form').hide();
    $('#certificate_manager_form').fadeIn();
    window.scrollTo(0, 0);
});
$('.display_certificate_info').click(function(){
    $('#add_user_form').hide();
    $('#certificate_manager_form').fadeIn();
    window.scrollTo(0, 0);
});
$('#save_certificate_manager_button').click(function(){
    $('#add_user_form').fadeIn();
    $('#certificate_manager_form').hide();
    window.scrollTo(0, 0);
});
$('#back_to_certificate_list').click(function(){
    $('#add_user_form').fadeIn();
    $('#certificate_manager_form').hide();
    window.scrollTo(0, 0);
});