<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $province_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property Province $province
 * @property CompanyProperty[] $companyProperties
 */
class City extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'city';

    /**
     * @var array
     */
    protected $fillable = ['province_id', 'name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companyProperties()
    {
        return $this->hasMany('App\CompanyProperty');
    }
}
