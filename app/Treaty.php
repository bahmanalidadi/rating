<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $treaty_number
 * @property string $treaty_employees_number
 * @property integer $treaty_date
 * @property string $treaty_workhouse_code
 * @property string $treaty_subject
 * @property string $treaty_summary
 * @property integer $treaty_price
 * @property integer $treaty_total_price
 * @property integer $treaty_duration
 * @property integer $treaty_start_date
 * @property integer $treaty_end_date
 * @property string $treaty_place
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Treaty extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'treaty';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'treaty_number', 'treaty_employees_number', 'treaty_date', 'treaty_workhouse_code', 'treaty_subject', 'treaty_summary', 'treaty_price', 'treaty_total_price', 'treaty_duration', 'treaty_start_date', 'treaty_end_date', 'treaty_place', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
