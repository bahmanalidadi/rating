<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $upload_file_type_id
 * @property integer $user_id
 * @property string $file
 * @property string $created_at
 * @property string $updated_at
 * @property UploadFileType $uploadFileType
 * @property User $user
 */
class UploadedFile extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'uploaded_file';

    /**
     * @var array
     */
    protected $fillable = ['upload_file_type_id', 'user_id', 'file', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploadFileType()
    {
        return $this->belongsTo('App\UploadFileType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
