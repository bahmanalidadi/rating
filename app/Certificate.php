<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $manager_id
 * @property string $certificate_title
 * @property integer $certificate_start_date
 * @property integer $certificate_end_date
 * @property string $office_type
 * @property string $office_no
 * @property string $created_at
 * @property string $updated_at
 * @property ManagerCompany $managerCompany
 */
class Certificate extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'certificate';

    /**
     * @var array
     */
    protected $fillable = ['manager_id', 'certificate_title', 'certificate_start_date', 'certificate_end_date', 'office_type', 'office_no', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function managerCompany()
    {
        return $this->belongsTo('App\ManagerCompany', 'manager_id');
    }
}
