<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $city_id
 * @property string $company_name
 * @property string $company_national_id
 * @property integer $company_type
 * @property string $register_number
 * @property integer $register_date
 * @property string $register_place
 * @property string $company_subject
 * @property string $activity_type
 * @property string $company_address
 * @property string $shipment_licence_number
 * @property string $printing_licence_number
 * @property string $established_history
 * @property integer $last_modified_date
 * @property string $current_company_subject
 * @property string $created_at
 * @property string $updated_at
 * @property City $city
 */
class Company extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'company_properties';

    /**
     * @var array
     */
    protected $fillable = ['city_id', 'company_name', 'company_national_id', 'company_type', 'register_number', 'register_date', 'register_place', 'company_subject', 'activity_type', 'company_address', 'shipment_licence_number', 'printing_licence_number', 'established_history', 'last_modified_date', 'current_company_subject', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
