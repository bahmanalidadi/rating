<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $office_space
 * @property string $office_type
 * @property string $office_no
 * @property boolean $office_floor
 * @property string $office_phone
 * @property string $office_mobile
 * @property string $office_postal_code
 * @property string $office_address
 * @property string $end_work_number
 * @property integer $end_work_date
 * @property string $evidence_number
 * @property integer $evidence_date
 * @property string $rental_number
 * @property integer $rental_date
 * @property integer $rent_duration
 * @property integer $rent_start
 * @property integer $rent_end
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class CentralOffice extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'central_office';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'office_space', 'office_type', 'office_no', 'office_floor', 'office_phone', 'office_mobile', 'office_postal_code', 'office_address', 'end_work_number', 'end_work_date', 'evidence_number', 'evidence_date', 'rental_number', 'rental_date', 'rent_duration', 'rent_start', 'rent_end', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
