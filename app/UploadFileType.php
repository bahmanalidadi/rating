<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $upload_file_category_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property UploadFileCategory $uploadFileCategory
 * @property UploadedFile[] $uploadedFiles
 */
class UploadFileType extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'upload_file_type';

    /**
     * @var array
     */
    protected $fillable = ['upload_file_category_id', 'name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploadFileCategory()
    {
        return $this->belongsTo('App\UploadFileCategory');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function uploadedFiles()
    {
        return $this->hasMany('App\UploadedFile');
    }
}
