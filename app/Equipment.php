<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $equipment_title
 * @property string $equipment_model
 * @property string $equipment_serial
 * @property integer $equipment_quantity
 * @property string $equipment_factor_number
 * @property string $equipment_group
 * @property integer $equipment_date
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Equipment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'equipment_title', 'equipment_model', 'equipment_serial', 'equipment_quantity', 'equipment_factor_number', 'equipment_group', 'equipment_date', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
