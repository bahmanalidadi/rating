<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

/**
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property CentralOffice[] $centralOffices
 * @property Equipment[] $equipment
 * @property JazbShohada[] $jazbShohadas
 * @property ManagerCompany[] $managerCompanies
 * @property Role[] $roles
 * @property Treaty[] $treaties
 * @property UploadedFile[] $uploadedFiles
 */
class User extends Authenticatable
{
    use EntrustUserTrait;
    use AuthenticableTrait;
    use Notifiable;

    /**
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'remember_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function centralOffices()
    {
        return $this->hasMany('App\CentralOffice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function equipment()
    {
        return $this->hasMany('App\Equipment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jazbShohadas()
    {
        return $this->hasMany('App\JazbShohada');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managerCompanies()
    {
        return $this->hasMany('App\ManagerCompany');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function treaties()
    {
        return $this->hasMany('App\Treaty');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function uploadedFiles()
    {
        return $this->hasMany('App\UploadedFile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }
}
