<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InspectorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function new_requests()
    {
        $user = Auth::user();
        return view('inspector.new_requests',
            [
                'user' => $user
            ]);
    }
    public function all_requests()
    {
        $user = Auth::user();
        return view('inspector.all_requests',
            [
                'user' => $user
            ]);
    }

    public function single_request(Request $request)
    {
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('inspector.single_request',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function inspect_result(Request $request){
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('inspector.inspect_result',
            [
                'user' => $user,
            ]);
    }
    public function single_inspect(Request $request){
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('inspector.single_inspect',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function template_forms(Request $request){
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('inspector.template_forms',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
}
