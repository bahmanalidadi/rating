<?php

namespace App\Http\Controllers;

use App\City;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add_company(Request $request)
    {
        $company_data = $request->all();
        $company = new Company();
        $company = $this->set_model_value($company, $company_data);
        $user = User::find(Auth::user()['id']);
        $user->company()->save($company);
    }

    public function set_model_value($model, $data)
    {
        foreach ($data as $key=>$item) {
            if($key != '_token')
            {
                $model->{$key}=$item;
            }
        }
        return $model;
    }

    public function upload_file(Request $request)
    {
        if ($request->hasFile('file')) {
            $user_id = Auth::user()->id;
            $file = $request->file;
            $extension = $request->file->extension();
            $rand_num = rand(1111111, 99999);
            $file_name = "image$rand_num.$extension";
            $upload_path = "images/upload/$user_id";
            if ($file->move($upload_path, $file_name)) {
                return response()->json([
                    'file_name' => "/$upload_path/$file_name",
                    'message' => 'yes'
                ], 200);
            }
        } else {
            return response()->json(['message' => 'File not sent'], 403);
        }
    }

    public function get_cities(Request $request)
    {
        $province_id = $request->input('province_id');
        $province = Province::find($province_id);

        $cities = $province->cities;
        $result = [];
        foreach ($cities as $city) {
            $result[] = [
                'city_id' => $city['id'],
                'name' => $city['name']
            ];
        }

        return response()->json([
            'cities' => $result
        ], 200);
    }
}
