<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AssessorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function new_requests()
    {
        $user = Auth::user();
        return view('assessor.new_requests',
            [
                'user' => $user
            ]);
    }
    public function requests()
    {
        $user = Auth::user();
        return view('assessor.requests',
            [
                'user' => $user
            ]);
    }

    public function single_request(Request $request)
    {
        $user = Auth::user();
        $mode = $request->input('mode');

        return view('assessor.single_request',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }

    public function rating_form_steps(Request $request)
    {
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('assessor.rating_form_steps',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function calculate_score(Request $request)
    {
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('assessor.rating_result_single',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function rating_result_details(Request $request)
    {
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('assessor.rating_result_details',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function history(Request $request)
    {
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('assessor.history',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function download_forms()
    {
        $user = Auth::user();
        return view('assessor.download_forms',
            [
                'user' => $user
            ]);
    }
}
