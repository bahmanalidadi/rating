<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResponsibleCompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function assessors()
    {
        $user = Auth::user();
        return view('responsible_company.assessors',
            [
                'user' => $user
            ]);
    }

    public function history(){
        $user = Auth::user();
        return view('responsible_company.history',
            [
                'user' => $user
            ]);
    }
    public function new_requests(Request $request){
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('responsible_company.requests',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function all_requests(Request $request){
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('responsible_company.requests',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function single_request(Request $request){
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('responsible_company.single_request',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function inspect_single_request(Request $request){
        $user = Auth::user();
        $mode = $request->input('mode');
        return view('responsible_company.inspect_single_request',
            [
                'user' => $user,
                'mode' => $mode
            ]);
    }
    public function rating_result(){
        $user = Auth::user();
        return view('responsible_company.rating_result',
            [
                'user' => $user
            ]);
    }
    public function rating_result_single(){
        $user = Auth::user();
        return view('responsible_company.rating_result_single',
            [
                'user' => $user
            ]);
    }
    public function rating_result_details(){
        $user = Auth::user();
        return view('responsible_company.rating_result_details',
            [
                'user' => $user
            ]);
    }

    public function certificate(){
        $user = Auth::user();
        return view('responsible_company.certificate',
            [
                'user' => $user
            ]);
    }

    public function summary_company(){
        $user = Auth::user();
        return view('responsible_company.summary_company',
            [
                'user' => $user
            ]);
    }

    public function inspectors(){
        $user = Auth::user();
        return view('responsible_company.inspectors ',
            [
                'user' => $user
            ]);
    }

    public function inspect_all_requests(){
        $user = Auth::user();
        return view('responsible_company.inspect_all_requests',
            [
                'user' => $user
            ]);
    }

    public function inspect_new_requests(){
        $user = Auth::user();
        return view('responsible_company.inspect_new_requests',
            [
                'user' => $user
            ]);
    }

    public function inspect_result(){
        $user = Auth::user();
        return view('responsible_company.inspect_result',
            [
                'user' => $user
            ]);
    }
    public function inspect_template_forms(){
        $user = Auth::user();
        return view('responsible_company.inspect_download_forms',
            [
                'user' => $user
            ]);
    }
    public function rating_template_forms(){
        $user = Auth::user();
        return view('responsible_company.rating_template_forms',
            [
                'user' => $user
            ]);
    }

}
