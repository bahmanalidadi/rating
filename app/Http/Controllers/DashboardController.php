<?php

namespace App\Http\Controllers;

use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->hasRole('user')) {
            return view('user.dashboard',
                [
                    'user' => $user
                ]);
        }
        elseif($user->hasRole('admin')){
            return view('admin.dashboard',
                [
                    'user' => $user
                ]);
        }
        elseif($user->hasRole('assessor')){
            return view('assessor.dashboard',
                [
                    'user' => $user
                ]);
        }
        elseif($user->hasRole('inspector')){
            return view('inspector.dashboard',
                [
                    'user' => $user
                ]);
        }
        elseif($user->hasRole('responsible_company')){
            return view('responsible_company.dashboard',
                [
                    'user' => $user
                ]);
        }
        elseif($user->hasRole('manager')){
            return view('manager.dashboard',
                [
                    'user' => $user
                ]);
        }
    }

    public function register_info()
    {
        $user = Auth::user();
        $province = Province::orderBy('name', 'asc')->get();
        return view("user.register_info", [
            'user' => $user,
            'province'=> $province
        ]);

    }
    public function request_eligibility()
    {
        $user = Auth::user();
        return view('user.request_eligibility',
            [
                'user' => $user
            ]);
    }
    public function request_rating()
    {
        $user = Auth::user();
        return view('user.request_rating',
            [
                'user' => $user
            ]);
    }


    public function rating_result(){
        $user = Auth::user();
        return view('user.rating_result',
            [
                'user' => $user
            ]);
    }
    public function rating_result_single(){
        $user = Auth::user();
        return view('user.rating_result_single',
            [
                'user' => $user
            ]);
    }
    public function rating_result_details(){
        $user = Auth::user();
        return view('user.rating_result_details',
            [
                'user' => $user
            ]);
    }

    public function request_membership(){
        $user = Auth::user();
        return view('user.request_membership',
            [
                'user' => $user
            ]);
    }

    public function pay_factor(){
        $user = Auth::user();
        return view('user.pay_factor',
            [
                'user' => $user
            ]);
    }
}
