<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function new_requests()
    {
        $user = Auth::user();
        return view('admin.new_requests',
            [
                'user' => $user
            ]);
    }
    public function inspect_new_requests()
    {
        $user = Auth::user();
        return view('admin.inspect_new_requests',
            [
                'user' => $user
            ]);
    }
    public function all_users()
    {
        $user = Auth::user();
        return view('admin.all_users',
            [
                'user' => $user
            ]);
    }
    public function all_requests()
    {
        $user = Auth::user();
        return view('admin.all_requests',
            [
                'user' => $user
            ]);
    }
    public function single_request()
    {
        $user = Auth::user();
        return view('admin.single_request',
            [
                'user' => $user
            ]);
    }
    public function inspect_single_request()
    {
        $user = Auth::user();
        return view('admin.inspect_single_request',
            [
                'user' => $user
            ]);
    }


    public function rating_result(){
        $user = Auth::user();
        return view('admin.rating_result',
            [
                'user' => $user
            ]);
    }
    public function rating_result_single(){
        $user = Auth::user();
        return view('admin.rating_result_single',
            [
                'user' => $user
            ]);
    }
    public function rating_result_details(){
        $user = Auth::user();
        return view('admin.rating_result_details',
            [
                'user' => $user
            ]);
    }
    public function users(){
        $user = Auth::user();
        return view('admin.users',
            [
                'user' => $user
            ]);
    }
    public function assessors(){
        $user = Auth::user();
        return view('admin.assessors',
            [
                'user' => $user
            ]);
    }
    public function responsible(){
        $user = Auth::user();
        return view('admin.responsible',
            [
                'user' => $user
            ]);
    }
    public function admins(){
        $user = Auth::user();
        return view('admin.admins',
            [
                'user' => $user
            ]);
    }
    public function inspectors(){
        $user = Auth::user();
        return view('admin.inspectors',
            [
                'user' => $user
            ]);
    }
    public function inspect_result(){
        $user = Auth::user();
        return view('admin.inspect_result',
            [
                'user' => $user
            ]);
    }
    public function single_inspect(){
        $user = Auth::user();
        return view('admin.single_inspect',
            [
                'user' => $user
            ]);
    }
    public function assessor_history(){
        $user = Auth::user();
        return view('admin.assessor_history',
            [
                'user' => $user
            ]);
    }

    public function expert_confirmed(){
        $user = Auth::user();
        return view('admin.expert_confirmed',
            [
                'user' => $user
            ]);
    }

    public function expert_not_confirmed(){
        $user = Auth::user();
        return view('admin.expert_not_confirmed',
            [
                'user' => $user
            ]);
    }

    public function office_confirmed(){
        $user = Auth::user();
        return view('admin.office_confirmed',
            [
                'user' => $user
            ]);
    }

    public function office_not_confirmed(){
        $user = Auth::user();
        return view('admin.office_not_confirmed',
            [
                'user' => $user
            ]);
    }

    public function member_list(){
        $user = Auth::user();
        return view('admin.member_list',
            [
                'user' => $user
            ]);
    }

    public function payments_made(){
        $user = Auth::user();
        return view('admin.payments_made',
            [
                'user' => $user
            ]);
    }

    public function receive_letters(){
        $user = Auth::user();
        return view('admin.receive_letters',
            [
                'user' => $user
            ]);
    }

    public function sent_letters(){
        $user = Auth::user();
        return view('admin.sent_letters',
            [
                'user' => $user
            ]);
    }

    public function account_definition(){
        $user = Auth::user();
        return view('admin.accounting.account_definition',
            [
                'user' => $user
            ]);
    }

    public function account_income(){
        $user = Auth::user();
        return view('admin.accounting.account_income',
            [
                'user' => $user
            ]);
    }

    public function admin_payments(){
        $user = Auth::user();
        return view('admin.accounting.payments',
            [
                'user' => $user
            ]);
    }
    public function admin_received(){
        $user = Auth::user();
        return view('admin.accounting.received',
            [
                'user' => $user
            ]);
    }

    public function moein_codes(){
        $user = Auth::user();
        return view('admin.accounting.moein_codes',
            [
                'user' => $user
            ]);
    }

    public function define_detailed(){
        $user = Auth::user();
        return view('admin.accounting.define_detailed',
            [
                'user' => $user
            ]);
    }

    public function received(){
        $user = Auth::user();
        return view('admin.accounting.received',
            [
                'user' => $user
            ]);
    }
}
