<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $father_name
 * @property integer $birth_date
 * @property integer $gender
 * @property string $national_code
 * @property string $id_number
 * @property string $place_of_issue
 * @property string $phone
 * @property string $mobile
 * @property integer $degree
 * @property string $major
 * @property integer $major_status
 * @property string $work_place
 * @property integer $position
 * @property integer $start_work_date
 * @property integer $end_work_date
 * @property string $experience
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Certificate[] $certificates
 */
class Managercompany extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'manager_company';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'first_name', 'last_name', 'father_name', 'birth_date', 'gender', 'national_code', 'id_number', 'place_of_issue', 'phone', 'mobile', 'degree', 'major', 'major_status', 'work_place', 'position', 'start_work_date', 'end_work_date', 'experience', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certificates()
    {
        return $this->hasMany('App\Certificate', 'manager_id');
    }
}
