<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $jazb_first_name
 * @property string $jazb_last_name
 * @property string $jazb_score_type
 * @property integer $jazb_percent
 * @property integer $jazb_start_date
 * @property integer $jazb_end_date
 * @property string $jazb_experience
 * @property string $jazb_accept_number
 * @property integer $jazb_accept_date
 * @property string $jazb_workhouse_code
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class JazbShohada extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'jazb_shohada';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'jazb_first_name', 'jazb_last_name', 'jazb_score_type', 'jazb_percent', 'jazb_start_date', 'jazb_end_date', 'jazb_experience', 'jazb_accept_number', 'jazb_accept_date', 'jazb_workhouse_code', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
